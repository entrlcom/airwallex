package airwallex

import (
	"context"
	"net/http"

	"github.com/pkg/errors"
)

// https://www.airwallex.com/docs/api#/Issuing/Authorizations/_api_v1_issuing_authorizations/get

type ListAuthorizationsRequest struct {
	BillingCurrency Currency            `json:"billing_currency,omitempty"`
	CardID          string              `json:"card_id,omitempty"`
	FromCreatedAt   *TimeISO8601        `json:"from_created_at,omitempty"`
	PageNum         int32               `json:"page_num,omitempty"`
	PageSize        int32               `json:"page_size,omitempty"`
	RetrievalRef    string              `json:"retrieval_ref,omitempty"`
	Status          AuthorizationStatus `json:"status,omitempty"`
	ToCreatedAt     *TimeISO8601        `json:"to_created_at,omitempty"`
}

type ListAuthorizationsResponse struct {
	HasMore bool             `json:"has_more,omitempty"`
	Items   []*Authorization `json:"items,omitempty"`
}

func (awx airwallex) ListAuthorizations(
	ctx context.Context,
	in *ListAuthorizationsRequest,
) (out *ListAuthorizationsResponse, err error) {
	out = new(ListAuthorizationsResponse)
	uri := string(awx.apiEndpoint) + string(listAuthorizationsEndpoint)
	if err = Do(ctx, awx.httpClient, http.MethodGet, uri, in, out); err != nil {
		return out, errors.Wrap(err, "Do()")
	}

	return out, err
}

//  https://www.airwallex.com/docs/api#/Issuing/Authorizations/_api_v1_issuing_authorizations__id_/get

type GetAuthorizationRequest struct {
	ID string `json:"id"`
}

type GetAuthorizationResponse Authorization

func (awx airwallex) GetAuthorization(
	ctx context.Context,
	in *GetAuthorizationRequest,
) (out *GetAuthorizationResponse, err error) {
	out = new(GetAuthorizationResponse)
	uri := string(awx.apiEndpoint) + string(getAuthorizationEndpoint)
	if err = Do(ctx, awx.httpClient, http.MethodGet, uri, in, out); err != nil {
		return out, errors.Wrap(err, "Do()")
	}

	return out, err
}
