package airwallex

import (
	"context"
	"net/http"

	"github.com/pkg/errors"
)

// https://www.airwallex.com/docs/api#/Payouts/Payers/_api_v1_payers/get

type ListPayersRequest struct {
	EntityType EntityType   `json:"entity_type,omitempty"`
	FromDate   *TimeISO8601 `json:"from_date,omitempty"`
	Name       string       `json:"name,omitempty"`
	NickName   string       `json:"nick_name,omitempty"`
	PageNum    int32        `json:"page_num,omitempty"`
	PageSize   int32        `json:"page_size,omitempty"`
	ToDate     *TimeISO8601 `json:"to_date,omitempty"`
}

type ListPayersResponse struct {
	HasMore bool     `json:"has_more,omitempty"`
	Items   []*Payer `json:"items,omitempty"`
}

func (awx airwallex) ListPayers(
	ctx context.Context,
	in *ListPayersRequest,
) (out *ListPayersResponse, err error) {
	out = new(ListPayersResponse)
	uri := string(awx.apiEndpoint) + string(listPayersEndpoint)
	if err = Do(ctx, awx.httpClient, http.MethodGet, uri, in, out); err != nil {
		return out, errors.Wrap(err, "Do()")
	}

	return out, err
}

// https://www.airwallex.com/docs/api#/Payouts/Payers/_api_v1_payers__payer_id_/get

type GetPayerRequest struct {
	PayerID string `json:"payer_id,omitempty"`
}

type GetPayerResponse Payer

func (awx airwallex) GetPayer(
	ctx context.Context,
	in *GetPayerRequest,
) (out *GetPayerResponse, err error) {
	out = new(GetPayerResponse)
	uri := string(awx.apiEndpoint) + string(getPayerEndpoint)
	if err = Do(ctx, awx.httpClient, http.MethodGet, uri, in, out); err != nil {
		return out, errors.Wrap(err, "Do()")
	}

	return out, err
}

// https://www.airwallex.com/docs/api#/Payouts/Payers/_api_v1_payers_create/post

type CreatePayerRequest struct {
	Nickname string        `json:"nickname,omitempty"`
	Payer    *PayerDetails `json:"payer,omitempty"`
}

type CreatePayerResponse Payer

func (awx airwallex) CreatePayer(
	ctx context.Context,
	in *CreatePayerRequest,
) (out *CreatePayerResponse, err error) {
	out = new(CreatePayerResponse)
	uri := string(awx.apiEndpoint) + string(createPayerEndpoint)
	if err = Do(ctx, awx.httpClient, http.MethodPost, uri, in, out); err != nil {
		return out, errors.Wrap(err, "Do()")
	}

	return out, err
}

// https://www.airwallex.com/docs/api#/Payouts/Payers/_api_v1_payers_delete__payer_id_/post

type DeletePayerRequest struct {
	PayerID string `json:"payer_id,omitempty"`
}

type DeletePayerResponse bool

func (awx airwallex) DeletePayer(
	ctx context.Context,
	in *DeletePayerRequest,
) (out *DeletePayerResponse, err error) {
	out = new(DeletePayerResponse)
	uri := string(awx.apiEndpoint) + string(deletePayerEndpoint)
	if err = Do(ctx, awx.httpClient, http.MethodPost, uri, in, out); err != nil {
		return out, errors.Wrap(err, "Do()")
	}

	return out, err
}

// https://www.airwallex.com/docs/api#/Payouts/Payers/_api_v1_payers_update__payer_id_/post

type UpdatePayerRequest Payer

type UpdatePayerResponse Payer

func (awx airwallex) UpdatePayer(
	ctx context.Context,
	in *UpdatePayerRequest,
) (out *UpdatePayerResponse, err error) {
	out = new(UpdatePayerResponse)
	uri := string(awx.apiEndpoint) + string(updatePayerEndpoint)
	if err = Do(ctx, awx.httpClient, http.MethodPost, uri, in, out); err != nil {
		return out, errors.Wrap(err, "Do()")
	}

	return out, err
}

// https://www.airwallex.com/docs/api#/Payouts/Payers/_api_v1_payers_validate/post

type ValidatePayerRequest struct {
	Nickname string        `json:"nickname,omitempty"`
	Payer    *PayerDetails `json:"payer,omitempty"`
}

type ValidatePayerResponse interface{}

func (awx airwallex) ValidatePayer(
	ctx context.Context,
	in *ValidatePayerRequest,
) (out *ValidatePayerResponse, err error) {
	out = new(ValidatePayerResponse)
	uri := string(awx.apiEndpoint) + string(validatePayerEndpoint)
	if err = Do(ctx, awx.httpClient, http.MethodPost, uri, in, out); err != nil {
		return out, errors.Wrap(err, "Do()")
	}

	return out, err
}
