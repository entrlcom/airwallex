package airwallex

import (
	"context"
	"net/http"

	"github.com/pkg/errors"
)

// https://www.airwallex.com/docs/api#/Payment_Acceptance/Payment_Links/_api_v1_pa_payment_links_create/post

type CreatePaymentLinkRequest struct {
	Amount      Decimal      `json:"amount,omitempty"`
	Currency    Currency     `json:"currency,omitempty"`
	ExpiresAt   *TimeISO8601 `json:"expires_at,omitempty"`
	Reusable    bool         `json:"reusable,omitempty"`
	Title       string       `json:"title,omitempty"`
	Description string       `json:"description,omitempty"`
	Reference   string       `json:"reference,omitempty"`
}

type CreatePaymentLinkResponse PaymentLink

func (awx airwallex) CreatePaymentLink(
	ctx context.Context,
	in *CreatePaymentLinkRequest,
) (out *CreatePaymentLinkResponse, err error) {
	out = new(CreatePaymentLinkResponse)
	uri := string(awx.apiEndpoint) + string(createPaymentLinkEndpoint)
	if err = Do(ctx, awx.httpClient, http.MethodPost, uri, in, out); err != nil {
		return out, errors.Wrap(err, "Do()")
	}

	return out, err
}

// https://www.airwallex.com/docs/api#/Payment_Acceptance/Payment_Links/_api_v1_pa_payment_links__id__update/post

type UpdatePaymentLinkRequest struct {
	ID          string `json:"id,omitempty"`
	Title       string `json:"title,omitempty"`
	Description string `json:"description,omitempty"`
	Reference   string `json:"reference,omitempty"`
}

type UpdatePaymentLinkResponse PaymentLink

func (awx airwallex) UpdatePaymentLink(
	ctx context.Context,
	in *UpdatePaymentLinkRequest,
) (out *UpdatePaymentLinkResponse, err error) {
	out = new(UpdatePaymentLinkResponse)
	uri := string(awx.apiEndpoint) + string(updatePaymentLinkEndpoint)
	if err = Do(ctx, awx.httpClient, http.MethodPost, uri, in, out); err != nil {
		return out, errors.Wrap(err, "Do()")
	}

	return out, err
}

// https://www.airwallex.com/docs/api#/Payment_Acceptance/Payment_Links/_api_v1_pa_payment_links__id_/get

type GetPaymentLinkRequest struct {
	ID string `json:"id,omitempty"`
}

type GetPaymentLinkResponse PaymentLink

func (awx airwallex) GetPaymentLink(
	ctx context.Context,
	in *GetPaymentLinkRequest,
) (out *GetPaymentLinkResponse, err error) {
	out = new(GetPaymentLinkResponse)
	uri := string(awx.apiEndpoint) + string(getPaymentLinkEndpoint)
	if err = Do(ctx, awx.httpClient, http.MethodGet, uri, in, out); err != nil {
		return out, errors.Wrap(err, "Do()")
	}

	return out, err
}

// https://www.airwallex.com/docs/api#/Payment_Acceptance/Payment_Links/_api_v1_pa_payment_links__id__notify_shopper/post

type SendPaymentLinkRequest struct {
	ID           string `json:"id,omitempty"`
	ShopperEmail string `json:"shopper_email,omitempty"`
}

type SendPaymentLinkResponse string

func (awx airwallex) SendPaymentLink(
	ctx context.Context,
	in *SendPaymentLinkRequest,
) (out *SendPaymentLinkResponse, err error) {
	out = new(SendPaymentLinkResponse)
	uri := string(awx.apiEndpoint) + string(sendPaymentLinkEndpoint)
	if err = Do(ctx, awx.httpClient, http.MethodPost, uri, in, out); err != nil {
		return out, errors.Wrap(err, "Do()")
	}

	return out, err
}

// https://www.airwallex.com/docs/api#/Payment_Acceptance/Payment_Links/_api_v1_pa_payment_links__id__activate/post

type ActivatePaymentLinkRequest struct {
	ID string `json:"id,omitempty"`
}

type ActivatePaymentLinkResponse PaymentLink

func (awx airwallex) ActivatePaymentLink(
	ctx context.Context,
	in *ActivatePaymentLinkRequest,
) (out *ActivatePaymentLinkResponse, err error) {
	out = new(ActivatePaymentLinkResponse)
	uri := string(awx.apiEndpoint) + string(activatePaymentLinkEndpoint)
	if err = Do(ctx, awx.httpClient, http.MethodPost, uri, in, out); err != nil {
		return out, errors.Wrap(err, "Do()")
	}

	return out, err
}

// https://www.airwallex.com/docs/api#/Payment_Acceptance/Payment_Links/_api_v1_pa_payment_links__id__deactivate/post

type DeactivatePaymentLinkRequest struct {
	ID string `json:"id,omitempty"`
}

type DeactivatePaymentLinkResponse PaymentLink

func (awx airwallex) DeactivatePaymentLink(
	ctx context.Context,
	in *DeactivatePaymentLinkRequest,
) (out *DeactivatePaymentLinkResponse, err error) {
	out = new(DeactivatePaymentLinkResponse)
	uri := string(awx.apiEndpoint) + string(deactivatePaymentLinkEndpoint)
	if err = Do(ctx, awx.httpClient, http.MethodPost, uri, in, out); err != nil {
		return out, errors.Wrap(err, "Do()")
	}

	return out, err
}

// https://www.airwallex.com/docs/api#/Payment_Acceptance/Payment_Links/_api_v1_pa_payment_links__id__delete/post

type DeletePaymentLinkRequest struct {
	ID string `json:"id,omitempty"`
}

type DeletePaymentLinkResponse string

func (awx airwallex) DeletePaymentLink(
	ctx context.Context,
	in *DeletePaymentLinkRequest,
) (out *DeletePaymentLinkResponse, err error) {
	out = new(DeletePaymentLinkResponse)
	uri := string(awx.apiEndpoint) + string(deletePaymentLinkEndpoint)
	if err = Do(ctx, awx.httpClient, http.MethodPost, uri, in, out); err != nil {
		return out, errors.Wrap(err, "Do()")
	}

	return out, err
}

// https://www.airwallex.com/docs/api#/Payment_Acceptance/Payment_Links/_api_v1_pa_payment_links/get

type ListPaymentLinksRequest struct {
	Active        bool         `json:"active,omitempty"`
	FromCreatedAt *TimeISO8601 `json:"from_created_at,omitempty"`
	PageNum       int32        `json:"page_num,omitempty"`
	PageSize      int32        `json:"page_size,omitempty"`
	Reusable      bool         `json:"reusable,omitempty"`
	ToCreatedAt   *TimeISO8601 `json:"to_created_at,omitempty"`
}

type ListPaymentLinksResponse struct {
	HasNext bool           `json:"has_next,omitempty"`
	Items   []*PaymentLink `json:"items,omitempty"`
}

func (awx airwallex) ListPaymentLinks(
	ctx context.Context,
	in *ListPaymentLinksRequest,
) (out *ListPaymentLinksResponse, err error) {
	out = new(ListPaymentLinksResponse)
	uri := string(awx.apiEndpoint) + string(listPaymentLinksEndpoint)
	if err = Do(ctx, awx.httpClient, http.MethodGet, uri, in, out); err != nil {
		return out, errors.Wrap(err, "Do()")
	}

	return out, err
}
