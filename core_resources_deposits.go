package airwallex

import (
	"context"
	"net/http"

	"github.com/pkg/errors"
)

// https://www.airwallex.com/docs/api#/Core_Resources/Deposits/_api_v1_deposits/get

type ListDepositsRequest struct {
	FromCreatedAt *TimeISO8601 `json:"from_created_at,omitempty"`
	PageNum       int32        `json:"page_num,omitempty"`
	PageSize      int32        `json:"page_size,omitempty"`
	ToCreatedAt   *TimeISO8601 `json:"to_created_at,omitempty"`
}

type ListDepositsResponse struct {
	HasMore bool       `json:"has_more,omitempty"`
	Items   []*Deposit `json:"items,omitempty"`
}

func (awx airwallex) ListDeposits(
	ctx context.Context,
	in *ListDepositsRequest,
) (out *ListDepositsResponse, err error) {
	out = new(ListDepositsResponse)
	uri := string(awx.apiEndpoint) + string(listDepositsEndpoint)
	if err = Do(ctx, awx.httpClient, http.MethodGet, uri, in, out); err != nil {
		return out, errors.Wrap(err, "Do()")
	}

	return out, err
}
