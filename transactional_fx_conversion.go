package airwallex

import (
	"context"
	"net/http"

	"github.com/google/uuid"
	"github.com/pkg/errors"
)

// https://www.airwallex.com/docs/api#/Transactional_FX/Conversion/_api_v1_conversions_create/post

type CreateConversionRequest struct {
	BuyAmount      Decimal   `json:"buy_amount,omitempty"`
	BuyCurrency    Currency  `json:"buy_currency,omitempty"`
	ConversionDate string    `json:"conversion_date,omitempty"` // TODO: Time (YYYY-MM-DD)
	QuoteID        string    `json:"quote_id,omitempty"`
	Reason         string    `json:"reason,omitempty"`
	RequestID      uuid.UUID `json:"request_id,omitempty"`
	SellAmount     Decimal   `json:"sell_amount,omitempty"`
	SellCurrency   Currency  `json:"sell_currency,omitempty"`
	TermAgreement  bool      `json:"term_agreement,omitempty"`
}

type CreateConversionResponse Conversion

func (awx airwallex) CreateConversion(
	ctx context.Context,
	in *CreateConversionRequest,
) (out *CreateConversionResponse, err error) {
	out = new(CreateConversionResponse)
	uri := string(awx.apiEndpoint) + string(createConversionEndpoint)
	if err = Do(ctx, awx.httpClient, http.MethodPost, uri, in, out); err != nil {
		return out, errors.Wrap(err, "Do()")
	}

	return out, err
}

// https://www.airwallex.com/docs/api#/Transactional_FX/Conversion/_api_v1_conversions__conversion_id_/get

type GetConversionRequest struct {
	ConversionID uuid.UUID `json:"conversion_id,omitempty"`
}

type GetConversionResponse Conversion

func (awx airwallex) GetConversion(
	ctx context.Context,
	in *GetConversionRequest,
) (out *GetConversionResponse, err error) {
	out = new(GetConversionResponse)
	uri := string(awx.apiEndpoint) + string(getConversionEndpoint)
	if err = Do(ctx, awx.httpClient, http.MethodGet, uri, in, out); err != nil {
		return out, errors.Wrap(err, "Do()")
	}

	return out, err
}

// https://www.airwallex.com/docs/api#/Transactional_FX/Conversion/_api_v1_conversions/get

type ListConversionsRequest struct {
	BuyCurrency   Currency         `json:"buy_currency,omitempty"`
	FromCreatedAt *TimeISO8601     `json:"from_created_at,omitempty"`
	PageNum       int32            `json:"page_num,omitempty"`
	PageSize      int32            `json:"page_size,omitempty"`
	RequestID     uuid.UUID        `json:"request_id,omitempty"`
	SellCurrency  Currency         `json:"sell_currency,omitempty"`
	Status        ConversionStatus `json:"status,omitempty"`
	ToCreatedAt   *TimeISO8601     `json:"to_created_at,omitempty"`
}

type ListConversionsResponse struct {
	HasMore bool          `json:"has_more,omitempty"`
	Items   []*Conversion `json:"items,omitempty"`
}

func (awx airwallex) ListConversions(
	ctx context.Context,
	in *ListConversionsRequest,
) (out *ListConversionsResponse, err error) {
	out = new(ListConversionsResponse)
	uri := string(awx.apiEndpoint) + string(listConversionsEndpoint)
	if err = Do(ctx, awx.httpClient, http.MethodGet, uri, in, out); err != nil {
		return out, errors.Wrap(err, "Do()")
	}

	return out, err
}

// https://www.airwallex.com/docs/api#/Transactional_FX/Conversion/_api_v1_conversions__conversion_id__reverse/post

type ReverseSettledConversionRequest struct {
	ConversionID uuid.UUID `json:"conversion_id,omitempty"`
	RequestID    uuid.UUID `json:"request_id,omitempty"`
}

type ReverseSettledConversionResponse struct {
	Original  *ConversionDetails `json:"original,omitempty"`
	RequestID uuid.UUID          `json:"request_id,omitempty"`
	Reverse   *ConversionDetails `json:"reverse,omitempty"`
}

func (awx airwallex) ReverseSettledConversion(
	ctx context.Context,
	in *ReverseSettledConversionRequest,
) (out *ReverseSettledConversionResponse, err error) {
	out = new(ReverseSettledConversionResponse)
	uri := string(awx.apiEndpoint) + string(reverseSettledConversionEndpoint)
	if err = Do(ctx, awx.httpClient, http.MethodPost, uri, in, out); err != nil {
		return out, errors.Wrap(err, "Do()")
	}

	return out, err
}
