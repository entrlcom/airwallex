package airwallex

import (
	"context"
	"net/http"

	"github.com/google/uuid"
	"github.com/pkg/errors"
)

// https://www.airwallex.com/docs/api#/Core_Resources/Global_Accounts/_api_v1_global_accounts/get

type ListGlobalAccountsRequest struct {
	CountryCode   string              `json:"country_code,omitempty"`
	Currency      Currency            `json:"currency,omitempty"`
	FromCreatedAt *TimeISO8601        `json:"from_created_at,omitempty"`
	NickName      string              `json:"nick_name,omitempty"`
	PageNum       int32               `json:"page_num,omitempty"`
	PageSize      int32               `json:"page_size,omitempty"`
	Status        GlobalAccountStatus `json:"status,omitempty"`
	ToCreatedAt   *TimeISO8601        `json:"to_created_at,omitempty"`
}

type ListGlobalAccountsResponse struct {
	HasMore bool             `json:"has_more,omitempty"`
	Items   []*GlobalAccount `json:"items,omitempty"`
}

func (awx airwallex) ListGlobalAccounts(
	ctx context.Context,
	in *ListGlobalAccountsRequest,
) (out *ListGlobalAccountsResponse, err error) {
	out = new(ListGlobalAccountsResponse)
	uri := string(awx.apiEndpoint) + string(listGlobalAccountsEndpoint)
	if err = Do(ctx, awx.httpClient, http.MethodGet, uri, in, out); err != nil {
		return out, errors.Wrap(err, "Do()")
	}

	return out, err
}

// https://www.airwallex.com/docs/api#/Core_Resources/Global_Accounts/_api_v1_global_accounts__id_/get

type GetGlobalAccountRequest struct {
	ID uuid.UUID `json:"id,omitempty"`
}

type GetGlobalAccountResponse GlobalAccount

func (awx airwallex) GetGlobalAccount(
	ctx context.Context,
	in *GetGlobalAccountRequest,
) (out *GetGlobalAccountResponse, err error) {
	out = new(GetGlobalAccountResponse)
	uri := string(awx.apiEndpoint) + string(getGlobalAccountEndpoint)
	if err = Do(ctx, awx.httpClient, http.MethodGet, uri, in, out); err != nil {
		return out, errors.Wrap(err, "Do()")
	}

	return out, err
}

// https://www.airwallex.com/docs/api#/Core_Resources/Global_Accounts/_api_v1_global_accounts__id__generate_statement_letter/post

type GenerateGlobalAccountStatementLetterRequest struct {
	ID                   uuid.UUID            `json:"id,omitempty"`
	AccountStatementType AccountStatementType `json:"account_statement_type,omitempty"`
	RegistrationInfo     *RegistrationInfo    `json:"registration_info,omitempty"`
}

type GenerateGlobalAccountStatementLetterResponse string

func (awx airwallex) GenerateGlobalAccountStatementLetter(
	ctx context.Context,
	in *GenerateGlobalAccountStatementLetterRequest,
) (out *GenerateGlobalAccountStatementLetterResponse, err error) {
	out = new(GenerateGlobalAccountStatementLetterResponse)
	uri := string(awx.apiEndpoint) + string(generateGlobalAccountStatementLetterEndpoint)
	if err = Do(ctx, awx.httpClient, http.MethodPost, uri, in, out); err != nil {
		return out, errors.Wrap(err, "Do()")
	}

	return out, err
}

// https://www.airwallex.com/docs/api#/Core_Resources/Global_Accounts/_api_v1_global_accounts__id__transactions/get

type ListGlobalAccountTransactionsRequest struct {
	ID            uuid.UUID    `json:"id,omitempty"`
	FromCreatedAt *TimeISO8601 `json:"from_created_at,omitempty"`
	PageNum       int32        `json:"page_num,omitempty"`
	PageSize      int32        `json:"page_size,omitempty"`
	ToCreatedAt   *TimeISO8601 `json:"to_created_at,omitempty"`
}

type ListGlobalAccountTransactionsResponse struct {
	HasMore bool                        `json:"has_more,omitempty"`
	Items   []*GlobalAccountTransaction `json:"items,omitempty"`
}

func (awx airwallex) ListGlobalAccountTransactions(
	ctx context.Context,
	in *ListGlobalAccountTransactionsRequest,
) (out *ListGlobalAccountTransactionsResponse, err error) {
	out = new(ListGlobalAccountTransactionsResponse)
	uri := string(awx.apiEndpoint) + string(listGlobalAccountTransactionsEndpoint)
	if err = Do(ctx, awx.httpClient, http.MethodGet, uri, in, out); err != nil {
		return out, errors.Wrap(err, "Do()")
	}

	return out, err
}

// https://www.airwallex.com/docs/api#/Core_Resources/Global_Accounts/_api_v1_global_accounts_create/post

type CreateGlobalAccountRequest struct {
	CountryCode    string                       `json:"country_code,omitempty"`
	Currency       Currency                     `json:"currency,omitempty"`
	NickName       string                       `json:"nick_name,omitempty"`
	PaymentMethods []GlobalAccountPaymentMethod `json:"payment_methods,omitempty"`
	RequestID      uuid.UUID                    `json:"request_id,omitempty"`
}

type CreateGlobalAccountResponse GlobalAccount

func (awx airwallex) CreateGlobalAccount(
	ctx context.Context,
	in *CreateGlobalAccountRequest,
) (out *CreateGlobalAccountResponse, err error) {
	out = new(CreateGlobalAccountResponse)
	uri := string(awx.apiEndpoint) + string(createGlobalAccountEndpoint)
	if err = Do(ctx, awx.httpClient, http.MethodPost, uri, in, out); err != nil {
		return out, errors.Wrap(err, "Do()")
	}

	return out, err
}

// https://www.airwallex.com/docs/api#/Core_Resources/Global_Accounts/_api_v1_global_accounts_update__id_/post

type UpdateGlobalAccountRequest struct {
	ID       uuid.UUID `json:"id,omitempty"`
	NickName string    `json:"nick_name,omitempty"`
}

type UpdateGlobalAccountResponse GlobalAccount

func (awx airwallex) UpdateGlobalAccount(
	ctx context.Context,
	in *UpdateGlobalAccountRequest,
) (out *UpdateGlobalAccountResponse, err error) {
	out = new(UpdateGlobalAccountResponse)
	uri := string(awx.apiEndpoint) + string(updateGlobalAccountEndpoint)
	if err = Do(ctx, awx.httpClient, http.MethodPost, uri, in, out); err != nil {
		return out, errors.Wrap(err, "Do()")
	}

	return out, err
}
