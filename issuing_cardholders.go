package airwallex

import (
	"context"
	"net/http"

	"github.com/pkg/errors"
)

// https://www.airwallex.com/docs/api#/Issuing/Cardholders/_api_v1_issuing_cardholders_create/post

type CreateCardholderRequest struct {
	Address       *CardholderAddress `json:"address,omitempty"`
	Email         string             `json:"email,omitempty"`
	Individual    *Individual        `json:"individual,omitempty"`
	MobileNumber  string             `json:"mobile_number,omitempty"`
	PostalAddress *CardholderAddress `json:"postal_address,omitempty"`
}

type CreateCardholderResponse Cardholder

func (awx airwallex) CreateCardholder(
	ctx context.Context,
	in *CreateCardholderRequest,
) (out *CreateCardholderResponse, err error) {
	out = new(CreateCardholderResponse)
	uri := string(awx.apiEndpoint) + string(createCardholderEndpoint)
	if err = Do(ctx, awx.httpClient, http.MethodPost, uri, in, out); err != nil {
		return out, errors.Wrap(err, "Do()")
	}

	return out, err
}

// https://www.airwallex.com/docs/api#/Issuing/Cardholders/_api_v1_issuing_cardholders/get

type ListCardholdersRequest struct {
	CardholderStatus string `json:"cardholder_status,omitempty"`
	PageNum          int32  `json:"page_num,omitempty"`
	PageSize         int32  `json:"page_size,omitempty"`
}

type ListCardholdersResponse struct {
	HasMore bool          `json:"has_more,omitempty"`
	Items   []*Cardholder `json:"items,omitempty"`
}

func (awx airwallex) ListCardholders(
	ctx context.Context,
	in *ListCardholdersRequest,
) (out *ListCardholdersResponse, err error) {
	out = new(ListCardholdersResponse)
	uri := string(awx.apiEndpoint) + string(listCardholdersEndpoint)
	if err = Do(ctx, awx.httpClient, http.MethodGet, uri, in, out); err != nil {
		return out, errors.Wrap(err, "Do()")
	}

	return out, err
}

// https://www.airwallex.com/docs/api#/Issuing/Cardholders/_api_v1_issuing_cardholders__id_/get

type GetCardholderRequest struct {
	ID string `json:"id,omitempty"`
}

type GetCardholderResponse Cardholder

func (awx airwallex) GetCardholder(
	ctx context.Context,
	in *GetCardholderRequest,
) (out *GetCardholderResponse, err error) {
	out = new(GetCardholderResponse)
	uri := string(awx.apiEndpoint) + string(getCardholderEndpoint)
	if err = Do(ctx, awx.httpClient, http.MethodGet, uri, in, out); err != nil {
		return out, errors.Wrap(err, "Do()")
	}

	return out, err
}

// https://www.airwallex.com/docs/api#/Issuing/Cardholders/_api_v1_issuing_cardholders__id__update/post

type UpdateCardholderRequest struct {
	Address       *CardholderAddress `json:"address,omitempty"`
	ID            string             `json:"id,omitempty"`
	Individual    *Individual        `json:"individual,omitempty"`
	MobileNumber  string             `json:"mobile_number,omitempty"`
	PostalAddress *CardholderAddress `json:"postal_address,omitempty"`
}

type UpdateCardholderResponse Cardholder

func (awx airwallex) UpdateCardholder(
	ctx context.Context,
	in *UpdateCardholderRequest,
) (out *UpdateCardholderResponse, err error) {
	out = new(UpdateCardholderResponse)
	uri := string(awx.apiEndpoint) + string(updateCardholderEndpoint)
	if err = Do(ctx, awx.httpClient, http.MethodPost, uri, in, out); err != nil {
		return out, errors.Wrap(err, "Do()")
	}

	return out, err
}
