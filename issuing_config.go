package airwallex

import (
	"context"
	"net/http"

	"github.com/pkg/errors"
)

// https://www.airwallex.com/docs/api#/Issuing/Config/_api_v1_issuing_config/get

type IssuingConfigResponse IssuingConfig

func (awx airwallex) GetIssuingConfig(
	ctx context.Context,
) (out *IssuingConfigResponse, err error) {
	out = new(IssuingConfigResponse)
	uri := string(awx.apiEndpoint) + string(getIssuingConfigEndpoint)
	if err = Do(ctx, awx.httpClient, http.MethodGet, uri, nil, out); err != nil {
		return out, errors.Wrap(err, "Do()")
	}

	return out, err
}
