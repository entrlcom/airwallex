package airwallex

import (
	"bytes"
	"context"
	"encoding/json"
	"io/ioutil"
	"log"
	"net/http"
	"net/url"
	"regexp"
	"strings"

	"github.com/google/go-querystring/query"
	"github.com/pkg/errors"
)

const userAgent = "airwallex-go"

var re = regexp.MustCompile(`{(.*?)}`)

type Transport struct {
	apiEndpoint                               ApiEndpoint
	apiKey, clientID, connectedAccount, token string
}

func (t *Transport) Login(ctx context.Context, ht *http.Transport) (err error) {
	uri := string(t.apiEndpoint) + string(loginEndpoint)

	var r *http.Request
	if r, err = http.NewRequestWithContext(ctx, http.MethodPost, uri, nil); err != nil {
		return errors.Wrap(err, "http.NewRequestWithContext()")
	}

	r.Header.Add("Content-Type", "application/json")
	r.Header.Add("User-Agent", userAgent)
	r.Header.Add("x-api-key", t.apiKey)
	r.Header.Add("x-client-id", t.clientID)

	var resp *http.Response
	if resp, err = ht.RoundTrip(r); err != nil {
		return errors.Wrap(err, "ht.RoundTrip()")
	}
	defer func() { _ = resp.Body.Close() }()

	var out LoginResponse
	if err = json.NewDecoder(resp.Body).Decode(&out); err != nil {
		body, _ := ioutil.ReadAll(resp.Body)
		log.Println("[BODY]", string(body))
		return errors.Wrap(err, "json.NewDecoder().Decode()")
	}

	t.token = out.Token

	return err
}

func (t *Transport) RoundTrip(in *http.Request) (out *http.Response, err error) {
	body, err := in.GetBody()
	if err != nil {
		err = errors.Wrap(err, "in.GetBody()")
		return
	}

	in.Header.Set("Authorization", "Bearer "+t.token)
	if len(t.connectedAccount) != 0 {
		in.Header.Set("x-on-behalf-of", t.connectedAccount)
	}

	ht := new(http.Transport)
	if out, err = ht.RoundTrip(in); err != nil {
		err = errors.Wrap(err, "ht.RoundTrip()")
		return
	}
	if out.StatusCode != http.StatusUnauthorized {
		return
	}

	if err = t.Login(in.Context(), ht); err != nil {
		err = errors.Wrap(err, "t.Login()")
		return
	}

	in.Header.Set("Authorization", "Bearer "+t.token)
	in.Body = body
	if out, err = ht.RoundTrip(in); err != nil {
		err = errors.Wrap(err, "ht.RoundTrip()")
		return
	}

	return
}

func Do(
	ctx context.Context,
	client *http.Client,
	method, uri string,
	in, out interface{},
) (err error) {
	var payload []byte

	if in != nil {
		var u *url.URL
		if u, err = url.ParseRequestURI(uri); err != nil {
			return errors.Wrap(err, "url.ParseRequestURI()")
		}

		items := re.FindAllString(uri, -1)
		params := make(map[string]string, len(items))
		for _, item := range items {
			item = strings.Trim(item, "{")
			item = strings.Trim(item, "}")
			params[item] = ""
		}

		switch method {
		case http.MethodGet:
			var queryParams url.Values
			if queryParams, err = url.ParseQuery(u.RawQuery); err != nil {
				return errors.Wrap(err, "url.ParseQuery()")
			}

			var values url.Values
			if values, err = query.Values(in); err != nil {
				return errors.Wrap(err, "query.Values()")
			}

			for k, v := range values {
				if _, ok := params[k]; ok {
					if len(v) > 0 {
						params[k] = v[0]
					}

					continue
				}

				for _, value := range v {
					if len(value) > 0 {
						queryParams.Add(k, value)
					}
				}
			}

			u.RawQuery = queryParams.Encode()
			uri = u.String()
		case http.MethodPost:
			if payload, err = json.Marshal(in); err != nil {
				return errors.Wrap(err, "json.Marshal()")
			}
		}

		if len(params) > 0 {
			oldnews := make([]string, len(params)*2)
			for k, v := range params {
				oldnews = append(oldnews, "{"+k+"}")
				oldnews = append(oldnews, v)
			}
			r := strings.NewReplacer(oldnews...)
			uri = r.Replace(uri)
		}
	}

	var r *http.Request
	if r, err = http.NewRequestWithContext(ctx, method, uri, bytes.NewBuffer(payload)); err != nil {
		return errors.Wrap(err, "http.NewRequestWithContext()")
	}

	r.Header.Add("Content-Type", "application/json")
	r.Header.Add("User-Agent", userAgent)

	var resp *http.Response
	if resp, err = client.Do(r); err != nil {
		return errors.Wrap(err, "a.client.Do()")
	}
	defer func() { _ = resp.Body.Close() }()

	if out != nil {
		if err = json.NewDecoder(resp.Body).Decode(&out); err != nil {
			return errors.Wrap(err, "json.NewDecoder().Decode()")
		}
	}

	return err
}
