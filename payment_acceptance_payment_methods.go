package airwallex

import (
	"context"
	"net/http"

	"github.com/google/uuid"
	"github.com/pkg/errors"
)

// https://www.airwallex.com/docs/api#/Payment_Acceptance/Payment_Methods/_api_v1_pa_payment_methods_create/post

type CreatePaymentMethodRequest struct {
	RequestID  uuid.UUID          `json:"request_id,omitempty"`
	CustomerID string             `json:"customer_id,omitempty"`
	Type       PaymentMethodName  `json:"type,omitempty"`
	Card       *PaymentMethodCard `json:"card,omitempty"`
	Metadata   Metadata           `json:"metadata,omitempty"`
}

type CreatePaymentMethodResponse struct {
	PaymentMethod
	RequestID uuid.UUID `json:"request_id,omitempty"`
}

func (awx airwallex) CreatePaymentMethod(
	ctx context.Context,
	in *CreatePaymentMethodRequest,
) (out *CreatePaymentMethodResponse, err error) {
	out = new(CreatePaymentMethodResponse)
	uri := string(awx.apiEndpoint) + string(createPaymentMethodEndpoint)
	if err = Do(ctx, awx.httpClient, http.MethodPost, uri, in, out); err != nil {
		return out, errors.Wrap(err, "Do()")
	}

	return out, err
}

// https://www.airwallex.com/docs/api#/Payment_Acceptance/Payment_Methods/_api_v1_pa_payment_methods__id_/get

type GetPaymentMethodRequest struct {
	ID string `json:"id,omitempty"`
}

type GetPaymentMethodResponse struct {
	PaymentMethod
	RequestID uuid.UUID `json:"request_id,omitempty"`
}

func (awx airwallex) GetPaymentMethod(
	ctx context.Context,
	in *GetPaymentMethodRequest,
) (out *GetPaymentMethodResponse, err error) {
	out = new(GetPaymentMethodResponse)
	uri := string(awx.apiEndpoint) + string(getPaymentMethodEndpoint)
	if err = Do(ctx, awx.httpClient, http.MethodGet, uri, in, out); err != nil {
		return out, errors.Wrap(err, "Do()")
	}

	return out, err
}

// https://www.airwallex.com/docs/api#/Payment_Acceptance/Payment_Methods/_api_v1_pa_payment_methods__id__update/post

type UpdatePaymentMethodRequest struct {
	Card      *PaymentMethodCard `json:"card,omitempty"`
	ID        string             `json:"id,omitempty"`
	Metadata  Metadata           `json:"metadata,omitempty"`
	RequestID uuid.UUID          `json:"request_id,omitempty"`
}

type UpdatePaymentMethodResponse struct {
	PaymentMethod
	RequestID uuid.UUID `json:"request_id,omitempty"`
}

func (awx airwallex) UpdatePaymentMethod(
	ctx context.Context,
	in *UpdatePaymentMethodRequest,
) (out *UpdatePaymentMethodResponse, err error) {
	out = new(UpdatePaymentMethodResponse)
	uri := string(awx.apiEndpoint) + string(updatePaymentMethodEndpoint)
	if err = Do(ctx, awx.httpClient, http.MethodPost, uri, in, out); err != nil {
		return out, errors.Wrap(err, "Do()")
	}

	return out, err
}

// https://www.airwallex.com/docs/api#/Payment_Acceptance/Payment_Methods/_api_v1_pa_payment_methods/get

type ListPaymentMethodsRequest struct {
	CustomerID    string            `json:"customer_id,omitempty"`
	FromCreatedAt *TimeISO8601      `json:"from_created_at,omitempty"`
	PageNum       int32             `json:"page_num,omitempty"`
	PageSize      int32             `json:"page_size,omitempty"`
	RetrievalRef  string            `json:"retrieval_ref,omitempty"`
	ToCreatedAt   *TimeISO8601      `json:"to_created_at,omitempty"`
	Type          PaymentMethodName `json:"type,omitempty"`
}

type ListPaymentMethodsResponse struct {
	HasMore bool             `json:"has_more,omitempty"`
	Items   []*PaymentMethod `json:"items,omitempty"`
}

func (awx airwallex) ListPaymentMethods(
	ctx context.Context,
	in *ListPaymentMethodsRequest,
) (out *ListPaymentMethodsResponse, err error) {
	out = new(ListPaymentMethodsResponse)
	uri := string(awx.apiEndpoint) + string(listPaymentMethodsEndpoint)
	if err = Do(ctx, awx.httpClient, http.MethodGet, uri, in, out); err != nil {
		return out, errors.Wrap(err, "Do()")
	}

	return out, err
}

// https://www.airwallex.com/docs/api#/Payment_Acceptance/Payment_Methods/_api_v1_pa_payment_methods__id__disable/post

type DisablePaymentMethodRequest struct {
	ID        string    `json:"id,omitempty"`
	RequestID uuid.UUID `json:"request_id,omitempty"`
}

type DisablePaymentMethodResponse struct {
	PaymentMethod
	RequestID uuid.UUID `json:"request_id,omitempty"`
}

func (awx airwallex) DisablePaymentMethod(
	ctx context.Context,
	in *DisablePaymentMethodRequest,
) (out *DisablePaymentMethodResponse, err error) {
	out = new(DisablePaymentMethodResponse)
	uri := string(awx.apiEndpoint) + string(disablePaymentMethodEndpoint)
	if err = Do(ctx, awx.httpClient, http.MethodPost, uri, in, out); err != nil {
		return out, errors.Wrap(err, "Do()")
	}

	return out, err
}
