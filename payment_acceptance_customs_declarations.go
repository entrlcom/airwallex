package airwallex

import (
	"context"
	"net/http"

	"github.com/google/uuid"
	"github.com/pkg/errors"
)

// https://www.airwallex.com/docs/api#/Payment_Acceptance/Customs_Declarations/_api_v1_pa_customs_declarations_create/post

type CreateCustomsDeclarationRequest struct {
	CustomsDetails  *CustomsDetails `json:"customs_details,omitempty"`
	PaymentIntentID string          `json:"payment_intent_id,omitempty"`
	RequestID       uuid.UUID       `json:"request_id,omitempty"`
	SubOrder        *SubOrder       `json:"sub_order,omitempty"`
	ShopperDetails  *ShopperDetails `json:"shopper_details,omitempty"`
}

type CreateCustomsDeclarationResponse struct {
	CustomsDeclaration
	RequestID string `json:"request_id,omitempty"`
}

func (awx airwallex) CreateCustomsDeclaration(
	ctx context.Context,
	in *CreateCustomsDeclarationRequest,
) (out *CreateCustomsDeclarationResponse, err error) {
	out = new(CreateCustomsDeclarationResponse)
	uri := string(awx.apiEndpoint) + string(createCustomsDeclarationEndpoint)
	if err = Do(ctx, awx.httpClient, http.MethodPost, uri, in, out); err != nil {
		return out, errors.Wrap(err, "Do()")
	}

	return out, err
}

// https://www.airwallex.com/docs/api#/Payment_Acceptance/Customs_Declarations/_api_v1_pa_customs_declarations__id__update/post

type UpdateCustomsDeclarationRequest struct {
	CustomsCode           string          `json:"customs_code,omitempty"`
	ID                    string          `json:"id,omitempty"`
	MerchantCustomsName   string          `json:"merchant_customs_name,omitempty"`
	MerchantCustomsNumber string          `json:"merchant_customs_number,omitempty"`
	RequestID             uuid.UUID       `json:"request_id,omitempty"`
	ShopperDetails        *ShopperDetails `json:"shopper_details,omitempty"`
	SubOrderAmount        Decimal         `json:"sub_order_amount,omitempty"`
}

type UpdateCustomsDeclarationResponse struct {
	CustomsDeclaration
	RequestID uuid.UUID `json:"request_id,omitempty"`
}

func (awx airwallex) UpdateCustomsDeclaration(
	ctx context.Context,
	in *UpdateCustomsDeclarationRequest,
) (out *UpdateCustomsDeclarationResponse, err error) {
	out = new(UpdateCustomsDeclarationResponse)
	uri := string(awx.apiEndpoint) + string(updateCustomsDeclarationEndpoint)
	if err = Do(ctx, awx.httpClient, http.MethodPost, uri, in, out); err != nil {
		return out, errors.Wrap(err, "Do()")
	}

	return out, err
}

// https://www.airwallex.com/docs/api#/Payment_Acceptance/Customs_Declarations/_api_v1_pa_customs_declarations__id__redeclare/post

type RedeclareCustomsDeclarationRequest struct {
	ID        string    `json:"id,omitempty"`
	RequestID uuid.UUID `json:"request_id,omitempty"`
}

type RedeclareCustomsDeclarationResponse struct {
	CustomsDeclaration
	RequestID uuid.UUID `json:"request_id,omitempty"`
}

func (awx airwallex) RedeclareCustomsDeclaration(
	ctx context.Context,
	in *RedeclareCustomsDeclarationRequest,
) (out *RedeclareCustomsDeclarationResponse, err error) {
	out = new(RedeclareCustomsDeclarationResponse)
	uri := string(awx.apiEndpoint) + string(redeclareCustomsDeclarationEndpoint)
	if err = Do(ctx, awx.httpClient, http.MethodPost, uri, in, out); err != nil {
		return out, errors.Wrap(err, "Do()")
	}

	return out, err
}

// https://www.airwallex.com/docs/api#/Payment_Acceptance/Customs_Declarations/_api_v1_pa_customs_declarations__id__redeclare/post

type GetCustomsDeclarationRequest struct {
	ID string `json:"id,omitempty"`
}

type GetCustomsDeclarationResponse struct {
	CustomsDeclaration
	RequestID uuid.UUID `json:"request_id,omitempty"`
}

func (awx airwallex) GetCustomsDeclaration(
	ctx context.Context,
	in *GetCustomsDeclarationRequest,
) (out *GetCustomsDeclarationResponse, err error) {
	out = new(GetCustomsDeclarationResponse)
	uri := string(awx.apiEndpoint) + string(getCustomsDeclarationEndpoint)
	if err = Do(ctx, awx.httpClient, http.MethodGet, uri, in, out); err != nil {
		return out, errors.Wrap(err, "Do()")
	}

	return out, err
}
