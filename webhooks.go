package airwallex

const (
	EventNameCustomerCreated                        EventName = "customer.created"
	EventNameCustomerUpdated                        EventName = "customer.updated"
	EventNameDisputeAccepted                        EventName = "dispute.accepted"
	EventNameDisputeDisputeReceivedByMerchant       EventName = "dispute.dispute_received_by_merchant"
	EventNameDisputeDisputeRespondedByMerchant      EventName = "dispute.dispute_responded_by_merchant"
	EventNameDisputeDisputeReversed                 EventName = "dispute.dispute_reversed"
	EventNameDisputeLost                            EventName = "dispute.lost"
	EventNameDisputeRFIReceivedByMerchant           EventName = "dispute.rfi_received_by_merchant"
	EventNameDisputeRFIRespondedByMerchant          EventName = "dispute.rfi_responded_by_merchant"
	EventNameDisputeWon                             EventName = "dispute.won"
	EventNamePaymentAttemptAuthenticationFailed     EventName = "payment_attempt.authentication_failed"
	EventNamePaymentAttemptAuthenticationRedirected EventName = "payment_attempt.authentication_redirected"
	EventNamePaymentAttemptAuthorizationFailed      EventName = "payment_attempt.authorization_failed"
	EventNamePaymentAttemptAuthorized               EventName = "payment_attempt.authorized"
	EventNamePaymentAttemptCancelled                EventName = "payment_attempt.cancelled"
	EventNamePaymentAttemptCaptureFailed            EventName = "payment_attempt.capture_failed"
	EventNamePaymentAttemptCaptureRequested         EventName = "payment_attempt.capture_requested"
	EventNamePaymentAttemptExpired                  EventName = "payment_attempt.expired"
	EventNamePaymentAttemptFailedToProcess          EventName = "payment_attempt.failed_to_process"
	EventNamePaymentAttemptPaid                     EventName = "payment_attempt.paid"
	EventNamePaymentAttemptReceived                 EventName = "payment_attempt.received"
	EventNamePaymentAttemptRiskDeclined             EventName = "payment_attempt.risk_declined"
	EventNamePaymentAttemptSettled                  EventName = "payment_attempt.settled"
	EventNamePaymentConsentCreated                  EventName = "payment_consent.created"
	EventNamePaymentConsentDisabled                 EventName = "payment_consent.disabled"
	EventNamePaymentConsentUpdated                  EventName = "payment_consent.updated"
	EventNamePaymentConsentVerified                 EventName = "payment_consent.verified"
	EventNamePaymentIntentCancelled                 EventName = "payment_intent.cancelled"
	EventNamePaymentIntentCreate                    EventName = "payment_intent.created"
	EventNamePaymentIntentRequiresCapture           EventName = "payment_intent.requires_capture"
	EventNamePaymentIntentRequiresCustomerAction    EventName = "payment_intent.requires_customer_action"
	EventNamePaymentIntentRequiresPaymentMethod     EventName = "payment_intent.requires_payment_method"
	EventNamePaymentIntentSucceeded                 EventName = "payment_intent.succeeded"
	EventNamePaymentMethodAttached                  EventName = "payment_method.attached"
	EventNamePaymentMethodCreated                   EventName = "payment_method.created"
	EventNamePaymentMethodDetached                  EventName = "payment_method.detached"
	EventNamePaymentMethodDisabled                  EventName = "payment_method.disabled"
	EventNamePaymentMethodUpdated                   EventName = "payment_method.updated"
	EventNameRefundAccepted                         EventName = "refund.accepted"
	EventNameRefundFailed                           EventName = "refund.failed"
	EventNameRefundReceived                         EventName = "refund.received"
	EventNameRefundSucceeded                        EventName = "refund.succeeded"
)

type DataError struct {
	Code    string `json:"code,omitempty"`
	Message string `json:"message,omitempty"`
	Source  string `json:"source,omitempty"`
}

type DataObject interface {
	isDataObject()
}

type EventName string

type WebhookPayload struct {
	AccountId string              `json:"account_id,omitempty"`
	CreatedAt *TimeISO8601        `json:"created_at,omitempty"`
	Data      *WebhookPayloadData `json:"data,omitempty"`
	ID        string              `json:"id,omitempty"`
	Name      EventName           `json:"name,omitempty"`
}

type WebhookPayloadData struct {
	Error  *DataError `json:"error,omitempty"`
	Object DataObject `json:"object,omitempty"`
}
