package airwallex

import (
	"context"
	"net/http"

	"github.com/google/uuid"
	"github.com/pkg/errors"
)

// https://www.airwallex.com/docs/api#/Scale/Charges/_api_v1_charges/get

type ListChargesRequest struct {
	Currency      Currency     `json:"currency,omitempty"`
	FromCreatedAt *TimeISO8601 `json:"from_created_at,omitempty"`
	PageNum       int32        `json:"page_num,omitempty"`
	PageSize      int32        `json:"page_size,omitempty"`
	RequestID     uuid.UUID    `json:"request_id,omitempty"`
	Source        string       `json:"source,omitempty"`
	Status        ChargeStatus `json:"status,omitempty"`
	ToCreatedAt   *TimeISO8601 `json:"to_created_at,omitempty"`
}

type ListChargesResponse struct {
	HasMore bool      `json:"has_more,omitempty"`
	Items   []*Charge `json:"items,omitempty"`
}

func (awx airwallex) ListCharges(
	ctx context.Context,
	in *ListChargesRequest,
) (out *ListChargesResponse, err error) {
	out = new(ListChargesResponse)
	uri := string(awx.apiEndpoint) + string(listChargesEndpoint)
	if err = Do(ctx, awx.httpClient, http.MethodGet, uri, in, out); err != nil {
		return out, errors.Wrap(err, "Do()")
	}

	return out, err
}

// https://www.airwallex.com/docs/api#/Scale/Charges/_api_v1_charges__id_/get

type GetChargeRequest struct {
	ID string `json:"id,omitempty"`
}

type GetChargeResponse Charge

func (awx airwallex) GetCharge(
	ctx context.Context,
	in *GetChargeRequest,
) (out *GetChargeResponse, err error) {
	out = new(GetChargeResponse)
	uri := string(awx.apiEndpoint) + string(getChargeEndpoint)
	if err = Do(ctx, awx.httpClient, http.MethodGet, uri, in, out); err != nil {
		return out, errors.Wrap(err, "Do()")
	}

	return out, err
}

// https://www.airwallex.com/docs/api#/Scale/Charges/_api_v1_charges_create/post

type CreateChargeRequest struct {
	Amount    Decimal   `json:"amount,omitempty"`
	Currency  Currency  `json:"currency,omitempty"`
	Reason    string    `json:"reason,omitempty"`
	Reference string    `json:"reference,omitempty"`
	RequestID uuid.UUID `json:"request_id,omitempty"`
	Source    string    `json:"source,omitempty"`
}

type CreateChargeResponse Charge

func (awx airwallex) CreateCharge(
	ctx context.Context,
	in *CreateChargeRequest,
) (out *CreateChargeResponse, err error) {
	out = new(CreateChargeResponse)
	uri := string(awx.apiEndpoint) + string(createChargeEndpoint)
	if err = Do(ctx, awx.httpClient, http.MethodPost, uri, in, out); err != nil {
		return out, errors.Wrap(err, "Do()")
	}

	return out, err
}
