package airwallex

import (
	"context"
	"net/http"

	"github.com/pkg/errors"
)

// https://www.airwallex.com/docs/api#/Payment_Acceptance/Config/_api_v1_pa_config_payment_method_types/get

type ListPaymentMethodTypesRequest struct {
	Active              bool            `json:"active,omitempty"`
	CountryCode         string          `json:"country_code,omitempty"`
	TransactionCurrency Currency        `json:"transaction_currency,omitempty"`
	TransactionMode     TransactionMode `json:"transaction_mode,omitempty"`
}

type ListPaymentMethodTypesResponse struct {
	HasMore bool                       `json:"has_more,omitempty"`
	Items   []*ConfigPaymentMethodType `json:"items,omitempty"`
}

func (awx airwallex) ListPaymentMethodTypes(
	ctx context.Context,
	in *ListPaymentMethodTypesRequest,
) (out *ListPaymentMethodTypesResponse, err error) {
	out = new(ListPaymentMethodTypesResponse)
	uri := string(awx.apiEndpoint) + string(listPaymentMethodTypesEndpoint)
	if err = Do(ctx, awx.httpClient, http.MethodGet, uri, in, out); err != nil {
		return out, errors.Wrap(err, "Do()")
	}

	return out, err
}

// https://www.airwallex.com/docs/api#/Payment_Acceptance/Config/_api_v1_pa_config_banks/get

type ListBanksRequest struct {
	CountryCode       string            `json:"country_code,omitempty"`
	PaymentMethodType PaymentMethodName `json:"payment_method_type,omitempty"`
}

type ListBanksResponse struct {
	HasMore bool          `json:"has_more,omitempty"`
	Items   []*ConfigBank `json:"items,omitempty"`
}

func (awx airwallex) ListBanks(
	ctx context.Context,
	in *ListBanksRequest,
) (out *ListBanksResponse, err error) {
	out = new(ListBanksResponse)
	uri := string(awx.apiEndpoint) + string(listBanksEndpoint)
	if err = Do(ctx, awx.httpClient, http.MethodGet, uri, in, out); err != nil {
		return out, errors.Wrap(err, "Do()")
	}

	return out, err
}
