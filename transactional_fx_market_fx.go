package airwallex

import (
	"context"
	"net/http"

	"github.com/pkg/errors"
)

// https://www.airwallex.com/docs/api#/Transactional_FX/Lock_FX/_api_v1_lockfx_create/post

type GetIndicativeMarketQuoteRequest struct {
	BuyCurrency    Currency `json:"buy_currency,omitempty"`
	SellCurrency   Currency `json:"sell_currency,omitempty"`
	BuyAmount      Decimal  `json:"buy_amount,omitempty"`
	ConversionDate string   `json:"conversion_date,omitempty"` // TODO: Time (YYYY-MM-DD)
	SellAmount     Decimal  `json:"sell_amount,omitempty"`
}

type GetIndicativeMarketQuoteResponse MarketFXQuote

func (awx airwallex) GetIndicativeMarketQuote(
	ctx context.Context,
	in *GetIndicativeMarketQuoteRequest,
) (out *GetIndicativeMarketQuoteResponse, err error) {
	out = new(GetIndicativeMarketQuoteResponse)
	uri := string(awx.apiEndpoint) + string(getIndicativeMarketQuoteEndpoint)
	if err = Do(ctx, awx.httpClient, http.MethodGet, uri, in, out); err != nil {
		return out, errors.Wrap(err, "Do()")
	}

	return out, err
}
