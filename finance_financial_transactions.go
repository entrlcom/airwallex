package airwallex

import (
	"context"
	"net/http"

	"github.com/google/uuid"
	"github.com/pkg/errors"
)

// https://www.airwallex.com/docs/api#/Finance/Financial_Transactions/_api_v1_financial_transactions/get

type ListFinancialTransactionsRequest struct {
	BatchID       string            `json:"batch_id,omitempty"`
	FromCreatedAt *TimeISO8601      `json:"from_created_at,omitempty"`
	PageNum       int32             `json:"page_num,omitempty"`
	PageSize      int32             `json:"page_size,omitempty"`
	SourceID      uuid.UUID         `json:"source_id,omitempty"`
	Status        TransactionStatus `json:"status,omitempty"`
	ToCreatedAt   *TimeISO8601      `json:"to_created_at,omitempty"`
}

type ListFinancialTransactionsResponse struct {
	HasMore bool                    `json:"has_more,omitempty"`
	Items   []*FinancialTransaction `json:"items,omitempty"`
}

func (awx airwallex) ListFinancialTransactions(
	ctx context.Context,
	in *ListFinancialTransactionsRequest,
) (out *ListFinancialTransactionsResponse, err error) {
	out = new(ListFinancialTransactionsResponse)
	uri := string(awx.apiEndpoint) + string(listFinancialTransactionsEndpoint)
	if err = Do(ctx, awx.httpClient, http.MethodGet, uri, in, out); err != nil {
		return out, errors.Wrap(err, "Do()")
	}

	return out, err
}

// https://www.airwallex.com/docs/api#/Finance/Financial_Transactions/_api_v1_financial_transactions__id_/get

type GetFinancialTransactionRequest struct {
	ID string `json:"id,omitempty"`
}

type GetFinancialTransactionResponse FinancialTransaction

func (awx airwallex) GetFinancialTransaction(
	ctx context.Context,
	in *GetFinancialTransactionRequest,
) (out *GetFinancialTransactionResponse, err error) {
	out = new(GetFinancialTransactionResponse)
	uri := string(awx.apiEndpoint) + string(getFinancialTransactionEndpoint)
	if err = Do(ctx, awx.httpClient, http.MethodGet, uri, in, out); err != nil {
		return out, errors.Wrap(err, "Do()")
	}

	return out, err
}
