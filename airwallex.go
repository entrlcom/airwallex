package airwallex

import (
	"context"
	"net/http"
	"time"
)

type ApiEndpoint string

const (
	apiDemoEndpoint ApiEndpoint = "https://api-demo.airwallex.com/api/v1"
	apiEndpoint     ApiEndpoint = "https://airwallex.com/api/v1"
)

type Endpoint string

const (
	loginEndpoint                                Endpoint = "/authentication/login"
	getCurrentBalancesEndpoint                   Endpoint = "/balances/current"
	getBalanceHistoryEndpoint                    Endpoint = "/balances/history"
	listDepositsEndpoint                         Endpoint = "/deposits"
	listGlobalAccountsEndpoint                   Endpoint = "/global_accounts"
	getGlobalAccountEndpoint                     Endpoint = "/global_accounts/{id}"
	generateGlobalAccountStatementLetterEndpoint Endpoint = "/global_accounts/{id}/generate_statement_letter"
	listGlobalAccountTransactionsEndpoint        Endpoint = "/global_accounts/{id}/transactions"
	createGlobalAccountEndpoint                  Endpoint = "/global_accounts/create"
	updateGlobalAccountEndpoint                  Endpoint = "/global_accounts/create/{id}"
	listFinancialTransactionsEndpoint            Endpoint = "/financial_transactions"
	getFinancialTransactionEndpoint              Endpoint = "/financial_transactions/{id}"
	listSettlementsEndpoint                      Endpoint = "/pa/financial/settlements"
	getSettlementEndpoint                        Endpoint = "/pa/financial/settlements/{id}"
	getSettlementReportEndpoint                  Endpoint = "/pa/financial/settlements/{id}/report"
	listAuthorizationsEndpoint                   Endpoint = "/issuing/authorizations"
	getAuthorizationEndpoint                     Endpoint = "/issuing/authorizations/{id}"
	createCardholderEndpoint                     Endpoint = "/issuing/cardholders/create"
	listCardholdersEndpoint                      Endpoint = "/issuing/cardholders"
	getCardholderEndpoint                        Endpoint = "/issuing/cardholders/{id}"
	updateCardholderEndpoint                     Endpoint = "/issuing/cardholders/{id}/update"
	createCardEndpoint                           Endpoint = "/issuing/cards/create"
	getSensitiveCardDetailsEndpoint              Endpoint = "/issuing/cards/{id}/details"
	listCardsEndpoint                            Endpoint = "/issuing/cards"
	activateCardEndpoint                         Endpoint = "/issuing/cards/{id}/activate"
	getCardEndpoint                              Endpoint = "/issuing/cards/{id}"
	getCardLimitsEndpoint                        Endpoint = "/issuing/cards/{id}/limits"
	updateCardEndpoint                           Endpoint = "/issuing/cards/{id}/update"
	getIssuingConfigEndpoint                     Endpoint = "/issuing/config"
	listTransactionsEndpoint                     Endpoint = "/issuing/transactions"
	getTransactionEndpoint                       Endpoint = "/issuing/transactions/{id}"
	createCustomerEndpoint                       Endpoint = "/pa/customers/create"
	getCustomerEndpoint                          Endpoint = "/pa/customers/{id}"
	updateCustomerEndpoint                       Endpoint = "/pa/customers/{id}/update"
	generateCustomerClientSecretEndpoint         Endpoint = "/pa/customers/{id}/generate_client_secret"
	addCustomerPaymentMethodEndpoint             Endpoint = "/pa/customers/{id}/add_payment_method"
	deleteCustomerPaymentMethodEndpoint          Endpoint = "/pa/customers/{id}/remove_payment_method"
	listCustomersEndpoint                        Endpoint = "/pa/customers"
	listPaymentMethodTypesEndpoint               Endpoint = "/pa/config/payment_method_types"
	listBanksEndpoint                            Endpoint = "/pa/config/banks"
	createCustomsDeclarationEndpoint             Endpoint = "/pa/customs_declarations/create"
	updateCustomsDeclarationEndpoint             Endpoint = "/pa/customs_declarations/{id}/update"
	redeclareCustomsDeclarationEndpoint          Endpoint = "/pa/customs_declarations/{id}/redeclare"
	getCustomsDeclarationEndpoint                Endpoint = "/pa/customs_declarations/{id}"
	createFundsSplitEndpoint                     Endpoint = "/pa/funds_splits/create"
	getFundsSplitEndpoint                        Endpoint = "/pa/funds_splits/{id}"
	listFundsSplitsEndpoint                      Endpoint = "/pa/funds_splits"
	releaseFundsSplitEndpoint                    Endpoint = "/pa/funds_splits/{id}/release"
	createPaymentConsentEndpoint                 Endpoint = "/pa/payment_consents/create"
	updatePaymentConsentEndpoint                 Endpoint = "/pa/payment_consents/{id}/update"
	verifyPaymentConsentEndpoint                 Endpoint = "/pa/payment_consents/{id}/verify"
	disablePaymentConsentEndpoint                Endpoint = "/pa/payment_consents/{id}/disable"
	getPaymentConsentEndpoint                    Endpoint = "/pa/payment_consents/{id}"
	listPaymentConsentsEndpoint                  Endpoint = "/pa/payment_consents"
	getPaymentAttemptEndpoint                    Endpoint = "/pa/payment_attempts/{id}"
	listPaymentAttemptsEndpoint                  Endpoint = "/pa/payment_attempts"
	createPaymentIntentEndpoint                  Endpoint = "/pa/payment_intents/create"
	getPaymentIntentEndpoint                     Endpoint = "/pa/payment_intents/{id}"
	confirmPaymentIntentEndpoint                 Endpoint = "/pa/payment_intents/{id}/confirm"
	continueConfirmPaymentIntentEndpoint         Endpoint = "/pa/payment_intents/{id}/confirm_continue"
	capturePaymentIntentEndpoint                 Endpoint = "/pa/payment_intents/{id}/capture"
	cancelPaymentIntentEndpoint                  Endpoint = "/pa/payment_intents/{id}/cancel"
	listPaymentIntentsEndpoint                   Endpoint = "/pa/payment_intents"
	createPaymentLinkEndpoint                    Endpoint = "/pa/payment_links/create"
	updatePaymentLinkEndpoint                    Endpoint = "/pa/payment_links/{id}/update"
	getPaymentLinkEndpoint                       Endpoint = "/pa/payment_links/{id}"
	sendPaymentLinkEndpoint                      Endpoint = "/pa/payment_links/{id}/notify_shopper"
	activatePaymentLinkEndpoint                  Endpoint = "/pa/payment_links/{id}/activate"
	deactivatePaymentLinkEndpoint                Endpoint = "/pa/payment_links/{id}/deactivate"
	deletePaymentLinkEndpoint                    Endpoint = "/pa/payment_links/{id}/delete"
	listPaymentLinksEndpoint                     Endpoint = "/pa/payment_links"
	createPaymentMethodEndpoint                  Endpoint = "/pa/payment_methods/create"
	getPaymentMethodEndpoint                     Endpoint = "/pa/payment_methods/{id}"
	updatePaymentMethodEndpoint                  Endpoint = "/pa/payment_methods/{id}/update"
	listPaymentMethodsEndpoint                   Endpoint = "/pa/payment_methods"
	disablePaymentMethodEndpoint                 Endpoint = "/pa/payment_methods/{id}/disable"
	createQuoteEndpoint                          Endpoint = "/pa/quotes/create"
	getQuoteEndpoint                             Endpoint = "/pa/quotes/{id}"
	listQuotesEndpoint                           Endpoint = "/pa/quotes"
	createRefundEndpoint                         Endpoint = "/pa/refunds/create"
	getRefundEndpoint                            Endpoint = "/pa/refunds/{id}"
	listRefundEndpoint                           Endpoint = "/pa/refunds"
	listBeneficiariesEndpoint                    Endpoint = "/beneficiaries"
	getBeneficiaryEndpoint                       Endpoint = "/beneficiaries/{beneficiary_id}"
	createBeneficiaryEndpoint                    Endpoint = "/beneficiaries/create"
	deleteBeneficiaryEndpoint                    Endpoint = "/beneficiaries/delete/{beneficiary_id}"
	updateBeneficiaryEndpoint                    Endpoint = "/beneficiaries/update/{beneficiary_id}"
	validateBeneficiaryEndpoint                  Endpoint = "/beneficiaries/validate"
	getAPISchemaEndpoint                         Endpoint = "/beneficiary_api_schemas/generate"
	getFormSchemaEndpoint                        Endpoint = "/beneficiary_form_schemas/generate"
	listPayersEndpoint                           Endpoint = "/payers"
	getPayerEndpoint                             Endpoint = "/payers/{payer_id}"
	createPayerEndpoint                          Endpoint = "/payers/create"
	deletePayerEndpoint                          Endpoint = "/payers/delete/{payer_id}"
	updatePayerEndpoint                          Endpoint = "/payers/update/{payer_id}"
	validatePayerEndpoint                        Endpoint = "/payers/validate"
	listPaymentsEndpoint                         Endpoint = "/payments"
	getPaymentEndpoint                           Endpoint = "/payments/{payment_id}"
	cancelPaymentEndpoint                        Endpoint = "/payments/cancel/{payment_id}"
	createPaymentEndpoint                        Endpoint = "/payments/create"
	retryPaymentEndpoint                         Endpoint = "/payments/retry/{payment_id}"
	updatePaymentEndpoint                        Endpoint = "/payments/update/{payment_id}"
	validatePaymentEndpoint                      Endpoint = "/payments/validate"
	createAccountInvitationLinkEndpoint          Endpoint = "/accounts/invitation_links/create"
	getAccountInvitationLinkEndpoint             Endpoint = "/accounts/invitation_links/{id}"
	listChargesEndpoint                          Endpoint = "/charges"
	getChargeEndpoint                            Endpoint = "/charges/{id}"
	createChargeEndpoint                         Endpoint = "/charges/create"
	listTransfersEndpoint                        Endpoint = "/transfers"
	getTransferEndpoint                          Endpoint = "/transfers/{id}"
	createTransferEndpoint                       Endpoint = "/transfers/create"
	createConversionEndpoint                     Endpoint = "/conversions/create"
	getConversionEndpoint                        Endpoint = "/conversions/{conversion_id}"
	listConversionsEndpoint                      Endpoint = "/conversions"
	reverseSettledConversionEndpoint             Endpoint = "/conversions/{conversion_id}/reverse"
	createLockFxQuoteEndpoint                    Endpoint = "/lockfx/create"
	getIndicativeMarketQuoteEndpoint             Endpoint = "/marketfx/quote"
)

type Airwallex interface {
	Login(ctx context.Context) (out *LoginResponse, err error)

	GetCurrentBalances(ctx context.Context) (out *GetCurrentBalancesResponse, err error)
	GetBalanceHistory(ctx context.Context, in *GetBalanceHistoryRequest) (out *GetBalanceHistoryResponse, err error)

	ListGlobalAccounts(ctx context.Context, in *ListGlobalAccountsRequest) (out *ListGlobalAccountsResponse, err error)
	GetGlobalAccount(ctx context.Context, in *GetGlobalAccountRequest) (out *GetGlobalAccountResponse, err error)
	GenerateGlobalAccountStatementLetter(ctx context.Context, in *GenerateGlobalAccountStatementLetterRequest) (out *GenerateGlobalAccountStatementLetterResponse, err error)
	ListGlobalAccountTransactions(ctx context.Context, in *ListGlobalAccountTransactionsRequest) (out *ListGlobalAccountTransactionsResponse, err error)
	CreateGlobalAccount(ctx context.Context, in *CreateGlobalAccountRequest) (out *CreateGlobalAccountResponse, err error)
	UpdateGlobalAccount(ctx context.Context, in *UpdateGlobalAccountRequest) (out *UpdateGlobalAccountResponse, err error)

	ListDeposits(ctx context.Context, in *ListDepositsRequest) (out *ListDepositsResponse, err error)

	ListFinancialTransactions(ctx context.Context, in *ListFinancialTransactionsRequest) (out *ListFinancialTransactionsResponse, err error)
	GetFinancialTransaction(ctx context.Context, in *GetFinancialTransactionRequest) (out *GetFinancialTransactionResponse, err error)

	ListSettlements(ctx context.Context, in *ListSettlementsRequest) (out *ListSettlementsResponse, err error)
	GetSettlement(ctx context.Context, in *GetSettlementRequest) (out *GetSettlementResponse, err error)
	GetSettlementReport(ctx context.Context, in *GetSettlementReportRequest) (out *GetSettlementReportResponse, err error)

	ListAuthorizations(ctx context.Context, in *ListAuthorizationsRequest) (out *ListAuthorizationsResponse, err error)
	GetAuthorization(ctx context.Context, in *GetAuthorizationRequest) (out *GetAuthorizationResponse, err error)

	CreateCardholder(ctx context.Context, in *CreateCardholderRequest) (out *CreateCardholderResponse, err error)
	ListCardholders(ctx context.Context, in *ListCardholdersRequest) (out *ListCardholdersResponse, err error)
	GetCardholder(ctx context.Context, in *GetCardholderRequest) (out *GetCardholderResponse, err error)
	UpdateCardholder(ctx context.Context, in *UpdateCardholderRequest) (out *UpdateCardholderResponse, err error)

	CreateCard(ctx context.Context, in *CreateCardRequest) (out *CreateCardResponse, err error)
	GetSensitiveCardDetails(ctx context.Context, in *GetSensitiveCardDetailsRequest) (out *GetSensitiveCardDetailsResponse, err error)
	ListCards(ctx context.Context, in *ListCardsRequest) (out *ListCardsResponse, err error)
	ActivateCard(ctx context.Context, in *ActivateCardRequest) (err error)
	GetCard(ctx context.Context, in *GetCardRequest) (out *GetCardResponse, err error)
	GetCardLimits(ctx context.Context, in *GetCardLimitsRequest) (out *GetCardLimitsResponse, err error)
	UpdateCard(ctx context.Context, in *UpdateCardRequest) (out *UpdateCardResponse, err error)

	GetIssuingConfig(ctx context.Context) (out *IssuingConfigResponse, err error)

	ListTransactions(ctx context.Context, in *ListTransactionsRequest) (out *ListTransactionsResponse, err error)
	GetTransaction(ctx context.Context, in *GetTransactionRequest) (out *GetTransactionResponse, err error)

	ListPaymentMethodTypes(ctx context.Context, in *ListPaymentMethodTypesRequest) (out *ListPaymentMethodTypesResponse, err error)
	ListBanks(ctx context.Context, in *ListBanksRequest) (out *ListBanksResponse, err error)

	CreateCustomer(ctx context.Context, in *CreateCustomerRequest) (out *CreateCustomerResponse, err error)
	GetCustomer(ctx context.Context, in *GetCustomerRequest) (out *GetCustomerResponse, err error)
	UpdateCustomer(ctx context.Context, in *UpdateCustomerRequest) (out *UpdateCustomerResponse, err error)
	GenerateCustomerClientSecret(ctx context.Context, in *GenerateCustomerClientSecretRequest) (out *GenerateCustomerClientSecretResponse, err error)
	AddCustomerPaymentMethod(ctx context.Context, in *AddCustomerPaymentMethodRequest) (out *AddCustomerPaymentMethodResponse, err error)
	DeleteCustomerPaymentMethod(ctx context.Context, in *DeleteCustomerPaymentMethodRequest) (out *DeleteCustomerPaymentMethodResponse, err error)
	ListCustomers(ctx context.Context, in *ListCustomersRequest) (out *ListCustomersResponse, err error)

	CreateCustomsDeclaration(ctx context.Context, in *CreateCustomsDeclarationRequest) (out *CreateCustomsDeclarationResponse, err error)
	UpdateCustomsDeclaration(ctx context.Context, in *UpdateCustomsDeclarationRequest) (out *UpdateCustomsDeclarationResponse, err error)
	RedeclareCustomsDeclaration(ctx context.Context, in *RedeclareCustomsDeclarationRequest) (out *RedeclareCustomsDeclarationResponse, err error)
	GetCustomsDeclaration(ctx context.Context, in *GetCustomsDeclarationRequest) (out *GetCustomsDeclarationResponse, err error)

	CreateFundsSplit(ctx context.Context, in *CreateFundsSplitRequest) (out *CreateFundsSplitResponse, err error)
	GetFundsSplit(ctx context.Context, in *GetFundsSplitRequest) (out *GetFundsSplitResponse, err error)
	ListFundsSplits(ctx context.Context, in *ListFundsSplitsRequest) (out *ListFundsSplitsResponse, err error)
	ReleaseFundsSplit(ctx context.Context, in *ReleaseFundsSplitRequest) (out *ReleaseFundsSplitResponse, err error)

	CreatePaymentConsent(ctx context.Context, in *CreatePaymentConsentRequest) (out *CreatePaymentConsentResponse, err error)
	UpdatePaymentConsent(ctx context.Context, in *UpdatePaymentConsentRequest) (out *UpdatePaymentConsentResponse, err error)
	VerifyPaymentConsent(ctx context.Context, in *VerifyPaymentConsentRequest) (out *VerifyPaymentConsentResponse, err error)
	DisablePaymentConsent(ctx context.Context, in *DisablePaymentConsentRequest) (out *DisablePaymentConsentResponse, err error)
	GetPaymentConsent(ctx context.Context, in *GetPaymentConsentRequest) (out *GetPaymentConsentResponse, err error)
	ListPaymentConsents(ctx context.Context, in *ListPaymentConsentsRequest) (out *ListPaymentConsentsResponse, err error)

	GetPaymentAttempt(ctx context.Context, in *GetPaymentAttemptRequest) (out *GetPaymentAttemptResponse, err error)
	ListPaymentAttempts(ctx context.Context, in *ListPaymentAttemptsRequest) (out *ListPaymentAttemptsResponse, err error)

	CreatePaymentIntent(ctx context.Context, in *CreatePaymentIntentRequest) (out *CreatePaymentIntentResponse, err error)
	GetPaymentIntent(ctx context.Context, in *GetPaymentIntentRequest) (out *GetPaymentIntentResponse, err error)
	ConfirmPaymentIntent(ctx context.Context, in *ConfirmPaymentIntentRequest) (out *ConfirmPaymentIntentResponse, err error)
	ContinueConfirmPaymentIntent(ctx context.Context, in *ContinueConfirmPaymentIntentRequest) (out *ContinueConfirmPaymentIntentResponse, err error)
	CapturePaymentIntent(ctx context.Context, in *CapturePaymentIntentRequest) (out *CapturePaymentIntentResponse, err error)
	CancelPaymentIntent(ctx context.Context, in *CancelPaymentIntentRequest) (out *CancelPaymentIntentResponse, err error)
	ListPaymentIntents(ctx context.Context, in *ListPaymentIntentsRequest) (out *ListPaymentIntentsResponse, err error)

	CreatePaymentLink(ctx context.Context, in *CreatePaymentLinkRequest) (out *CreatePaymentLinkResponse, err error)
	UpdatePaymentLink(ctx context.Context, in *UpdatePaymentLinkRequest) (out *UpdatePaymentLinkResponse, err error)
	GetPaymentLink(ctx context.Context, in *GetPaymentLinkRequest) (out *GetPaymentLinkResponse, err error)
	SendPaymentLink(ctx context.Context, in *SendPaymentLinkRequest) (out *SendPaymentLinkResponse, err error)
	ActivatePaymentLink(ctx context.Context, in *ActivatePaymentLinkRequest) (out *ActivatePaymentLinkResponse, err error)
	DeactivatePaymentLink(ctx context.Context, in *DeactivatePaymentLinkRequest) (out *DeactivatePaymentLinkResponse, err error)
	DeletePaymentLink(ctx context.Context, in *DeletePaymentLinkRequest) (out *DeletePaymentLinkResponse, err error)
	ListPaymentLinks(ctx context.Context, in *ListPaymentLinksRequest) (out *ListPaymentLinksResponse, err error)

	CreatePaymentMethod(ctx context.Context, in *CreatePaymentMethodRequest) (out *CreatePaymentMethodResponse, err error)
	GetPaymentMethod(ctx context.Context, in *GetPaymentMethodRequest) (out *GetPaymentMethodResponse, err error)
	UpdatePaymentMethod(ctx context.Context, in *UpdatePaymentMethodRequest) (out *UpdatePaymentMethodResponse, err error)
	ListPaymentMethods(ctx context.Context, in *ListPaymentMethodsRequest) (out *ListPaymentMethodsResponse, err error)
	DisablePaymentMethod(ctx context.Context, in *DisablePaymentMethodRequest) (out *DisablePaymentMethodResponse, err error)

	CreateQuote(ctx context.Context, in *CreateQuoteRequest) (out *CreateQuoteResponse, err error)
	GetQuote(ctx context.Context, in *GetQuoteRequest) (out *GetQuoteResponse, err error)
	ListQuotes(ctx context.Context, in *ListQuotesRequest) (out *ListQuotesResponse, err error)

	CreateRefund(ctx context.Context, in *CreateRefundRequest) (out *CreateRefundResponse, err error)
	GetRefund(ctx context.Context, in *GetRefundRequest) (out *GetRefundResponse, err error)
	ListRefunds(ctx context.Context, in *ListRefundsRequest) (out *ListRefundsResponse, err error)

	ListBeneficiaries(ctx context.Context, in *ListBeneficiariesRequest) (out *ListBeneficiariesResponse, err error)
	GetBeneficiary(ctx context.Context, in *GetBeneficiaryRequest) (out *GetBeneficiaryResponse, err error)
	CreateBeneficiary(ctx context.Context, in *CreateBeneficiaryRequest) (out *CreateBeneficiaryResponse, err error)
	DeleteBeneficiary(ctx context.Context, in *DeleteBeneficiaryRequest) (out *DeleteBeneficiaryResponse, err error)
	UpdateBeneficiary(ctx context.Context, in *UpdateBeneficiaryRequest) (out *UpdateBeneficiaryResponse, err error)
	ValidateBeneficiary(ctx context.Context, in *ValidateBeneficiaryRequest) (out *ValidateBeneficiaryResponse, err error)
	GetAPISchema(ctx context.Context, in *GetAPISchemaRequest) (out *GetAPISchemaResponse, err error)
	GetFormSchema(ctx context.Context, in *GetFormSchemaRequest) (out *GetFormSchemaResponse, err error)

	ListPayers(ctx context.Context, in *ListPayersRequest) (out *ListPayersResponse, err error)
	GetPayer(ctx context.Context, in *GetPayerRequest) (out *GetPayerResponse, err error)
	CreatePayer(ctx context.Context, in *CreatePayerRequest) (out *CreatePayerResponse, err error)
	DeletePayer(ctx context.Context, in *DeletePayerRequest) (out *DeletePayerResponse, err error)
	UpdatePayer(ctx context.Context, in *UpdatePayerRequest) (out *UpdatePayerResponse, err error)
	ValidatePayer(ctx context.Context, in *ValidatePayerRequest) (out *ValidatePayerResponse, err error)

	ListPayments(ctx context.Context, in *ListPaymentsRequest) (out *ListPaymentsResponse, err error)
	GetPayment(ctx context.Context, in *GetPaymentRequest) (out *GetPaymentResponse, err error)
	CancelPayment(ctx context.Context, in *CancelPaymentRequest) (out *CancelPaymentResponse, err error)
	CreatePayment(ctx context.Context, in *CreatePaymentRequest) (out *CreatePaymentResponse, err error)
	RetryPayment(ctx context.Context, in *RetryPaymentRequest) (out *RetryPaymentResponse, err error)
	UpdatePayment(ctx context.Context, in *UpdatePaymentRequest) (out *UpdatePaymentResponse, err error)
	ValidatePayment(ctx context.Context, in *ValidatePaymentRequest) (out *ValidatePaymentResponse, err error)

	CreateAccountInvitationLink(ctx context.Context, in *CreateAccountInvitationLinkRequest) (out *CreateAccountInvitationLinkResponse, err error)
	GetAccountInvitationLink(ctx context.Context, in *GetAccountInvitationLinkRequest) (out *GetAccountInvitationLinkResponse, err error)

	// TODO: Scale Accounts API

	ListCharges(ctx context.Context, in *ListChargesRequest) (out *ListChargesResponse, err error)
	GetCharge(ctx context.Context, in *GetChargeRequest) (out *GetChargeResponse, err error)
	CreateCharge(ctx context.Context, in *CreateChargeRequest) (out *CreateChargeResponse, err error)

	ListTransfers(ctx context.Context, in *ListTransfersRequest) (out *ListTransfersResponse, err error)
	GetTransfer(ctx context.Context, in *GetTransferRequest) (out *GetTransferResponse, err error)
	CreateTransfer(ctx context.Context, in *CreateTransferRequest) (out *CreateTransferResponse, err error)

	CreateConversion(ctx context.Context, in *CreateConversionRequest) (out *CreateConversionResponse, err error)
	GetConversion(ctx context.Context, in *GetConversionRequest) (out *GetConversionResponse, err error)
	ListConversions(ctx context.Context, in *ListConversionsRequest) (out *ListConversionsResponse, err error)
	ReverseSettledConversion(ctx context.Context, in *ReverseSettledConversionRequest) (out *ReverseSettledConversionResponse, err error)

	CreateLockFxQuote(ctx context.Context, in *CreateLockFxQuoteRequest) (out *CreateLockFxQuoteResponse, err error)

	GetIndicativeMarketQuote(ctx context.Context, in *GetIndicativeMarketQuoteRequest) (out *GetIndicativeMarketQuoteResponse, err error)
	// TODO:
}
type airwallex struct {
	apiEndpoint                        ApiEndpoint
	apiKey, clientID, connectedAccount string
	httpClient                         *http.Client
}

func NewAirwallex(host, apiKey, clientID string, opts ...Option) Airwallex {
	awx := &airwallex{
		apiKey:   apiKey,
		clientID: clientID,
	}

	for _, opt := range opts {
		opt(awx)
	}

	if len(awx.apiEndpoint) == 0 {
		awx.apiEndpoint = ApiEndpoint(host)
	}
	if awx.httpClient == nil {
		awx.httpClient = &http.Client{
			Timeout: time.Second * 5,
			Transport: &Transport{
				apiEndpoint: awx.apiEndpoint,
				apiKey:      apiKey,
				clientID:    clientID,
			},
		}
	}

	return awx
}
