package airwallex

import (
	"net/http"
)

type Option func(awx *airwallex)

func WithConnectedAccount(connectedAccount string) Option {
	return func(awx *airwallex) {
		awx.connectedAccount = connectedAccount
	}
}

func WithHttpClient(httpClient *http.Client) Option {
	return func(awx *airwallex) {
		awx.httpClient = httpClient
	}
}

func WithSandbox(sandbox bool) Option {
	return func(awx *airwallex) {
		awx.apiEndpoint = apiEndpoint
		if sandbox {
			awx.apiEndpoint = apiDemoEndpoint
		}
	}
}
