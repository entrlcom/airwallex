package airwallex

import (
	"context"
	"net/http"

	"github.com/google/uuid"
	"github.com/pkg/errors"
)

// https://www.airwallex.com/docs/api#/Finance/Settlements/_api_v1_pa_financial_settlements/get

type ListSettlementsRequest struct {
	Currency      Currency          `json:"currency,omitempty"`
	FromSettledAt *TimeISO8601      `json:"from_settled_at,omitempty"`
	PageNum       int32             `json:"page_num,omitempty"`
	PageSize      int32             `json:"page_size,omitempty"`
	Status        TransactionStatus `json:"status,omitempty"`
	ToSettledAt   *TimeISO8601      `json:"to_settled_at,omitempty"`
}

type ListSettlementsResponse struct {
	HasMore bool          `json:"has_more,omitempty"`
	Items   []*Settlement `json:"items,omitempty"`
}

func (awx airwallex) ListSettlements(
	ctx context.Context,
	in *ListSettlementsRequest,
) (out *ListSettlementsResponse, err error) {
	out = new(ListSettlementsResponse)
	uri := string(awx.apiEndpoint) + string(listSettlementsEndpoint)
	if err = Do(ctx, awx.httpClient, http.MethodGet, uri, in, out); err != nil {
		return out, errors.Wrap(err, "Do()")
	}

	return out, err
}

// https://www.airwallex.com/docs/api#/Finance/Settlements/_api_v1_pa_financial_settlements__id_/get

type GetSettlementRequest struct {
	ID string `json:"id,omitempty"`
}

type GetSettlementResponse Settlement

func (awx airwallex) GetSettlement(
	ctx context.Context,
	in *GetSettlementRequest,
) (out *GetSettlementResponse, err error) {
	out = new(GetSettlementResponse)
	uri := string(awx.apiEndpoint) + string(getSettlementEndpoint)
	if err = Do(ctx, awx.httpClient, http.MethodGet, uri, in, out); err != nil {
		return out, errors.Wrap(err, "Do()")
	}

	return out, err
}

// https://www.airwallex.com/docs/api#/Finance/Settlements/_api_v1_pa_financial_settlements__id__report/get

type GetSettlementReportRequest struct {
	ID string `json:"id,omitempty"`
}

type GetSettlementReportResponse struct {
	CreatedAt *TimeISO8601 `json:"created_at,omitempty"`
	ID        uuid.UUID    `json:"id,omitempty"`
	ReportURL string       `json:"report_url,omitempty"`
}

func (awx airwallex) GetSettlementReport(
	ctx context.Context,
	in *GetSettlementReportRequest,
) (out *GetSettlementReportResponse, err error) {
	out = new(GetSettlementReportResponse)
	uri := string(awx.apiEndpoint) + string(getSettlementReportEndpoint)
	if err = Do(ctx, awx.httpClient, http.MethodGet, uri, in, out); err != nil {
		return out, errors.Wrap(err, "Do()")
	}

	return out, err
}
