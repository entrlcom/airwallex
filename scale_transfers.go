package airwallex

import (
	"context"
	"net/http"

	"github.com/google/uuid"
	"github.com/pkg/errors"
)

// https://www.airwallex.com/docs/api#/Scale/Transfers/_api_v1_transfers/get

type ListTransfersRequest struct {
	Currency      Currency       `json:"currency,omitempty"`
	Destination   string         `json:"destination,omitempty"`
	FromCreatedAt *TimeISO8601   `json:"from_created_at,omitempty"`
	PageNum       int32          `json:"page_num,omitempty"`
	PageSize      int32          `json:"page_size,omitempty"`
	RequestID     uuid.UUID      `json:"request_id,omitempty"`
	Status        TransferStatus `json:"status,omitempty"`
	ToCreatedAt   *TimeISO8601   `json:"to_created_at,omitempty"`
}

type ListTransfersResponse struct {
	HasMore bool        `json:"has_more,omitempty"`
	Items   []*Transfer `json:"items,omitempty"`
}

func (awx airwallex) ListTransfers(
	ctx context.Context,
	in *ListTransfersRequest,
) (out *ListTransfersResponse, err error) {
	out = new(ListTransfersResponse)
	uri := string(awx.apiEndpoint) + string(listTransfersEndpoint)
	if err = Do(ctx, awx.httpClient, http.MethodGet, uri, in, out); err != nil {
		return out, errors.Wrap(err, "Do()")
	}

	return out, err
}

// https://www.airwallex.com/docs/api#/Scale/Transfers/_api_v1_transfers__id_/get

type GetTransferRequest struct {
	ID string `json:"id,omitempty"`
}

type GetTransferResponse Transfer

func (awx airwallex) GetTransfer(
	ctx context.Context,
	in *GetTransferRequest,
) (out *GetTransferResponse, err error) {
	uri := string(awx.apiEndpoint) + string(getTransferEndpoint)
	if err = Do(ctx, awx.httpClient, http.MethodGet, uri, in, out); err != nil {
		return out, errors.Wrap(err, "Do()")
	}

	return out, err
}

// https://www.airwallex.com/docs/api#/Scale/Transfers/_api_v1_transfers_create/post

type CreateTransferRequest struct {
	Amount      Decimal   `json:"amount,omitempty"`
	Currency    Currency  `json:"currency,omitempty"`
	Destination string    `json:"destination,omitempty"`
	Reason      string    `json:"reason,omitempty"`
	Reference   string    `json:"reference,omitempty"`
	RequestID   uuid.UUID `json:"request_id,omitempty"`
}

type CreateTransferResponse Transfer

func (awx airwallex) CreateTransfer(
	ctx context.Context,
	in *CreateTransferRequest,
) (out *CreateTransferResponse, err error) {
	out = new(CreateTransferResponse)
	uri := string(awx.apiEndpoint) + string(createTransferEndpoint)
	if err = Do(ctx, awx.httpClient, http.MethodPost, uri, in, out); err != nil {
		return out, errors.Wrap(err, "Do()")
	}

	return out, err
}
