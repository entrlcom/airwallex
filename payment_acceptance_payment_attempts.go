package airwallex

import (
	"context"
	"net/http"

	"github.com/pkg/errors"
)

// https://www.airwallex.com/docs/api#/Payment_Acceptance/Payment_Attempts/_api_v1_pa_payment_attempts__id_/get

type GetPaymentAttemptRequest struct {
	ID string `json:"id,omitempty"`
}

type GetPaymentAttemptResponse PaymentAttempt

func (awx airwallex) GetPaymentAttempt(
	ctx context.Context,
	in *GetPaymentAttemptRequest,
) (out *GetPaymentAttemptResponse, err error) {
	out = new(GetPaymentAttemptResponse)
	uri := string(awx.apiEndpoint) + string(getPaymentAttemptEndpoint)
	if err = Do(ctx, awx.httpClient, http.MethodGet, uri, in, out); err != nil {
		return out, errors.Wrap(err, "Do()")
	}

	return out, err
}

// https://www.airwallex.com/docs/api#/Payment_Acceptance/Payment_Attempts/_api_v1_pa_payment_attempts/get

type ListPaymentAttemptsRequest struct {
	Currency        Currency             `json:"currency,omitempty"`
	FromCreatedAt   *TimeISO8601         `json:"from_created_at,omitempty"`
	PageNum         int32                `json:"page_num,omitempty"`
	PageSize        int32                `json:"page_size,omitempty"`
	PaymentIntentID string               `json:"payment_intent_id,omitempty"`
	Status          PaymentAttemptStatus `json:"status,omitempty"`
	ToCreatedAt     *TimeISO8601         `json:"to_created_at,omitempty"`
}

type ListPaymentAttemptsResponse struct {
	HasMore bool              `json:"has_more,omitempty"`
	Items   []*PaymentAttempt `json:"items,omitempty"`
}

func (awx airwallex) ListPaymentAttempts(
	ctx context.Context,
	in *ListPaymentAttemptsRequest,
) (out *ListPaymentAttemptsResponse, err error) {
	out = new(ListPaymentAttemptsResponse)
	uri := string(awx.apiEndpoint) + string(listPaymentAttemptsEndpoint)
	if err = Do(ctx, awx.httpClient, http.MethodGet, uri, in, out); err != nil {
		return out, errors.Wrap(err, "Do()")
	}

	return out, err
}
