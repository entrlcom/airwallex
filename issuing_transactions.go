package airwallex

import (
	"context"
	"net/http"

	"github.com/google/uuid"
	"github.com/pkg/errors"
)

// https://www.airwallex.com/docs/api#/Issuing/Transactions/_api_v1_issuing_transactions/get

type ListTransactionsRequest struct {
	BillingCurrency Currency               `json:"billing_currency,omitempty"`
	CardID          uuid.UUID              `json:"card_id,omitempty"`
	FromCreatedAt   *TimeISO8601           `json:"from_created_at,omitempty"`
	PageNum         int32                  `json:"page_num,omitempty"`
	PageSize        int32                  `json:"page_size,omitempty"`
	RetrievalRef    string                 `json:"retrieval_ref,omitempty"`
	ToCreatedAt     *TimeISO8601           `json:"to_created_at,omitempty"`
	TransactionType IssuingTransactionType `json:"transaction_type,omitempty"`
}

type ListTransactionsResponse struct {
	HasMore bool                  `json:"has_more,omitempty"`
	Items   []*IssuingTransaction `json:"items,omitempty"`
}

func (awx airwallex) ListTransactions(
	ctx context.Context,
	in *ListTransactionsRequest,
) (out *ListTransactionsResponse, err error) {
	out = new(ListTransactionsResponse)
	uri := string(awx.apiEndpoint) + string(listTransactionsEndpoint)
	if err = Do(ctx, awx.httpClient, http.MethodGet, uri, in, out); err != nil {
		return out, errors.Wrap(err, "Do()")
	}

	return out, err
}

// https://www.airwallex.com/docs/api#/Issuing/Transactions/_api_v1_issuing_transactions__id_/get

type GetTransactionRequest struct {
	ID string `json:"id,omitempty"`
}

type GetTransactionResponse IssuingTransaction

func (awx airwallex) GetTransaction(
	ctx context.Context,
	in *GetTransactionRequest,
) (out *GetTransactionResponse, err error) {
	out = new(GetTransactionResponse)
	uri := string(awx.apiEndpoint) + string(getTransactionEndpoint)
	if err = Do(ctx, awx.httpClient, http.MethodGet, uri, in, out); err != nil {
		return out, errors.Wrap(err, "Do()")
	}

	return out, err
}
