package airwallex

import (
	"encoding/json"
	"time"

	"github.com/google/uuid"
	"github.com/pkg/errors"
)

const (
	AccountInvitationLinkModeOAuth2       AccountInvitationLinkMode = "oauth2"
	AccountInvitationLinkModeScaleConnect AccountInvitationLinkMode = "scale_connect"
)

const (
	AccountStatementTypeAmazon  AccountStatementType = "AMAZON"
	AccountStatementTypeGeneral AccountStatementType = "GENERAL"
)

const (
	ActionMethodGet  ActionMethod = "GET"
	ActionMethodPost ActionMethod = "POST"
)

const (
	ActionTypeRedirect     ActionType = "redirect"
	ActionTypeRedirectForm ActionType = "redirect_form"
)

const (
	AuthorizationStatusCleared  AuthorizationStatus = "CLEARED"
	AuthorizationStatusExpired  AuthorizationStatus = "EXPIRED"
	AuthorizationStatusFailed   AuthorizationStatus = "FAILED"
	AuthorizationStatusPending  AuthorizationStatus = "PENDING"
	AuthorizationStatusReversed AuthorizationStatus = "REVERSED"
)

const (
	CardAllowedTransactionCountMultiple CardAllowedTransactionCount = "MULTIPLE"
	CardAllowedTransactionCountSingle   CardAllowedTransactionCount = "SINGLE"
)

const (
	CardBrandMastercard CardBrand = "mastercard"
	CardBrandVisa       CardBrand = "visa"
)

const (
	CardFormFactorPhysical CardFormFactor = "PHYSICAL"
	CardFormFactorVirtual  CardFormFactor = "VIRTUAL"
)

const (
	CardholderStatusDisabled   CardholderStatus = "DISABLED"
	CardholderStatusIncomplete CardholderStatus = "INCOMPLETE"
	CardholderStatusPending    CardholderStatus = "PENDING"
	CardholderStatusReady      CardholderStatus = "READY"
)

const (
	CardStatusActive   CardStatus = "ACTIVE"
	CardStatusBlocked  CardStatus = "BLOCKED"
	CardStatusClosed   CardStatus = "CLOSED"
	CardStatusFailed   CardStatus = "FAILED"
	CardStatusInactive CardStatus = "INACTVE"
	CardStatusLost     CardStatus = "LOST"
	CardStatusPending  CardStatus = "PENDING"
	CardStatusStolen   CardStatus = "STOLEN"
)

const (
	CardTransactionLimitIntervalAllTime        CardTransactionLimitInterval = "ALL_TIME"
	CardTransactionLimitIntervalDaily          CardTransactionLimitInterval = "DAILY"
	CardTransactionLimitIntervalMonthly        CardTransactionLimitInterval = "MONTHLY"
	CardTransactionLimitIntervalPerTransaction CardTransactionLimitInterval = "PER_TRANSACTION"
	CardTransactionLimitIntervalWeekly         CardTransactionLimitInterval = "WEEKLY"
)

const (
	CardType1DDOTA           CardType1 = "DD_OTA"
	CardType1Debit           CardType1 = "DEBIT"
	CardType1GoodFundsCredit CardType1 = "GOOD_FUNDS_CREDIT"
	CardType1Prepaid         CardType1 = "PREPAID"
)

const (
	CardTypeCredit CardType2 = "credit"
	CardTypeDebit  CardType2 = "debit"
)

const (
	ChargeStatusNew       ChargeStatus = "NEW"
	ChargeStatusPending   ChargeStatus = "PENDING"
	ChargeStatusSettle    ChargeStatus = "SETTLED"
	ChargeStatusSuspended ChargeStatus = "SUSPENDED"
)

const (
	ClearingSystemACH  ClearingSystem = "ACH"
	ClearingSystemRTGS ClearingSystem = "RTGS"
)

const (
	ContinueConfirmType3DSCheckEnrollment ContinueConfirmType = "3ds_check_enrollment"
	ContinueConfirmType3DSValidate        ContinueConfirmType = "3ds_validate"
	ContinueConfirmTypeDCC                ContinueConfirmType = "dcc"
)

const (
	ConversionStatusAwaitingFunds     ConversionStatus = "AWAITING_FUNDS"
	ConversionStatusCancelled         ConversionStatus = "CANCELLED"
	ConversionStatusPendingRollover   ConversionStatus = "PENDING_ROLLOVER"
	ConversionStatusPendingSettlement ConversionStatus = "PENDING_SETTLEMENT"
	ConversionStatusRolloverRequested ConversionStatus = "ROLLOVER_REQUESTED"
	ConversionStatusSettled           ConversionStatus = "SETTLED"
)

const (
	CustomsDeclarationStatusFail       CustomsDeclarationStatus = "FAIL"
	CustomsDeclarationStatusProcessing CustomsDeclarationStatus = "PROCESSING"
	CustomsDeclarationStatusSuccess    CustomsDeclarationStatus = "SUCCESS"
	CustomsDeclarationStatusUndeclared CustomsDeclarationStatus = "UNDECLARED"
)

const (
	EntityTypeCompany  EntityType = "COMPANY"
	EntityTypePersonal EntityType = "PERSONAL"
)

const (
	FailureCodeAuthenticationFailed FailureCode = "authentication_failed"
	FailureCodeAuthorizationFailed  FailureCode = "authorization_failed"
	FailureCodeCaptureFailed        FailureCode = "capture_failed"
	FailureCodeFraudRejected        FailureCode = "fraud_rejected"
	FailureCodeProviderUnavailable  FailureCode = "provider_unavailable"
	FailureCodeSystemUnavailable    FailureCode = "system_unavailable"
)

const (
	FeePaidByBeneficiary FeePaidBy = "BENEFICIARY"
	FeePaidByPayer       FeePaidBy = "PAYER"
)

const (
	FieldTypeInput         FieldType = "INPUT"
	FieldTypePaymentMethod FieldType = "PAYMENT_METHOD"
	FieldTypeRadio         FieldType = "RADIO"
	FieldTypeSelect        FieldType = "SELECT"
)

const (
	FundsSplitStatusCreated  FundsSplitStatus = "CREATED"
	FundsSplitStatusReleased FundsSplitStatus = "RELEASED"
	FundsSplitStatusSettled  FundsSplitStatus = "SETTLED"
)

const (
	GenderF Gender = "F"
	GenderM Gender = "M"
)

const (
	GlobalAccountPaymentMethodTypeLocal GlobalAccountPaymentMethod = "LOCAL"
	GlobalAccountPaymentMethodTypeSWIFT GlobalAccountPaymentMethod = "SWIFT"
)

const (
	GlobalAccountStatusActive   GlobalAccountStatus = "ACTIVE"
	GlobalAccountStatusClosed   GlobalAccountStatus = "CLOSED"
	GlobalAccountStatusInactive GlobalAccountStatus = "INACTIVE"
)

const (
	GlobalAccountTransactionStatusNew       GlobalAccountTransactionStatus = "NEW"
	GlobalAccountTransactionStatusPrecessed GlobalAccountTransactionStatus = "PROCESSED"
)

const (
	GlobalAccountTransactionTypeCredit GlobalAccountTransactionType = "CREDIT"
	GlobalAccountTransactionTypeDebit  GlobalAccountTransactionType = "DEBIT"
)

const (
	IndividualIdentificationTypeDriversLicence IndividualIdentificationType = "DRIVERS_LICENCE"
	IndividualIdentificationTypeIDCard         IndividualIdentificationType = "ID_CARD"
	IndividualIdentificationTypePassport       IndividualIdentificationType = "PASSPORT"
)

const (
	IssuingFailureReasonAccountInvalid             IssuingFailureReason = "ACCOUNT_INVALID"
	IssuingFailureReasonCardInvalid                IssuingFailureReason = "CARD_INVALID"
	IssuingFailureReasonInsufficientFunds          IssuingFailureReason = "INSUFFICIENT_FUNDS"
	IssuingFailureReasonLimitExceeded              IssuingFailureReason = "LIMIT_EXCEEDED"
	IssuingFailureReasonInternalError              IssuingFailureReason = "INTERNAL_ERROR"
	IssuingFailureReasonNotSupported               IssuingFailureReason = "NOT_SUPPORTED"
	IssuingFailureReasonMerchantCategoryNotAllowed IssuingFailureReason = "MERCHANT_CATEGORY_NOT_ALLOWED"
	IssuingFailureReasonCurrencyNotAllowed         IssuingFailureReason = "CURRENCY_NOT_ALLOWED"
	IssuingFailureReasonRemoteAuthDeclined         IssuingFailureReason = "REMOTE_AUTH_DECLINED"
)

const (
	IssuingTransactionStatusApproved IssuingTransactionStatus = "APPROVED"
	IssuingTransactionStatusFailed   IssuingTransactionStatus = "FAILED"
	IssuingTransactionStatusPending  IssuingTransactionStatus = "PENDING"
)

const (
	IssuingTransactionTypeAuthorization  IssuingTransactionType = "AUTHORIZATION"
	IssuingTransactionTypeClearing       IssuingTransactionType = "CLEARING"
	IssuingTransactionTypeOriginalCredit IssuingTransactionType = "ORIGINAL_CREDIT"
	IssuingTransactionTypeRefund         IssuingTransactionType = "REFUND"
	IssuingTransactionTypeReversal       IssuingTransactionType = "REVERSAL"
)

const (
	LockFXQuoteValidityHr1   LockFXQuoteValidity = "HR_1"
	LockFXQuoteValidityHr4   LockFXQuoteValidity = "HR_4"
	LockFXQuoteValidityHr8   LockFXQuoteValidity = "HR_8"
	LockFXQuoteValidityHr24  LockFXQuoteValidity = "HR_24"
	LockFXQuoteValidityMin1  LockFXQuoteValidity = "MIN_1"
	LockFXQuoteValidityMin15 LockFXQuoteValidity = "MIN_15"
	LockFXQuoteValidityMin30 LockFXQuoteValidity = "MIN_30"
)

const (
	MerchantTriggerReasonScheduled   MerchantTriggerReason = "scheduled"
	MerchantTriggerReasonUnscheduled MerchantTriggerReason = "unscheduled"
)

const OAuth2ResponseTypeCode OAuth2ResponseType = "code"

const (
	OSTypeAndroid OSType = "android"
	OSTypeIOS     OSType = "ios"
)

const (
	PaymentAttemptStatusAuthenticationRedirected PaymentAttemptStatus = "AUTHENTICATION_REDIRECTED"
	PaymentAttemptStatusAuthorized               PaymentAttemptStatus = "AUTHORIZED"
	PaymentAttemptStatusCancelled                PaymentAttemptStatus = "CANCELLED"
	PaymentAttemptStatusCaptureRequested         PaymentAttemptStatus = "CAPTURE_REQUESTED"
	PaymentAttemptStatusExpired                  PaymentAttemptStatus = "EXPIRED"
	PaymentAttemptStatusFailed                   PaymentAttemptStatus = "FAILED"
	PaymentAttemptStatusPaid                     PaymentAttemptStatus = "PAID"
	PaymentAttemptStatusPendingAuthentication    PaymentAttemptStatus = "PENDING_AUTHENTICATION"
	PaymentAttemptStatusPendingAuthorization     PaymentAttemptStatus = "PENDING_AUTHORIZATION"
	PaymentAttemptStatusPendingManualReview      PaymentAttemptStatus = "PENDING_MANUAL_REVIEW"
	PaymentAttemptStatusReceived                 PaymentAttemptStatus = "RECEIVED"
	PaymentAttemptStatusSettled                  PaymentAttemptStatus = "SETTLED"
)

const (
	PaymentIntentStatusCancelled              PaymentIntentStatus = "CANCELLED"
	PaymentIntentStatusRequiresCapture        PaymentIntentStatus = "REQUIRES_CAPTURE"
	PaymentIntentStatusRequiresCustomerAction PaymentIntentStatus = "REQUIRES_CUSTOMER_ACTION"
	PaymentIntentStatusRequiresPaymentMethod  PaymentIntentStatus = "REQUIRES_PAYMENT_METHOD"
	PaymentIntentStatusSucceeded              PaymentIntentStatus = "SUCCEEDED"
)

const (
	PaymentMethodFlowInApp    PaymentMethodFlow = "inapp"
	PaymentMethodFlowJSAPI    PaymentMethodFlow = "jsapi"
	PaymentMethodFlowMiniprog PaymentMethodFlow = "miniprog"
	PaymentMethodFlowMWeb     PaymentMethodFlow = "mweb"
	PaymentMethodFlowWebQR    PaymentMethodFlow = "webqr"
)

const (
	PaymentMethodNameAlfamart        PaymentMethodName = "alfamart"
	PaymentMethodNameAlipayCN        PaymentMethodName = "alipaycn"
	PaymentMethodNameAlipayHK        PaymentMethodName = "alipayhk"
	PaymentMethodNameApplePay        PaymentMethodName = "applepay"
	PaymentMethodNameAXSKiosk        PaymentMethodName = "axs_kiosk"
	PaymentMethodNameBancontact      PaymentMethodName = "bancontact"
	PaymentMethodNameBankTransfer    PaymentMethodName = "bank_transfer"
	PaymentMethodNameBigC            PaymentMethodName = "bigc"
	PaymentMethodNameBLIK            PaymentMethodName = "blik"
	PaymentMethodNameBoost           PaymentMethodName = "boost"
	PaymentMethodNameCard            PaymentMethodName = "card"
	PaymentMethodNameDANA            PaymentMethodName = "dana"
	PaymentMethodNameDOKUEWallet     PaymentMethodName = "doku_ewallet"
	PaymentMethodNameDragonpay       PaymentMethodName = "dragonpay"
	PaymentMethodNameENETS           PaymentMethodName = "enets"
	PaymentMethodNameEPS             PaymentMethodName = "eps"
	PaymentMethodNameESUN            PaymentMethodName = "esun"
	PaymentMethodNameFamilyMart      PaymentMethodName = "family_mart"
	PaymentMethodNameFPX             PaymentMethodName = "fpx"
	PaymentMethodNameGCash           PaymentMethodName = "gcash"
	PaymentMethodNameGiropay         PaymentMethodName = "giropay"
	PaymentMethodNameGrabPay         PaymentMethodName = "grabpay"
	PaymentMethodNameHiLife          PaymentMethodName = "hi_life"
	PaymentMethodNameIDEAL           PaymentMethodName = "ideal"
	PaymentMethodNameIndomaret       PaymentMethodName = "indomaret"
	PaymentMethodNameKakaoPay        PaymentMethodName = "kakaopay"
	PaymentMethodNameKonbini         PaymentMethodName = "konbini"
	PaymentMethodNameMaxima          PaymentMethodName = "maxima"
	PaymentMethodNameMultibanco      PaymentMethodName = "multibanco"
	PaymentMethodNameMyBank          PaymentMethodName = "mybank"
	PaymentMethodNameNarvesen        PaymentMethodName = "narvesen"
	PaymentMethodNameOnlineBanking   PaymentMethodName = "online_banking"
	PaymentMethodNameP24             PaymentMethodName = "p24"
	PaymentMethodNamePaybyBankapp    PaymentMethodName = "paybybankapp"
	PaymentMethodNamePayEasy         PaymentMethodName = "payeasy"
	PaymentMethodNamePayPost         PaymentMethodName = "paypost"
	PaymentMethodNamePaysafecard     PaymentMethodName = "paysafecard"
	PaymentMethodNamePaysafecash     PaymentMethodName = "paysafecash"
	PaymentMethodNamePaysera         PaymentMethodName = "paysera"
	PaymentMethodNamePerlasTerminals PaymentMethodName = "perlas_terminals"
	PaymentMethodNamePermataNet      PaymentMethodName = "permatanet"
	PaymentMethodNamePOLi            PaymentMethodName = "poli"
	PaymentMethodNameSatispay        PaymentMethodName = "satispay"
	PaymentMethodNameSafetyPay       PaymentMethodName = "safetypay"
	PaymentMethodNameSAMKiosk        PaymentMethodName = "sam_kiosk"
	PaymentMethodNameSevenEleven     PaymentMethodName = "seven_eleven"
	PaymentMethodNameShopeePay       PaymentMethodName = "shopee_pay"
	PaymentMethodNameSkrill          PaymentMethodName = "skrill"
	PaymentMethodNameSOFORT          PaymentMethodName = "sofort"
	PaymentMethodNameTescoLotus      PaymentMethodName = "tesco_lotus"
	PaymentMethodNameTNG             PaymentMethodName = "tng"
	PaymentMethodNameTrueMoney       PaymentMethodName = "truemoney"
	PaymentMethodNameTrustly         PaymentMethodName = "trustly"
	PaymentMethodNameVerkkopankki    PaymentMethodName = "verkkopankki"
	PaymentMethodNameWeChatPay       PaymentMethodName = "wechatpay"
)

const (
	PaymentMethodStatusCreated             PaymentMethodStatus = "CREATED"
	PaymentMethodStatusDisabled            PaymentMethodStatus = "DISABLED"
	PaymentMethodStatusExpired             PaymentMethodStatus = "EXPIRED"
	PaymentMethodStatusInvalid             PaymentMethodStatus = "INVALID"
	PaymentMethodStatusPendingVerification PaymentMethodStatus = "PENDING_VERIFICATION"
	PaymentMethodStatusVerified            PaymentMethodStatus = "VERIFIED"
)

const (
	PaymentStatusCancellationRequested PaymentStatus = "CANCELLATION_REQUESTED"
	PaymentStatusCancelled             PaymentStatus = "CANCELLED"
	PaymentStatusCompleted             PaymentStatus = "COMPLETED"
	PaymentStatusDispatched            PaymentStatus = "DISPATCHED"
	PaymentStatusFailed                PaymentStatus = "FAILED"
	PaymentStatusInReview              PaymentStatus = "IN_REVIEW"
	PaymentStatusNew                   PaymentStatus = "NEW"
	PaymentStatusReadyForDispatch      PaymentStatus = "READY_FOR_DISPATCH"
	PaymentStatusRetried               PaymentStatus = "RETRIED"
	PaymentStatusSuspended             PaymentStatus = "SUSPENDED"
)

const (
	PrefilledFormDataFieldBusinessName PrefilledFormDataField = "business_name"
	PrefilledFormDataFieldContactName  PrefilledFormDataField = "contact_name"
	PrefilledFormDataFieldCountry      PrefilledFormDataField = "country"
	PrefilledFormDataFieldEmail        PrefilledFormDataField = "email"
	PrefilledFormDataFieldFirstName    PrefilledFormDataField = "first_name"
	PrefilledFormDataFieldLastName     PrefilledFormDataField = "last_name"
	PrefilledFormDataFieldMobileNumber PrefilledFormDataField = "mobile_umber"
)

const (
	ProviderOriginalResponseCode00 ProviderOriginalResponseCode = "00"
	ProviderOriginalResponseCode01 ProviderOriginalResponseCode = "01"
	ProviderOriginalResponseCode02 ProviderOriginalResponseCode = "02"
	ProviderOriginalResponseCode03 ProviderOriginalResponseCode = "03"
	ProviderOriginalResponseCode04 ProviderOriginalResponseCode = "04"
	ProviderOriginalResponseCode05 ProviderOriginalResponseCode = "05"
	ProviderOriginalResponseCode06 ProviderOriginalResponseCode = "06"
	ProviderOriginalResponseCode07 ProviderOriginalResponseCode = "07"
	ProviderOriginalResponseCode08 ProviderOriginalResponseCode = "08"
	ProviderOriginalResponseCode10 ProviderOriginalResponseCode = "10"
	ProviderOriginalResponseCode11 ProviderOriginalResponseCode = "11"
	ProviderOriginalResponseCode12 ProviderOriginalResponseCode = "12"
	ProviderOriginalResponseCode13 ProviderOriginalResponseCode = "13"
	ProviderOriginalResponseCode14 ProviderOriginalResponseCode = "14"
	ProviderOriginalResponseCode15 ProviderOriginalResponseCode = "15"
	ProviderOriginalResponseCode19 ProviderOriginalResponseCode = "19"
	ProviderOriginalResponseCode21 ProviderOriginalResponseCode = "21"
	ProviderOriginalResponseCode25 ProviderOriginalResponseCode = "25"
	ProviderOriginalResponseCode28 ProviderOriginalResponseCode = "28"
	ProviderOriginalResponseCode30 ProviderOriginalResponseCode = "30"
	ProviderOriginalResponseCode39 ProviderOriginalResponseCode = "39"
	ProviderOriginalResponseCode41 ProviderOriginalResponseCode = "41"
	ProviderOriginalResponseCode43 ProviderOriginalResponseCode = "43"
	ProviderOriginalResponseCode51 ProviderOriginalResponseCode = "51"
	ProviderOriginalResponseCode52 ProviderOriginalResponseCode = "52"
	ProviderOriginalResponseCode53 ProviderOriginalResponseCode = "53"
	ProviderOriginalResponseCode54 ProviderOriginalResponseCode = "54"
	ProviderOriginalResponseCode55 ProviderOriginalResponseCode = "55"
	ProviderOriginalResponseCode57 ProviderOriginalResponseCode = "57"
	ProviderOriginalResponseCode58 ProviderOriginalResponseCode = "58"
	ProviderOriginalResponseCode59 ProviderOriginalResponseCode = "59"
	ProviderOriginalResponseCode61 ProviderOriginalResponseCode = "61"
	ProviderOriginalResponseCode62 ProviderOriginalResponseCode = "62"
	ProviderOriginalResponseCode63 ProviderOriginalResponseCode = "63"
	ProviderOriginalResponseCode64 ProviderOriginalResponseCode = "64"
	ProviderOriginalResponseCode65 ProviderOriginalResponseCode = "65"
	ProviderOriginalResponseCode70 ProviderOriginalResponseCode = "70"
	ProviderOriginalResponseCode71 ProviderOriginalResponseCode = "71"
	ProviderOriginalResponseCode74 ProviderOriginalResponseCode = "74"
	ProviderOriginalResponseCode75 ProviderOriginalResponseCode = "75"
	ProviderOriginalResponseCode76 ProviderOriginalResponseCode = "76"
	ProviderOriginalResponseCode78 ProviderOriginalResponseCode = "78"
	ProviderOriginalResponseCode79 ProviderOriginalResponseCode = "79"
	ProviderOriginalResponseCode80 ProviderOriginalResponseCode = "80"
	ProviderOriginalResponseCode81 ProviderOriginalResponseCode = "81"
	ProviderOriginalResponseCode82 ProviderOriginalResponseCode = "82"
	ProviderOriginalResponseCode84 ProviderOriginalResponseCode = "84"
	ProviderOriginalResponseCode85 ProviderOriginalResponseCode = "85"
	ProviderOriginalResponseCode86 ProviderOriginalResponseCode = "86"
	ProviderOriginalResponseCode87 ProviderOriginalResponseCode = "87"
	ProviderOriginalResponseCode88 ProviderOriginalResponseCode = "88"
	ProviderOriginalResponseCode89 ProviderOriginalResponseCode = "89"
	ProviderOriginalResponseCode91 ProviderOriginalResponseCode = "91"
	ProviderOriginalResponseCode92 ProviderOriginalResponseCode = "92"
	ProviderOriginalResponseCode93 ProviderOriginalResponseCode = "93"
	ProviderOriginalResponseCode94 ProviderOriginalResponseCode = "94"
	ProviderOriginalResponseCode96 ProviderOriginalResponseCode = "96"
	ProviderOriginalResponseCode1A ProviderOriginalResponseCode = "1A"
	ProviderOriginalResponseCodeB1 ProviderOriginalResponseCode = "B1"
	ProviderOriginalResponseCodeN0 ProviderOriginalResponseCode = "N0"
	ProviderOriginalResponseCodeN3 ProviderOriginalResponseCode = "N3"
	ProviderOriginalResponseCodeN4 ProviderOriginalResponseCode = "N4"
	ProviderOriginalResponseCodeN7 ProviderOriginalResponseCode = "N7"
	ProviderOriginalResponseCodeN8 ProviderOriginalResponseCode = "N8"
	ProviderOriginalResponseCodeP5 ProviderOriginalResponseCode = "P5"
	ProviderOriginalResponseCodeP6 ProviderOriginalResponseCode = "P6"
	ProviderOriginalResponseCodeQ1 ProviderOriginalResponseCode = "Q1"
	ProviderOriginalResponseCodeR0 ProviderOriginalResponseCode = "R0"
	ProviderOriginalResponseCodeR1 ProviderOriginalResponseCode = "R1"
	ProviderOriginalResponseCodeR3 ProviderOriginalResponseCode = "R3"
	ProviderOriginalResponseCodeZ3 ProviderOriginalResponseCode = "Z3"
)

const (
	// TOOD: add QuoteType constants
	QuoteTypeMCP QuoteType = "mcp"
)

const (
	RefundStatusAccepted  RefundStatus = "ACCEPTED"
	RefundStatusFailed    RefundStatus = "FAILED"
	RefundStatusReceived  RefundStatus = "RECEIVED"
	RefundStatusSucceeded RefundStatus = "SUCCEEDED"
)

const (
	SettleViaAirwallex SettleVia = "airwallex"
	SettleViaAmex      SettleVia = "amex"
	SettleViaPayPal    SettleVia = "paypal"
)

const (
	SourceTypeAdjustment          SourceType = "ADJUSTMENT"
	SourceTypeCharge              SourceType = "CHARGE"
	SourceTypeConversion          SourceType = "CONVERSION"
	SourceTypeDeposit             SourceType = "DEPOSIT"
	SourceTypeDispute             SourceType = "DISPUTE"
	SourceTypeFee                 SourceType = "FEE"
	SourceTypePayment             SourceType = "PAYMENT"
	SourceTypePaymentAttempt      SourceType = "PAYMENT_ATTEMPT"
	SourceTypePaymentIntent       SourceType = "PAYMENT_INTENT"
	SourceTypePaymentRefund       SourceType = "PAYMENT_REFUND"
	SourceTypePaymentRefundFailed SourceType = "PAYMENT_REFUND_FAILED"
	SourceTypePaymentReserve      SourceType = "PAYMENT_RESERVE"
	SourceTypePaymentSettlement   SourceType = "PAYMENT_SETTLEMENT"
	SourceTypePayout              SourceType = "PAYOUT"
	SourceTypeRefund              SourceType = "REFUND"
	SourceTypeTransfer            SourceType = "TRANSFER"
)

const (
	SWIFTChargeOptionPayer  SWIFTChargeOption = "PAYER"
	SWIFTChargeOptionShared SWIFTChargeOption = "SHARED"
)

const (
	TransactionModeOneOff    TransactionMode = "oneoff"
	TransactionModeRecurring TransactionMode = "recurring"
)

const (
	ThreeDSActionExternal3DS ThreeDSAction = "EXTERNAL_3DS"
	ThreeDSActionForce3DS    ThreeDSAction = "FORCE_3DS"
)

const (
	TransactionStatusPending TransactionStatus = "PENDING"
	TransactionStatusSettled TransactionStatus = "SETTLED"
)

const (
	TransactionTypePaymentAttempt        TransactionType = "PAYMENT_ATTEMPT"
	TransactionTypeDispute               TransactionType = "DISPUTE"
	TransactionTypeDisputeReversal       TransactionType = "DISPUTE_REVERSAL"
	TransactionTypeDisputeLost           TransactionType = "DISPUTE_LOST"
	TransactionTypeRefund                TransactionType = "REFUND"
	TransactionTypeRefundReversal        TransactionType = "REFUND_REVERSAL"
	TransactionTypeRefundFailure         TransactionType = "REFUND_FAILURE"
	TransactionTypePaymentReserveHold    TransactionType = "PAYMENT_RESERVE_HOLD"
	TransactionTypePaymentReserveRelease TransactionType = "PAYMENT_RESERVE_RELEASE"
	TransactionTypePayout                TransactionType = "PAYOUT"
	TransactionTypePayoutFailure         TransactionType = "PAYOUT_FAILURE"
	TransactionTypePayoutReversal        TransactionType = "PAYOUT_REVERSAL"
	TransactionTypeConversionSell        TransactionType = "CONVERSION_SELL"
	TransactionTypeConversionBuy         TransactionType = "CONVERSION_BUY"
	TransactionTypeConversionReversal    TransactionType = "CONVERSION_REVERSAL"
	TransactionTypeDeposit               TransactionType = "DEPOSIT"
	TransactionTypeAdjustment            TransactionType = "ADJUSTMENT"
	TransactionTypeFee                   TransactionType = "FEE"
	TransactionTypeDdCredit              TransactionType = "DD_CREDIT"
	TransactionTypeDdDebit               TransactionType = "DD_DEBIT"
	TransactionTypeDcCredit              TransactionType = "DC_CREDIT"
	TransactionTypeDcDebit               TransactionType = "DC_DEBIT"
)

const (
	TransferStatusNew     TransferStatus = "NEW"
	TransferStatusSettled TransferStatus = "SETTLED"
)

const (
	TriggeredByCustomer TriggeredBy = "customer"
	TriggeredByMerchant TriggeredBy = "merchant"
)

type AccountInvitationLink struct {
	AccountID         string                             `json:"account_id,omitempty"`
	CreatedAt         *TimeISO8601                       `json:"created_at,omitempty"`
	ExpireAt          *TimeISO8601                       `json:"expire_at,omitempty"`
	ID                string                             `json:"id,omitempty"`
	Identifier        string                             `json:"identifier,omitempty"`
	Metadata          Metadata                           `json:"metadata,omitempty"`
	Mode              AccountInvitationLinkMode          `json:"mode,omitempty"`
	OAuth2            *AccountInvitationLinkOAuth2       `json:"oauth2,omitempty"`
	PrefilledFormData []*PrefilledFormData               `json:"prefilled_formdata,omitempty"`
	ScaleConnect      *AccountInvitationLinkScaleConnect `json:"scale_connect,omitempty"`
	URL               string                             `json:"url,omitempty"`
}

type AccountInvitationLinkMode string

type AccountInvitationLinkOAuth2 struct {
	RedirectURI  string             `json:"redirect_uri,omitempty"`
	ResponseType OAuth2ResponseType `json:"response_type,omitempty"`
	Scope        []string           `json:"scope,omitempty"`
	State        string             `json:"state,omitempty"`
}

type AccountInvitationLinkScaleConnect struct {
	RedirectURI string `json:"redirect_uri,omitempty"`
}

type AccountStatementType string

type ActionMethod string

type ActionType string

type AdditionalInfoBasic struct {
	FirstSuccessfulOrderDate string `json:"first_successful_order_date,omitempty"` // TODO: Time (YYYY-MM-DD)
	RegisteredViaSocialMedia bool   `json:"registered_via_social_media,omitempty"`
	RegistrationDate         string `json:"registration_date,omitempty"` // TODO: Time (YYYY-MM-DD)
}

type AddressBasic struct {
	City        string `json:"city,omitempty"`
	CountryCode string `json:"country_code,omitempty"`
	Postcode    string `json:"postcode,omitempty"`
	State       string `json:"state,omitempty"`
	Street      string `json:"street,omitempty"`
}

type APISchema struct {
	Condition *SchemaCondition  `json:"condition,omitempty"`
	Fields    []*APISchemaField `json:"fields,omitempty"`
	Key       string            `json:"key,omitempty"`
}

type APISchemaField struct {
	Key      string                 `json:"key,omitempty"`
	Path     string                 `json:"path,omitempty"`
	Required bool                   `json:"required,omitempty"`
	Rule     map[string]interface{} `json:"rule,omitempty"`
}

type Authorization struct {
	AuthCode             string               `json:"auth_code,omitempty"`
	BillingAmount        Decimal              `json:"billing_amount,omitempty"`
	BillingCurrency      Currency             `json:"billing_currency,omitempty"`
	CardID               string               `json:"card_id,omitempty"`
	CardNickname         string               `json:"card_nickname,omitempty"`
	CreateTime           *TimeISO8601         `json:"create_time,omitempty"`
	ExpiryDate           *TimeISO8601         `json:"expiry_date,omitempty"`
	FailureReason        IssuingFailureReason `json:"failure_reason,omitempty"`
	MaskedCardNumber     string               `json:"masked_card_number,omitempty"`
	Merchant             *Merchant            `json:"merchant,omitempty"`
	NetworkTransactionID string               `json:"network_transaction_id,omitempty"`
	RetrievalRef         string               `json:"retrieval_ref,omitempty"`
	Status               AuthorizationStatus  `json:"status,omitempty"`
	TransactionAmount    Decimal              `json:"transaction_amount,omitempty"`
	TransactionCurrency  Currency             `json:"transaction_currency,omitempty"`
	TransactionID        uuid.UUID            `json:"transaction_id,omitempty"`
	UpdatedByTransaction uuid.UUID            `json:"updated_by_transaction,omitempty"`
}

type AuthorizationStatus string

type Balance struct {
	Amount      Decimal      `json:"amount,omitempty"`
	Balance     Decimal      `json:"balance,omitempty"`
	Currency    Currency     `json:"currency,omitempty"`
	Description string       `json:"description,omitempty"`
	Fee         Decimal      `json:"fee,omitempty"`
	PostedAt    *TimeISO8601 `json:"posted_at,omitempty"`
	Source      uuid.UUID    `json:"source,omitempty"`
	SourceType  SourceType   `json:"source_type,omitempty"`
}

type BankDetails struct {
	AccountCurrency      Currency       `json:"account_currency,omitempty"`
	AccountName          string         `json:"account_name,omitempty"`
	AccountNumber        string         `json:"account_number,omitempty"`
	AccountRoutingType1  string         `json:"account_routing_type_1,omitempty"`
	AccountRoutingType2  string         `json:"account_routing_type_2,omitempty"`
	AccountRoutingValue1 string         `json:"account_routing_value_1,omitempty"`
	AccountRoutingValue2 string         `json:"account_routing_value_2,omitempty"`
	BankBranch           string         `json:"bank_branch,omitempty"`
	BankCountryCode      string         `json:"bank_country_code,omitempty"`
	BankName             string         `json:"bank_name,omitempty"`
	BankStreetAddress    string         `json:"bank_street_address,omitempty"`
	BindingMobileNumber  string         `json:"binding_mobile_number,omitempty"`
	Fingerprint          string         `json:"fingerprint,omitempty"`
	IBAN                 string         `json:"iban,omitempty"`
	LocalClearingSystem  ClearingSystem `json:"local_clearing_system,omitempty"`
	SwiftCode            string         `json:"swift_code,omitempty"`
	TransactionReference string         `json:"transaction_reference,omitempty"`
}

type Beneficiary struct {
	Beneficiary     *BeneficiaryDetails          `json:"beneficiary,omitempty"`
	BeneficiaryID   string                       `json:"beneficiary_id,omitempty"`
	Nickname        string                       `json:"nickname,omitempty"`
	PayerEntityType EntityType                   `json:"payer_entity_type,omitempty"`
	PaymentMethods  []GlobalAccountPaymentMethod `json:"payment_methods,omitempty"`
}

type BeneficiaryAdditionalInfo struct {
	BusinessArea               string `json:"business_area,omitempty"`
	BusinessPhoneNumber        string `json:"business_phone_number,omitempty"`
	BusinessRegistrationNumber string `json:"business_registration_number,omitempty"`
	LegalRepFirstNameInChinese string `json:"legal_rep_first_name_in_chinese,omitempty"`
	LegalRepIDNumber           string `json:"legal_rep_id_number,omitempty"`
	LegalRepLastNameInChinese  string `json:"legal_rep_last_name_in_chinese,omitempty"`
	PersonalEmail              string `json:"personal_email,omitempty"`
	PersonalFirstNameInChinese string `json:"personal_first_name_in_chinese,omitempty"`
	PersonalIDNumber           string `json:"personal_id_number,omitempty"`
	PersonalIDType             string `json:"personal_id_type,omitempty"`
	PersonalLastNameInChinese  string `json:"personal_last_name_in_chinese,omitempty"`
	PersonalMobileNumber       string `json:"personal_mobile_number,omitempty"`
}

type BeneficiaryAddress struct {
	City          string `json:"city,omitempty"`
	CountryCode   string `json:"country_code,omitempty"`
	Postcode      string `json:"postcode,omitempty"`
	State         string `json:"state,omitempty"`
	StreetAddress string `json:"street_address,omitempty"`
}

type BeneficiaryDetails struct {
	AdditionalInfo *BeneficiaryAdditionalInfo `json:"additional_info,omitempty"`
	Address        *BeneficiaryAddress        `json:"address,omitempty"`
	BankDetails    *BankDetails               `json:"bank_details,omitempty"`
	CompanyName    string                     `json:"company_name,omitempty"`
	DateOfBirth    string                     `json:"date_of_birth,omitempty"` // TODO: Time (YYYY-MM-DD)
	EntityType     EntityType                 `json:"entity_type,omitempty"`
	FirstName      string                     `json:"first_name,omitempty"`
	LastName       string                     `json:"last_name,omitempty"`
}

type Billing struct {
	Address     *AddressBasic `json:"address,omitempty"`
	DateOfBirth string        `json:"date_of_birth,omitempty"` // TODO: YYYY-MM-DD
	Email       string        `json:"email,omitempty"`
	FirstName   string        `json:"first_name,omitempty"`
	LastName    string        `json:"last_name,omitempty"`
	PhoneNumber string        `json:"phone_number,omitempty"`
}

type Browser struct {
	JavaEnabled       bool   `json:"java_enabled,omitempty"`
	JavaScriptEnabled bool   `json:"javascript_enabled,omitempty"`
	UserAgent         string `json:"user_agent,omitempty"`
}

type Card struct {
	ActivateOnIssue       bool                       `json:"activate_on_issue,omitempty"`
	AuthorizationControls *CardAuthorizationControls `json:"authorization_controls,omitempty"`
	Brand                 CardBrand                  `json:"brand,omitempty"`
	CardID                uuid.UUID                  `json:"card_id,omitempty"`
	CardNumber            string                     `json:"card_number,omitempty"`
	CardStatus            CardStatus                 `json:"card_status,omitempty"` // TODO: enum?
	CardholderID          string                     `json:"cardholder_id,omitempty"`
	ClientData            string                     `json:"client_data,omitempty"`
	CreatedAt             *TimeISO8601               `json:"created_at,omitempty"`
	CreatedBy             string                     `json:"created_by,omitempty"`
	FormFactor            CardFormFactor             `json:"form_factor,omitempty"`
	IssueTo               string                     `json:"issue_to,omitempty"` // TODO: enum?
	NameOnCard            string                     `json:"name_on_card,omitempty"`
	NickName              string                     `json:"nick_name,omitempty"`
	Note                  string                     `json:"note,omitempty"`
	PostalAddress         *CardholderAddress         `json:"postal_address,omitempty"`
	PrimaryContactDetails *CardPrimaryContactDetails `json:"primary_contact_details,omitempty"`
	Type                  CardType1                  `json:"type,omitempty"`
}

type CardAllowedTransactionCount string

type CardAuthorizationControls struct {
	ActiveFrom                *TimeISO8601                `json:"active_from,omitempty"`
	ActiveTo                  *TimeISO8601                `json:"active_to,omitempty"`
	AllowedCurrencies         []Currency                  `json:"allowed_currencies,omitempty"`
	AllowedMerchantCategories []string                    `json:"allowed_merchant_categories,omitempty"`
	AllowedTransactionCount   CardAllowedTransactionCount `json:"allowed_transaction_count,omitempty"`
	TransactionLimits         *CardTransactionLimits      `json:"transaction_limits,omitempty"`
}

type CardBrand string

type CardFormFactor string

type Cardholder struct {
	Address       *CardholderAddress `json:"address,omitempty"`
	CardholderID  string             `json:"cardholder_id,omitempty"`
	Email         string             `json:"email,omitempty"`
	Individual    *Individual        `json:"individual,omitempty"`
	MobileNumber  string             `json:"mobile_number,omitempty"`
	PostalAddress *CardholderAddress `json:"postal_address,omitempty"`
	Status        CardholderStatus   `json:"status,omitempty"`
}

type CardholderAddress struct {
	City     string `json:"city,omitempty"`
	Country  string `json:"country,omitempty"`
	Line1    string `json:"line1,omitempty"`
	Line2    string `json:"line2,omitempty"`
	Postcode string `json:"postcode,omitempty"`
	State    string `json:"state,omitempty"`
}

type CardholderStatus string

type CardLimit struct {
	Amount    Decimal                      `json:"amount,omitempty"`
	Interval  CardTransactionLimitInterval `json:"interval,omitempty"`
	Remaining Decimal                      `json:"remaining,omitempty"`
}

type CardPrimaryContactDetails struct {
	Email        string `json:"email,omitempty"`
	FullName     string `json:"full_name,omitempty"`
	MobileNumber string `json:"mobile_number,omitempty"`
}

type CardRiskControl struct {
	SkipRiskProcessing bool          `json:"skip_risk_processing,omitempty"`
	ThreeDSAction      ThreeDSAction `json:"three_ds_action,omitempty"`
}

type CardStatus string

type CardTransactionLimit struct {
	Amount   Decimal                      `json:"amount,omitempty"`
	Interval CardTransactionLimitInterval `json:"interval,omitempty"`
}

type CardTransactionLimitInterval string

type CardTransactionLimits struct {
	Currency Currency                `json:"currency,omitempty"`
	Limits   []*CardTransactionLimit `json:"limits,omitempty"`
}

type CardType1 string

type CardType2 string

type Charge struct {
	Amount    Decimal      `json:"amount,omitempty"`
	CreatedAt *TimeISO8601 `json:"created_at,omitempty"`
	Currency  Currency     `json:"currency,omitempty"`
	Fee       Decimal      `json:"fee,omitempty"`
	ID        uuid.UUID    `json:"id,omitempty"`
	Reason    string       `json:"reason,omitempty"`
	Reference string       `json:"reference,omitempty"`
	RequestID uuid.UUID    `json:"request_id,omitempty"`
	Source    string       `json:"source,omitempty"`
	Status    ChargeStatus `json:"status,omitempty"`
	UpdatedAt *TimeISO8601 `json:"updated_at,omitempty"`
}

type ChargeStatus string

type ClearingSystem string

type ConfigBank struct {
	BankName    string     `json:"bank_name,omitempty"`
	DisplayName string     `json:"display_name,omitempty"`
	Resources   *Resources `json:"resources,omitempty"`
}

type ConfigPaymentMethodType struct {
	Active                bool                `json:"active,omitempty"`
	Flows                 []PaymentMethodFlow `json:"flows,omitempty"`
	Name                  PaymentMethodName   `json:"name,omitempty"`
	TransactionCurrencies []Currency          `json:"transaction_currencies,omitempty"`
	TransactionMode       TransactionMode     `json:"transaction_mode,omitempty"`
}

type ContinueConfirmType string

type Conversion struct {
	ConversionDetails
	CreatedAt        *TimeISO8601     `json:"created_at,omitempty"`
	LastUpdatedAt    *TimeISO8601     `json:"last_updated_at,omitempty"`
	QuoteID          string           `json:"quote_id,omitempty"`
	Reason           string           `json:"reason,omitempty"`
	RequestID        uuid.UUID        `json:"request_id,omitempty"`
	ShortReferenceID string           `json:"short_reference_id,omitempty"`
	Status           ConversionStatus `json:"status,omitempty"`
}

type ConversionDetails struct {
	AwxRate              Decimal      `json:"awx_rate,omitempty"`
	BuyAmount            Decimal      `json:"buy_amount,omitempty"`
	BuyCurrency          Currency     `json:"buy_currency,omitempty"`
	ClientRate           Decimal      `json:"client_rate,omitempty"`
	ConversionDate       string       `json:"conversion_date,omitempty"` // TODO: Time (YYYY-MM-DD)
	ConversionID         uuid.UUID    `json:"conversion_id,omitempty"`
	CurrencyPair         CurrencyPair `json:"currency_pair,omitempty"`
	DealtCurrency        Currency     `json:"dealt_currency,omitempty"`
	SellAmount           Decimal      `json:"sell_amount,omitempty"`
	SellCurrency         Currency     `json:"sell_currency,omitempty"`
	SettlementCutoffTime *TimeISO8601 `json:"settlement_cutoff_time,omitempty"`
}

type ConversionStatus string

type CurrentBalance struct {
	AvailableAmount Decimal  `json:"available_amount,omitempty"`
	Currency        Currency `json:"currency,omitempty"`
	PendingAmount   Decimal  `json:"pending_amount,omitempty"`
	ReservedAmount  Decimal  `json:"reserved_amount,omitempty"`
	TotalAmount     Decimal  `json:"total_amount,omitempty"`
}

type CustomsDeclaration struct {
	AwxRequestID                        string                   `json:"awx_request_id,omitempty"`
	CreatedAt                           *TimeISO8601             `json:"created_at,omitempty"`
	CustomsDetails                      *CustomsDetails          `json:"customs_details,omitempty"`
	CustomsStatusMessage                string                   `json:"customs_status_message,omitempty"`
	ID                                  string                   `json:"id,omitempty"`
	PaymentMethodType                   string                   `json:"payment_method_type,omitempty"`
	ProviderTransactionID               string                   `json:"provider_transaction_id,omitempty"`
	ShopperIdentityCheckResult          string                   `json:"shopper_identity_check_result,omitempty"`
	Status                              CustomsDeclarationStatus `json:"status,omitempty"`
	SubOrder                            *SubOrder                `json:"sub_order,omitempty"`
	VerificationDepartmentCode          string                   `json:"verification_department_code,omitempty"`
	VerificationDepartmentTransactionID string                   `json:"verification_department_transaction_id,omitempty"`
	UpdatedAt                           *TimeISO8601             `json:"updated_at,omitempty"`
}

type CustomsDeclarationStatus string

type Customer struct {
	ID                 string               `json:"id,omitempty"`
	MerchantCustomerID string               `json:"merchant_customer_id,omitempty"`
	FirstName          string               `json:"first_name,omitempty"`
	LastName           string               `json:"last_name,omitempty"`
	Email              string               `json:"email,omitempty"`
	PhoneNumber        string               `json:"phone_number,omitempty"`
	AdditionalInfo     *AdditionalInfoBasic `json:"additional_info,omitempty"`
	Metadata           Metadata             `json:"metadata,omitempty"`
	PaymentMethods     []*PaymentMethods    `json:"payment_methods,omitempty"`
	CreatedAt          *TimeISO8601         `json:"created_at,omitempty"`
	UpdatedAt          *TimeISO8601         `json:"updated_at,omitempty"`
}

type CustomsDetails struct {
	CustomsCode           string `json:"customs_code,omitempty"` // TODO: enum?
	MerchantCustomsName   string `json:"merchant_customs_name,omitempty"`
	MerchantCustomsNumber string `json:"merchant_customs_number,omitempty"`
}

type DCCData struct {
	DCCDataBasic
	ClientRate    float64      `json:"client_rate,omitempty"`
	CurrencyPair  CurrencyPair `json:"currency_pair,omitempty"`
	RateExpiry    string       `json:"rate_expiry,omitempty"`
	RateTimestamp string       `json:"rate_timestamp,omitempty"`
}

type DCCDataBasic struct {
	Amount   Decimal  `json:"amount,omitempty"`
	Currency Currency `json:"currency,omitempty"`
}

type Deposit struct {
	Amount          Decimal      `json:"amount,omitempty"`
	CreatedAt       *TimeISO8601 `json:"created_at,omitempty"`
	Currency        Currency     `json:"currency,omitempty"`
	DepositID       uuid.UUID    `json:"deposit_id,omitempty"`
	GlobalAccountID uuid.UUID    `json:"global_account_id,omitempty"`
	StatementRef    string       `json:"statement_ref,omitempty"`
}

type DeviceData struct {
	AcceptHeader     string    `json:"accept_header,omitempty"`
	Browser          *Browser  `json:"browser,omitempty"`
	DeviceID         string    `json:"device_id,omitempty"`
	IpAddress        string    `json:"ip_address,omitempty"`
	Language         string    `json:"language,omitempty"`
	Location         *Location `json:"location,omitempty"`
	Mobile           *Mobile   `json:"mobile,omitempty"`
	ScreenColorDepth int32     `json:"screen_color_depth,omitempty"`
	ScreenHeight     int32     `json:"screen_height,omitempty"`
	ScreenWidth      int32     `json:"screen_width,omitempty"`
	Timezone         string    `json:"timezone,omitempty"`
}

type EntityType string

type FailureCode string

type FeePaidBy string

type Field struct {
	Default     string    `json:"default,omitempty"`
	Description string    `json:"description,omitempty"`
	Key         string    `json:"key,omitempty"`
	Label       string    `json:"label,omitempty"`
	Placeholder string    `json:"placeholder,omitempty"`
	Refresh     bool      `json:"refresh,omitempty"`
	Tip         string    `json:"tip,omitempty"`
	Type        FieldType `json:"type,omitempty"`
}

type FieldType string

type FinancialTransaction struct {
	Amount             Decimal           `json:"amount,omitempty"`
	BatchID            string            `json:"batchID,omitempty"`
	ClientRate         Decimal           `json:"clientRate,omitempty"`
	CreatedAt          *TimeISO8601      `json:"createdAt,omitempty"`
	Currency           Currency          `json:"currency,omitempty"`
	CurrencyPair       CurrencyPair      `json:"currencyPair,omitempty"`
	Description        string            `json:"description,omitempty"`
	EstimatedSettledAt *TimeISO8601      `json:"estimatedSettledAt,omitempty"`
	Fee                Decimal           `json:"fee,omitempty"`
	ID                 uuid.UUID         `json:"id,omitempty"`
	Net                Decimal           `json:"net,omitempty"`
	SettledAt          *TimeISO8601      `json:"settledAt,omitempty"`
	SourceID           uuid.UUID         `json:"sourceID,omitempty"`
	SourceType         SourceType        `json:"sourceType,omitempty"`
	Status             TransactionStatus `json:"status,omitempty"`
	TransactionType    TransactionType   `json:"transactionType,omitempty"`
}

type FormSchema struct {
	Condition *SchemaCondition   `json:"condition,omitempty"`
	Fields    []*FormSchemaField `json:"fields,omitempty"`
}

type FormSchemaField struct {
	Enabled  bool                   `json:"enabled,omitempty"`
	Field    *Field                 `json:"field,omitempty"`
	Path     string                 `json:"path,omitempty"`
	Required bool                   `json:"required,omitempty"`
	Rule     map[string]interface{} `json:"rule,omitempty"`
}

type FundsSplit struct {
	Amount      Decimal          `json:"amount,omitempty"`
	AutoRelease bool             `json:"auto_release,omitempty"`
	CreatedAt   *TimeISO8601     `json:"created_at,omitempty"`
	Currency    Currency         `json:"currency,omitempty"`
	Destination string           `json:"destination,omitempty"`
	ID          string           `json:"id,omitempty"`
	Metadata    Metadata         `json:"metadata,omitempty"`
	SourceID    string           `json:"source_id,omitempty"`
	SourceType  SourceType       `json:"source_type,omitempty"`
	Status      FundsSplitStatus `json:"status,omitempty"`
}

type FundsSplitStatus string

type Gender string

type GlobalAccount struct {
	AccountName         string                       `json:"account_name,omitempty"`
	AccountNumber       string                       `json:"account_number,omitempty"`
	AccountRoutingType  string                       `json:"account_routing_type,omitempty"`
	AccountRoutingValue string                       `json:"account_routing_value,omitempty"`
	BranchCode          string                       `json:"branch_code,omitempty"`
	ClearingSystems     []ClearingSystem             `json:"clearing_systems,omitempty"`
	CountryCode         string                       `json:"country_code,omitempty"`
	Currency            Currency                     `json:"currency,omitempty"`
	ID                  uuid.UUID                    `json:"id,omitempty"`
	InstitutionName     string                       `json:"institution_name,omitempty"`
	NickName            string                       `json:"nick_name,omitempty"`
	PaymentMethods      []GlobalAccountPaymentMethod `json:"payment_methods,omitempty"`
	RequestID           uuid.UUID                    `json:"request_id,omitempty"`
	Status              GlobalAccountStatus          `json:"status,omitempty"`
	SwiftCode           string                       `json:"swift_code,omitempty"`
}

type GlobalAccountPaymentMethod string

type GlobalAccountStatus string

type GlobalAccountTransaction struct {
	Amount       Decimal                        `json:"amount,omitempty"`
	CreateTime   *TimeISO8601                   `json:"createTime,omitempty"`
	Currency     Currency                       `json:"currency,omitempty"`
	Description  string                         `json:"description,omitempty"`
	FeeAmount    Decimal                        `json:"fee_amount,omitempty"`
	FeeCurrency  Decimal                        `json:"fee_currency,omitempty"`
	ID           uuid.UUID                      `json:"id,omitempty"`
	PayerCountry string                         `json:"payer_country,omitempty"` // TODO: enum (GlobalAccounts | Get global account transactions)
	PayerName    string                         `json:"payer_name,omitempty"`
	Status       GlobalAccountTransactionStatus `json:"status,omitempty"`
	Type         GlobalAccountTransactionType   `json:"type,omitempty"`
}

type GlobalAccountTransactionStatus string

type GlobalAccountTransactionType string

type Individual struct {
	DateOfBirth    string                    `json:"date_of_birth,omitempty"` // TODO: Time (YYYY-MM-DD)
	Identification *IndividualIdentification `json:"identification,omitempty"`
	Name           *IndividualName           `json:"name,omitempty"`
}

type IndividualIdentification struct {
	Country             string                       `json:"country,omitempty"`
	DocumentBackFileID  string                       `json:"document_back_file_id,omitempty"`
	DocumentFrontFileID string                       `json:"document_front_file_id,omitempty"`
	ExpiryDate          string                       `json:"expiry_date,omitempty"` // TODO: Time (YYYY-MM-DD)
	Gender              Gender                       `json:"gender,omitempty"`
	Number              string                       `json:"number,omitempty"`
	State               string                       `json:"state,omitempty"` // TODO: enum?
	Type                IndividualIdentificationType `json:"type,omitempty"`
}

type IndividualIdentificationType string

type IndividualName struct {
	FirstName  string `json:"first_name,omitempty"`
	LastName   string `json:"last_name,omitempty"`
	MiddleName string `json:"middle_name,omitempty"`
	Title      string `json:"title,omitempty"`
}

type IssuingConfig struct {
	SpendingLimitSettings *SpendingLimitSettings `json:"spending_limit_settings,omitempty"`
}

type IssuingFailureReason string

type IssuingTransaction struct {
	AuthCode             string                   `json:"auth_code,omitempty"`
	BillingAmount        Decimal                  `json:"billing_amount,omitempty"`
	BillingCurrency      Currency                 `json:"billing_currency,omitempty"`
	CardID               uuid.UUID                `json:"card_id,omitempty"`
	CardNickname         string                   `json:"card_nickname,omitempty"`
	ClientData           string                   `json:"client_data,omitempty"`
	FailureReason        IssuingFailureReason     `json:"failure_reason,omitempty"`
	MaskedCardNumber     string                   `json:"masked_card_number,omitempty"`
	Merchant             *Merchant                `json:"merchant,omitempty"`
	NetworkTransactionID string                   `json:"network_transaction_id,omitempty"`
	PostedDate           *TimeISO8601             `json:"posted_date,omitempty"`
	RetrievalRef         string                   `json:"retrieval_ref,omitempty"`
	Status               IssuingTransactionStatus `json:"status,omitempty"`
	TransactionAmount    Decimal                  `json:"transaction_amount,omitempty"`
	TransactionCurrency  Currency                 `json:"transaction_currency,omitempty"`
	TransactionDate      *TimeISO8601             `json:"transaction_date,omitempty"`
	TransactionID        uuid.UUID                `json:"transaction_id,omitempty"`
	TransactionType      IssuingTransactionType   `json:"transaction_type,omitempty"`
}

type IssuingTransactionStatus string

type IssuingTransactionType string

type Location struct {
	Lat string `json:"lat,omitempty"`
	Lon string `json:"lon,omitempty"`
}

type LockFXQuote struct {
	AwxRate        Decimal             `json:"awx_rate,omitempty"`
	BuyAmount      Decimal             `json:"buy_amount,omitempty"`
	BuyCurrency    Currency            `json:"buy_currency,omitempty"`
	ClientRate     Decimal             `json:"client_rate,omitempty"`
	ConversionDate string              `json:"conversion_date,omitempty"` // TODO: Time (YYYY-MM-DD)
	DealtCurrency  Currency            `json:"dealt_currency,omitempty"`
	QuoteID        string              `json:"quote_id,omitempty"`
	SellAmount     Decimal             `json:"sell_amount,omitempty"`
	SellCurrency   Currency            `json:"sell_currency,omitempty"`
	Usage          string              `json:"usage,omitempty"`
	ValidFrom      *TimeISO8601        `json:"valid_from,omitempty"`
	ValidTo        *TimeISO8601        `json:"valid_to,omitempty"`
	Validity       LockFXQuoteValidity `json:"validity,omitempty"`
}

type LockFXQuoteValidity string

type MarketFXQuote struct {
	AwxRate              Decimal      `json:"awx_rate,omitempty"`
	BuyAmount            Decimal      `json:"buy_amount,omitempty"`
	BuyCurrency          Currency     `json:"buy_currency,omitempty"`
	ClientRate           Decimal      `json:"client_rate,omitempty"`
	ConversionDate       string       `json:"conversion_date,omitempty"` // TODO: Time (YYYY-MM-DD)
	CurrencyPair         CurrencyPair `json:"currency_pair,omitempty"`
	DealtCurrency        Currency     `json:"dealt_currency,omitempty"`
	QuoteID              string       `json:"quote_id,omitempty"`
	SellAmount           Decimal      `json:"sell_amount,omitempty"`
	SellCurrency         Currency     `json:"sell_currency,omitempty"`
	SettlementCutoffTime *TimeISO8601 `json:"settlement_cutoff_time,omitempty"`
}

type Merchant struct {
	CategoryCode string `json:"category_code,omitempty"`
	City         string `json:"city,omitempty"`
	Country      string `json:"country,omitempty"`
	Name         string `json:"name,omitempty"`
}

type MerchantTriggerReason string

type Mobile struct {
	DeviceModel string `json:"device_model,omitempty"`
	OSType      string `json:"os_type,omitempty"`
	OSVersion   string `json:"os_version,omitempty"`
}

type NextAction struct {
	NextActionBasic
	DCCData   *DCCData `json:"dcc_data,omitempty"`
	QRCodeURL string   `json:"qrcode_url,omitempty"`
}

type NextActionBasic struct {
	ContentType string       `json:"content_type,omitempty"`
	Data        interface{}  `json:"data,omitempty"`
	Method      ActionMethod `json:"method,omitempty"`
	Type        ActionType   `json:"type,omitempty"`
	URL         string       `json:"url,omitempty"`
}

type OAuth2ResponseType string

type Order struct {
	Products []*Product `json:"products,omitempty"`
	Shipping *Shipping  `json:"shipping,omitempty"`
	Type     string     `json:"type,omitempty"`
}

type OSType string

type Payer struct {
	Nickname string        `json:"nickname,omitempty"`
	Payer    *PayerDetails `json:"payer,omitempty"`
	PayerID  string        `json:"payer_id,omitempty"`
}

type PayerAdditionalInfo struct {
	BusinessRegistrationNumber string `json:"business_registration_number,omitempty"`
	BusinessRegistrationType   string `json:"business_registration_type,omitempty"`
	ExternalID                 string `json:"external_id,omitempty"`
	PersonalEmail              string `json:"personal_email,omitempty"`
	PersonalIDNumber           string `json:"personal_id_number,omitempty"`
}

type PayerDetails struct {
	AdditionalInfo *PayerAdditionalInfo `json:"additional_info,omitempty"`
	Address        *BeneficiaryAddress  `json:"address,omitempty"` // TODO: rename BeneficiaryAddress
	CompanyName    string               `json:"company_name,omitempty"`
	DateOfBirth    *TimeISO8601         `json:"date_of_birth,omitempty"`
	EntityType     EntityType           `json:"entity_type,omitempty"`
	FirstName      string               `json:"first_name,omitempty"`
	LastName       string               `json:"last_name,omitempty"`
}

type Payment struct {
	AmountBeneficiaryReceives Decimal                    `json:"amount_beneficiary_receives,omitempty"`
	AmountPayerPays           Decimal                    `json:"amount_payer_pays,omitempty"`
	Beneficiary               *BeneficiaryDetails        `json:"beneficiary,omitempty"`
	BeneficiaryID             string                     `json:"beneficiary_id,omitempty"`
	CreatedAt                 *TimeISO8601               `json:"created_at,omitempty"`
	FailureReason             string                     `json:"failure_reason,omitempty"`
	FailureType               string                     `json:"failure_type,omitempty"` // TODO: enum?
	FeeAmount                 Decimal                    `json:"fee_amount,omitempty"`
	FeeCurrency               Currency                   `json:"fee_currency,omitempty"`
	FeePaidBy                 FeePaidBy                  `json:"fee_paid_by,omitempty"`
	LastUpdatedAt             *TimeISO8601               `json:"last_updated_at,omitempty"`
	Payer                     *PayerDetails              `json:"payer,omitempty"`
	PayerID                   string                     `json:"payer_id,omitempty"`
	PaymentAmount             Decimal                    `json:"payment_amount,omitempty"`
	PaymentCurrency           Currency                   `json:"payment_currency,omitempty"`
	PaymentDate               string                     `json:"payment_date,omitempty"` // TODO: Time (YYYY-MM-DD)
	PaymentID                 uuid.UUID                  `json:"payment_id,omitempty"`
	PaymentMethod             GlobalAccountPaymentMethod `json:"payment_method,omitempty"`
	Reason                    string                     `json:"reason,omitempty"`
	Reference                 string                     `json:"reference,omitempty"`
	RequestID                 uuid.UUID                  `json:"request_id,omitempty"`
	ShortReferenceID          string                     `json:"short_reference_id,omitempty"`
	SourceAmount              Decimal                    `json:"source_amount,omitempty"`
	SourceCurrency            Currency                   `json:"source_currency,omitempty"`
	Status                    PaymentStatus              `json:"status,omitempty"`
	SWIFTChargeOption         SWIFTChargeOption          `json:"swift_charge_option,omitempty"`
	UnderlyingConversionID    uuid.UUID                  `json:"underlying_conversion_id,omitempty"`
}

type PaymentAttempt struct {
	Amount                       Decimal                      `json:"amount,omitempty"`
	AuthorizationCode            string                       `json:"authorization_code,omitempty"`
	CapturedAmount               Decimal                      `json:"captured_amount,omitempty"`
	CreatedAt                    *TimeISO8601                 `json:"created_at,omitempty"`
	Currency                     Currency                     `json:"currency,omitempty"`
	DCCData                      *DCCDataBasic                `json:"dcc_data,omitempty"`
	FailureCode                  FailureCode                  `json:"failure_code,omitempty"`
	ID                           string                       `json:"id,omitempty"`
	MerchantOrderID              string                       `json:"merchant_order_id,omitempty"`
	PaymentConsentID             string                       `json:"payment_consent_id,omitempty"`
	PaymentIntentID              string                       `json:"payment_intent_id,omitempty"`
	PaymentMethod                *PaymentMethod               `json:"payment_method,omitempty"`
	ProviderOriginalResponseCode ProviderOriginalResponseCode `json:"provider_original_response_code,omitempty"`
	ProviderTransactionID        string                       `json:"provider_transaction_id,omitempty"`
	RefundedAmount               Decimal                      `json:"refunded_amount,omitempty"`
	SettleVia                    SettleVia                    `json:"settle_via,omitempty"`
	Status                       PaymentAttemptStatus         `json:"status,omitempty"`
	UpdatedAt                    *TimeISO8601                 `json:"updated_at,omitempty"`
}

type PaymentAttemptStatus string

type PaymentConsent struct {
	ClientSecret           string                       `json:"client_secret,omitempty"`
	ConnectedAccountID     string                       `json:"connected_account_id,omitempty"`
	CreatedAt              *TimeISO8601                 `json:"created_at,omitempty"`
	CustomerID             string                       `json:"customer_id,omitempty"`
	ID                     string                       `json:"id,omitempty"`
	InitialPaymentIntentID string                       `json:"initial_payment_intent_id,omitempty"`
	MerchantTriggerReason  MerchantTriggerReason        `json:"merchant_trigger_reason,omitempty"`
	Metadata               Metadata                     `json:"metadata,omitempty"`
	NextAction             *NextActionBasic             `json:"next_action,omitempty"`
	NextTriggeredBy        TriggeredBy                  `json:"next_triggered_by,omitempty"`
	PaymentMethod          *PaymentConsentPaymentMethod `json:"payment_method,omitempty"`
	RequiresCVC            bool                         `json:"requires_cvc,omitempty"`
	Status                 PaymentMethodStatus          `json:"status,omitempty"`
	UpdatedAt              *TimeISO8601                 `json:"updated_at,omitempty"`
}

type PaymentConsentPaymentMethod struct {
	Card *Card  `json:"card,omitempty"`
	ID   string `json:"id,omitempty"`
	Type string `json:"type,omitempty"`
}

type PaymentConsentReference struct {
	CVC string `json:"cvc,omitempty"`
	ID  string `json:"id,omitempty"`
}

type PaymentConsentVerificationOptions struct {
	AlipayHK  *PaymentMethodAlipayHK                `json:"alipayhk,omitempty"`
	Card      *PaymentConsentVerificationOptionCard `json:"card,omitempty"`
	DANA      *PaymentMethodDANA                    `json:"dana,omitempty"`
	GCash     *PaymentMethodGCash                   `json:"gcash,omitempty"`
	KakaoPay  *PaymentMethodKakaoPay                `json:"kakaopay,omitempty"`
	TNG       *PaymentMethodTNG                     `json:"tng,omitempty"`
	TrueMoney *PaymentMethodTrueMoney               `json:"true_money,omitempty"`
}

type PaymentConsentVerificationOptionCard struct {
	Amount      Decimal      `json:"amount,omitempty"`
	Currency    Currency     `json:"currency,omitempty"`
	CVC         string       `json:"cvc,omitempty"`
	RiskControl *RiskControl `json:"risk_control,omitempty"`
}

type PaymentIntent struct {
	Amount               Decimal               `json:"amount,omitempty"`
	CancellationReason   string                `json:"cancellation_reason,omitempty"`
	CancelledAt          *TimeISO8601          `json:"cancelled_at,omitempty"`
	CapturedAmount       Decimal               `json:"captured_amount,omitempty"`
	ClientSecret         string                `json:"client_secret,omitempty"`
	ConnectedAccountID   string                `json:"connected_account_id,omitempty"`
	CreatedAt            *TimeISO8601          `json:"created_at,omitempty"`
	Currency             Currency              `json:"currency,omitempty"`
	CustomerID           string                `json:"customer_id,omitempty"`
	Descriptor           string                `json:"descriptor,omitempty"`
	ID                   string                `json:"id,omitempty"`
	LatestPaymentAttempt *PaymentAttempt       `json:"latest_payment_attempt,omitempty"`
	MerchantOrderID      string                `json:"merchant_order_id,omitempty"`
	Metadata             Metadata              `json:"metadata,omitempty"`
	NextAction           *NextActionBasic      `json:"next_action,omitempty"`
	Order                *Order                `json:"order,omitempty"`
	PaymentConsentID     string                `json:"payment_consent_id,omitempty"`
	PaymentLinkID        string                `json:"payment_link_id,omitempty"`
	PaymentMethodOptions *PaymentMethodOptions `json:"payment_method_options,omitempty"`
	Status               PaymentIntentStatus   `json:"status,omitempty"`
	UpdatedAt            *TimeISO8601          `json:"updated_at,omitempty"`
}

type PaymentIntentStatus string

type PaymentLink struct {
	Active      bool         `json:"active,omitempty"`
	Amount      Decimal      `json:"amount,omitempty"`
	Currency    Currency     `json:"currency,omitempty"`
	Code        string       `json:"code,omitempty"`
	URL         string       `json:"url,omitempty"`
	Description string       `json:"description,omitempty"`
	ExpiresAt   *TimeISO8601 `json:"expires_at,omitempty"`
	ID          string       `json:"id,omitempty"`
	Title       string       `json:"title,omitempty"`
	Reference   string       `json:"reference,omitempty"`
	Reusable    bool         `json:"reusable,omitempty"`
	CreatedAt   *TimeISO8601 `json:"created_at,omitempty"`
	UpdatedAt   *TimeISO8601 `json:"updated_at,omitempty"`
}

type PaymentMethod struct {
	Card       *Card               `json:"card,omitempty"`
	CreatedAt  *TimeISO8601        `json:"created_at,omitempty"`
	CustomerID string              `json:"customer_id,omitempty"`
	ID         string              `json:"id,omitempty"`
	Metadata   Metadata            `json:"metadata,omitempty"`
	Status     PaymentMethodStatus `json:"status,omitempty"`
	Type       PaymentMethodName   `json:"type,omitempty"`
	UpdatedAt  *TimeISO8601        `json:"updated_at,omitempty"`
}

type PaymentMethods struct {
	Alfamart        *PaymentMethodAlfamart        `json:"alfamart,omitempty"`
	AlipayCN        *PaymentMethodAlipayCN        `json:"alipaycn,omitempty"`
	Alipayhk        *PaymentMethodAlipayHK        `json:"alipayhk,omitempty"`
	ApplePay        *PaymentMethodApplePay        `json:"applepay,omitempty"`
	AxsKiosk        *PaymentMethodAXSKiosk        `json:"axs_kiosk,omitempty"`
	Bancontact      *PaymentMethodBancontact      `json:"bancontact,omitempty"`
	BankTransfer    *PaymentMethodBankTransfer    `json:"bank_transfer,omitempty"`
	Bigc            *PaymentMethodBigC            `json:"bigc,omitempty"`
	BitPay          *PaymentMethodBitPay          `json:"bitpay,omitempty"`
	Blik            *PaymentMethodBLIK            `json:"blik,omitempty"`
	Boost           *PaymentMethodBoost           `json:"boost,omitempty"`
	Card            *PaymentMethodCard            `json:"card,omitempty"`
	DANA            *PaymentMethodDANA            `json:"dana,omitempty"`
	DOKUEWallet     *PaymentMethodDOKUEWallet     `json:"doku_ewallet,omitempty"`
	Dragonpay       *PaymentMethodDragonpay       `json:"dragonpay,omitempty"`
	ENETS           *PaymentMethodENETS           `json:"enets,omitempty"`
	EPS             *PaymentMethodEPS             `json:"eps,omitempty"`
	ESUN            *PaymentMethodESUN            `json:"esun,omitempty"`
	FamilyMart      *PaymentMethodFamilyMart      `json:"family_mart,omitempty"`
	FPX             *PaymentMethodFPX             `json:"fpx,omitempty"`
	GCash           *PaymentMethodGCash           `json:"gcash,omitempty"`
	Giropay         *PaymentMethodGiropay         `json:"giropay,omitempty"`
	GrabPay         *PaymentMethodGrabPay         `json:"grabpay,omitempty"`
	HiLife          *PaymentMethodHiLife          `json:"hi_life,omitempty"`
	IDEAL           *PaymentMethodIDEAL           `json:"ideal,omitempty"`
	Indomaret       *PaymentMethodIndomaret       `json:"indomaret,omitempty"`
	KakaoPay        *PaymentMethodKakaoPay        `json:"kakaopay,omitempty"`
	Konbini         *PaymentMethodKonbini         `json:"konbini,omitempty"`
	Maxima          *PaymentMethodMaxima          `json:"maxima,omitempty"`
	Multibanco      *PaymentMethodMultibanco      `json:"multibanco,omitempty"`
	MyBank          *PaymentMethodMyBank          `json:"mybank,omitempty"`
	Narvesen        *PaymentMethodNarvesen        `json:"narvesen,omitempty"`
	OnlineBanking   *PaymentMethodOnlineBanking   `json:"online_banking,omitempty"`
	OVO             *PaymentMethodOVO             `json:"ovo,omitempty"`
	P24             *PaymentMethodP24             `json:"p24,omitempty"`
	PaybyBankapp    *PaymentMethodPaybyBankapp    `json:"paybybankapp,omitempty"`
	PayEasy         *PaymentMethodPayEasy         `json:"payeasy,omitempty"`
	PayPal          *PaymentMethodPayPal          `json:"paypal,omitempty"`
	PayPost         *PaymentMethodPayPost         `json:"paypost,omitempty"`
	Paysafecard     *PaymentMethodPaysafecard     `json:"paysafecard,omitempty"`
	Paysafecash     *PaymentMethodPaysafecash     `json:"paysafecash,omitempty"`
	Paysera         *PaymentMethodPaysera         `json:"paysera,omitempty"`
	PayU            *PaymentMethodPayU            `json:"payu,omitempty"`
	PerlasTerminals *PaymentMethodPerlasTerminals `json:"perlas_terminals,omitempty"`
	PermataATM      *PaymentMethodPermataATM      `json:"permata_atm,omitempty"`
	PermataNet      *PaymentMethodPermataNet      `json:"permatanet,omitempty"`
	POLi            *PaymentMethodPOLi            `json:"poli,omitempty"`
	SAMKiosk        *PaymentMethodSAMKiosk        `json:"sam_kiosk,omitempty"`
	Satispay        *PaymentMethodSatispay        `json:"satispay,omitempty"`
	SevenEleven     *PaymentMethodSevenEleven     `json:"seven_eleven,omitempty"`
	ShopeePay       *PaymentMethodShopeePay       `json:"shopee_pay,omitempty"`
	Skrill          *PaymentMethodSkrill          `json:"skrill,omitempty"`
	SOFORT          *PaymentMethodSOFORT          `json:"sofort,omitempty"`
	TescoLotus      *PaymentMethodTescoLotus      `json:"tesco_lotus,omitempty"`
	Tng             *PaymentMethodTNG             `json:"tng,omitempty"`
	Truemoney       *PaymentMethodTrueMoney       `json:"truemoney,omitempty"`
	Trustly         *PaymentMethodTrustly         `json:"trustly,omitempty"`
	Type            PaymentMethodName             `json:"type,omitempty"`
	Verkkopankki    *PaymentMethodVerkkopankki    `json:"verkkopankki,omitempty"`
	WeChatPay       *PaymentMethodWeChatPay       `json:"wechatpay,omitempty"`
}

type PaymentMethodAlfamart struct {
	ShopperEmail string `json:"shopper_email,omitempty"`
	ShopperName  string `json:"shopper_name,omitempty"`
}

type PaymentMethodAlipayCN struct {
	Flow   PaymentMethodFlow `json:"flow,omitempty"`
	OSType OSType            `json:"os_type,omitempty"`
}

type PaymentMethodAlipayHK struct {
	Flow   PaymentMethodFlow `json:"flow,omitempty"`
	OSType OSType            `json:"os_type,omitempty"`
}

type PaymentMethodApplePay struct {
	Billing            *Billing  `json:"billing,omitempty"`
	CardBrand          CardBrand `json:"card_brand,omitempty"`
	CardType           CardType2 `json:"card_type,omitempty"`
	Data               string    `json:"data,omitempty"`
	EphemeralPublicKey string    `json:"ephemeral_public_key,omitempty"`
	PublicKeyHash      string    `json:"public_key_hash,omitempty"`
	Signature          string    `json:"signature,omitempty"`
	TransactionID      string    `json:"transaction_id,omitempty"`
	Version            string    `json:"version,omitempty"`
	WrappedKey         string    `json:"wrapped_key,omitempty"`
}

type PaymentMethodAXSKiosk struct {
	ShopperEmail string `json:"shopper_email,omitempty"`
	ShopperName  string `json:"shopper_name,omitempty"`
	ShopperPhone string `json:"shopper_phone,omitempty"`
}

type PaymentMethodBancontact struct {
	ShopperName string `json:"shopper_name,omitempty"`
}

type PaymentMethodBankTransfer struct {
	BankName     string `json:"bank_name,omitempty"`
	CountryCode  string `json:"country_code,omitempty"`
	ShopperEmail string `json:"shopper_email,omitempty"`
	ShopperName  string `json:"shopper_name,omitempty"`
	ShopperPhone string `json:"shopper_phone,omitempty"`
}

type PaymentMethodBigC struct {
	ShopperEmail string `json:"shopper_email,omitempty"`
	ShopperName  string `json:"shopper_name,omitempty"`
	ShopperPhone string `json:"shopper_phone,omitempty"`
}

type PaymentMethodBitPay struct {
	CountryCode string `json:"country_code,omitempty"`
	ShopperName string `json:"shopper_name,omitempty"`
}

type PaymentMethodBLIK struct {
	ShopperName string `json:"shopper_name,omitempty"`
}

type PaymentMethodBoost struct {
	ShopperEmail string `json:"shopper_email,omitempty"`
	ShopperName  string `json:"shopper_name,omitempty"`
	ShopperPhone string `json:"shopper_phone,omitempty"`
}

type PaymentMethodCard struct {
	AVCCheck          string    `json:"avc_check,omitempty"` // TODO: enum?
	Billing           *Billing  `json:"billing,omitempty"`
	BIN               string    `json:"bin,omitempty"`
	Brand             CardBrand `json:"brand,omitempty"`
	CardType          CardType2 `json:"card_type,omitempty"`
	CVCCheck          string    `json:"cvc_check,omitempty"` // TODO: enum?
	ExpiryMonth       string    `json:"expiry_month,omitempty"`
	ExpiryYear        string    `json:"expiry_year,omitempty"`
	Fingerprint       string    `json:"fingerprint,omitempty"`
	IssuerCountryCode string    `json:"issuer_country_code,omitempty"`
	Last4             string    `json:"last4,omitempty"`
	Name              string    `json:"name,omitempty"`
}

type PaymentMethodDANA struct {
	Flow   PaymentMethodFlow `json:"flow,omitempty"`
	OSType OSType            `json:"os_type,omitempty"`
}

type PaymentMethodDOKUEWallet struct {
	ShopperEmail string `json:"shopper_email,omitempty"`
	ShopperName  string `json:"shopper_name,omitempty"`
}

type PaymentMethodDragonpay struct {
	ShopperEmail string `json:"shopper_email,omitempty"`
	ShopperName  string `json:"shopper_name,omitempty"`
	ShopperPhone string `json:"shopper_phone,omitempty"`
}

type PaymentMethodENETS struct {
	BankName     string `json:"bank_name,omitempty"`
	ShopperEmail string `json:"shopper_email,omitempty"`
	ShopperName  string `json:"shopper_name,omitempty"`
	ShopperPhone string `json:"shopper_phone,omitempty"`
}

type PaymentMethodEPS struct {
	ShopperName string `json:"shopper_name,omitempty"`
}

type PaymentMethodESUN struct {
	ShopperEmail string `json:"shopper_email,omitempty"`
	ShopperName  string `json:"shopper_name,omitempty"`
	ShopperPhone string `json:"shopper_phone,omitempty"`
}

type PaymentMethodFamilyMart struct {
	ShopperEmail string `json:"shopper_email,omitempty"`
	ShopperName  string `json:"shopper_name,omitempty"`
	ShopperPhone string `json:"shopper_phone,omitempty"`
}

type PaymentMethodFlow string

type PaymentMethodFPX struct {
	BankName     string `json:"bank_name,omitempty"`
	ShopperEmail string `json:"shopper_email,omitempty"`
	ShopperName  string `json:"shopper_name,omitempty"`
	ShopperPhone string `json:"shopper_phone,omitempty"`
}

type PaymentMethodGCash struct {
	Flow   PaymentMethodFlow `json:"flow,omitempty"`
	OSType OSType            `json:"os_type,omitempty"`
}

type PaymentMethodGiropay struct {
	ShopperName string `json:"shopper_name,omitempty"`
}

type PaymentMethodGrabPay struct {
	ShopperName string `json:"shopper_name,omitempty"`
}

type PaymentMethodHiLife struct {
	ShopperEmail string `json:"shopper_email,omitempty"`
	ShopperName  string `json:"shopper_name,omitempty"`
	ShopperPhone string `json:"shopper_phone,omitempty"`
}

type PaymentMethodIDEAL struct {
	BankName    string `json:"bank_name,omitempty"`
	ShopperName string `json:"shopper_name,omitempty"`
}

type PaymentMethodIndomaret struct {
	ShopperEmail string `json:"shopper_email,omitempty"`
	ShopperName  string `json:"shopper_name,omitempty"`
}

type PaymentMethodKakaoPay struct {
	Flow   PaymentMethodFlow `json:"flow,omitempty"`
	OSType OSType            `json:"os_type,omitempty"`
}

type PaymentMethodKonbini struct {
	ShopperEmail string `json:"shopper_email,omitempty"`
	ShopperName  string `json:"shopper_name,omitempty"`
	ShopperPhone string `json:"shopper_phone,omitempty"`
}

type PaymentMethodMaxima struct {
	ShopperName string `json:"shopper_name,omitempty"`
}

type PaymentMethodMultibanco struct {
	ShopperName string `json:"shopper_name,omitempty"`
}

type PaymentMethodMyBank struct {
	ShopperName string `json:"shopper_name,omitempty"`
}

type PaymentMethodName string

type PaymentMethodNarvesen struct {
	ShopperName string `json:"shopper_name,omitempty"`
}

type PaymentMethodOnlineBanking struct {
	BankName     string `json:"bank_name,omitempty"`
	CountryCode  string `json:"country_code,omitempty"`
	ShopperEmail string `json:"shopper_email,omitempty"`
	ShopperName  string `json:"shopper_name,omitempty"`
	ShopperPhone string `json:"shopper_phone,omitempty"`
}

type PaymentMethodOptions struct {
	Card *PaymentMethodOptionsCard `json:"card,omitempty"`
}

type PaymentMethodOptionsCard struct {
	RiskControl *CardRiskControl `json:"risk_control,omitempty"`
}

type PaymentMethodOVO struct {
	ShopperEmail string `json:"shopper_email,omitempty"`
	ShopperName  string `json:"shopper_name,omitempty"`
}

type PaymentMethodP24 struct {
	ShopperEmail string `json:"shopper_email,omitempty"`
	ShopperName  string `json:"shopper_name,omitempty"`
}

type PaymentMethodPaybyBankapp struct {
	ShopperName string `json:"shopper_name,omitempty"`
}

type PaymentMethodPayEasy struct {
	ShopperEmail string `json:"shopper_email,omitempty"`
	ShopperName  string `json:"shopper_name,omitempty"`
	ShopperPhone string `json:"shopper_phone,omitempty"`
}

type PaymentMethodPayPal struct {
	CountryCode string `json:"country_code,omitempty"`
	ShopperName string `json:"shopper_name,omitempty"`
}

type PaymentMethodPayPost struct {
	ShopperName string `json:"shopper_name,omitempty"`
}

type PaymentMethodPaysafecard struct {
	CountryCode string `json:"country_code,omitempty"`
	ShopperName string `json:"shopper_name,omitempty"`
}

type PaymentMethodPaysafecash struct {
	CountryCode  string `json:"country_code,omitempty"`
	ShopperEmail string `json:"shopper_email,omitempty"`
	ShopperName  string `json:"shopper_name,omitempty"`
}

type PaymentMethodPaysera struct {
	BankName    string `json:"bank_name,omitempty"`
	CountryCode string `json:"country_code,omitempty"`
	ShopperName string `json:"shopper_name,omitempty"`
}

type PaymentMethodPayU struct {
	ShopperName string `json:"shopper_name,omitempty"`
}

type PaymentMethodPerlasTerminals struct {
	ShopperName string `json:"shopper_name,omitempty"`
}

type PaymentMethodPermataATM struct {
	ShopperEmail string `json:"shopper_email,omitempty"`
	ShopperName  string `json:"shopper_name,omitempty"`
	ShopperPhone string `json:"shopper_phone,omitempty"`
}

type PaymentMethodPermataNet struct {
	ShopperEmail string `json:"shopper_email,omitempty"`
	ShopperName  string `json:"shopper_name,omitempty"`
}

type PaymentMethodPOLi struct {
	ShopperName string `json:"shopper_name,omitempty"`
}

type PaymentMethodSAMKiosk struct {
	ShopperEmail string `json:"shopper_email,omitempty"`
	ShopperName  string `json:"shopper_name,omitempty"`
	ShopperPhone string `json:"shopper_phone,omitempty"`
}

type PaymentMethodSatispay struct {
	CountryCode string `json:"country_code,omitempty"`
	ShopperName string `json:"shopper_name,omitempty"`
}

type PaymentMethodSevenEleven struct {
	ShopperEmail string `json:"shopper_email,omitempty"`
	ShopperName  string `json:"shopper_name,omitempty"`
	ShopperPhone string `json:"shopper_phone,omitempty"`
}

type PaymentMethodShopeePay struct {
	ShopperEmail string `json:"shopper_email,omitempty"`
	ShopperName  string `json:"shopper_name,omitempty"`
	ShopperPhone string `json:"shopper_phone,omitempty"`
}

type PaymentMethodSkrill struct {
	CountryCode  string `json:"country_code,omitempty"`
	ShopperEmail string `json:"shopper_email,omitempty"`
	ShopperName  string `json:"shopper_name,omitempty"`
}

type PaymentMethodSOFORT struct {
	CountryCode string `json:"country_code,omitempty"`
	ShopperName string `json:"shopper_name,omitempty"`
}

type PaymentMethodStatus string

type PaymentMethodTescoLotus struct {
	ShopperEmail string `json:"shopper_email,omitempty"`
	ShopperName  string `json:"shopper_name,omitempty"`
	ShopperPhone string `json:"shopper_phone,omitempty"`
}

type PaymentMethodTNG struct {
	Flow   PaymentMethodFlow `json:"flow,omitempty"`
	OSType OSType            `json:"os_type,omitempty"`
}

type PaymentMethodTrueMoney struct {
	Flow   PaymentMethodFlow `json:"flow,omitempty"`
	OSType OSType            `json:"os_type,omitempty"`
}

type PaymentMethodTrustly struct {
	CountryCode string `json:"country_code,omitempty"`
	ShopperName string `json:"shopper_name,omitempty"`
}

type PaymentMethodVerkkopankki struct {
	BankName     string `json:"bank_name,omitempty"`
	ShopperEmail string `json:"shopper_email,omitempty"`
	ShopperName  string `json:"shopper_name,omitempty"`
}

type PaymentMethodWeChatPay struct {
	Flow      PaymentMethodFlow `json:"flow,omitempty"`
	IpAddress string            `json:"ip_address,omitempty"`
	OpenID    string            `json:"open_id,omitempty"`
}

type PaymentStatus string

type PerTransactionLimit struct {
	Currency Currency `json:"currency,omitempty"`
	Default  Decimal  `json:"default,omitempty"`
	Maximum  Decimal  `json:"maximum,omitempty"`
}

type PrefilledFormData struct {
	Editable bool   `json:"editable,omitempty"`
	Field    string `json:"field,omitempty"`
	Value    string `json:"value,omitempty"`
	Verified bool   `json:"verified,omitempty"`
}

type PrefilledFormDataField string

type Product struct {
	Code      string  `json:"code,omitempty"`
	Desc      string  `json:"desc,omitempty"`
	Name      string  `json:"name,omitempty"`
	Quantity  int32   `json:"quantity,omitempty"`
	Sku       string  `json:"sku,omitempty"`
	Type      string  `json:"type,omitempty"`
	UnitPrice Decimal `json:"unit_price,omitempty"`
	URL       string  `json:"url,omitempty"`
}

type ProviderOriginalResponseCode string

type Quote struct {
	ClientRate         Decimal      `json:"client_rate,omitempty"`
	CreatedAt          *TimeISO8601 `json:"created_at,omitempty"`
	CurrencyPair       CurrencyPair `json:"currency_pair,omitempty"`
	ID                 string       `json:"id,omitempty"`
	PaymentCurrency    Currency     `json:"payment_currency,omitempty"`
	SettlementCurrency Currency     `json:"settlement_currency,omitempty"`
	Type               QuoteType    `json:"type,omitempty"`
	ValidFrom          *TimeISO8601 `json:"valid_from,omitempty"`
	ValidTo            *TimeISO8601 `json:"valid_to,omitempty"`
}

type QuoteType string

type Refund struct {
	AcquirerReferenceNumber string       `json:"acquirer_reference_number,omitempty"`
	Amount                  Decimal      `json:"amount,omitempty"`
	CreatedAt               *TimeISO8601 `json:"created_at,omitempty"`
	Currency                Currency     `json:"currency,omitempty"`
	ID                      string       `json:"id,omitempty"`
	Metadata                Metadata     `json:"metadata,omitempty"`
	PaymentAttemptID        string       `json:"payment_attempt_id,omitempty"`
	PaymentIntentID         string       `json:"payment_intent_id,omitempty"`
	Reason                  string       `json:"reason,omitempty"`
	RequestID               uuid.UUID    `json:"request_id,omitempty"`
	Status                  RefundStatus `json:"status,omitempty"`
	UpdatedAt               *TimeISO8601 `json:"updated_at,omitempty"`
}

type RefundStatus string

type RegistrationInfo struct {
	Agreement         bool               `json:"agreement,omitempty"`
	RegisteredAddress *RegisteredAddress `json:"registered_address,omitempty"`
	RegisteredEmail   string             `json:"registered_email,omitempty"`
	RegisteredName    string             `json:"registered_name,omitempty"`
}

type RegisteredAddress struct {
	Address  string `json:"address,omitempty"`
	City     string `json:"city,omitempty"`
	Country  string `json:"country,omitempty"`
	Postcode string `json:"postcode,omitempty"`
	State    string `json:"state,omitempty"`
}

type Resources struct {
	LogoURL string `json:"logo_url,omitempty"`
}

type RiskControl struct {
	SkipRiskProcessing      bool   `json:"skip_risk_processing,omitempty"`
	ThreeDomainSecureAction string `json:"three_domain_secure_action,omitempty"`
	ThreeDSAction           string `json:"three_ds_action,omitempty"`
}

type SchemaCondition struct {
	AccountCurrency     Currency                     `json:"account_currency,omitempty"`
	BankCountryCode     string                       `json:"bank_country_code,omitempty"`
	EntityType          EntityType                   `json:"entity_type,omitempty"`
	LocalClearingSystem ClearingSystem               `json:"local_clearing_system,omitempty"`
	PaymentMethod       []GlobalAccountPaymentMethod `json:"payment_method,omitempty"`
}

type Settlement struct {
	Amount             Decimal           `json:"amount,omitempty"`
	CreatedAt          *TimeISO8601      `json:"created_at,omitempty"`
	Currency           Currency          `json:"currency,omitempty"`
	EstimatedSettledAt *TimeISO8601      `json:"estimated_settled_at,omitempty"`
	Fee                Decimal           `json:"fee,omitempty"`
	ID                 uuid.UUID         `json:"id,omitempty"`
	SettledAt          *TimeISO8601      `json:"settled_at,omitempty"`
	Status             TransactionStatus `json:"status,omitempty"`
}

type SettleVia string

type Shipping struct {
	Address        *AddressBasic `json:"address,omitempty"`
	FirstName      string        `json:"first_name,omitempty"`
	LastName       string        `json:"last_name,omitempty"`
	PhoneNumber    string        `json:"phone_number,omitempty"`
	ShippingMethod string        `json:"shipping_method,omitempty"`
}

type ShopperDetails struct {
	ShopperID   string `json:"shopper_id,omitempty"`
	ShopperName string `json:"shopper_name,omitempty"`
}

type SourceType string

type SpendingLimitSettings struct {
	PerTransactionLimits []*PerTransactionLimit `json:"per_transaction_limits,omitempty"`
}

type SubOrder struct {
	Amount          Decimal  `json:"amount,omitempty"`
	Currency        Currency `json:"currency,omitempty"`
	OrderNumber     string   `json:"order_number,omitempty"`
	ProviderOrderID string   `json:"provider_order_id,omitempty"`
	ShippingFee     Decimal  `json:"shipping_fee,omitempty"`
}

type SWIFTChargeOption string

type TransactionMode string

type Transfer struct {
	Amount      Decimal        `json:"amount,omitempty"`
	CreatedAt   *TimeISO8601   `json:"created_at,omitempty"`
	Currency    Currency       `json:"currency,omitempty"`
	Destination string         `json:"destination,omitempty"`
	Fee         Decimal        `json:"fee,omitempty"`
	ID          uuid.UUID      `json:"id,omitempty"`
	Reason      string         `json:"reason,omitempty"`
	Reference   string         `json:"reference,omitempty"`
	RequestID   uuid.UUID      `json:"request_id,omitempty"`
	Status      TransferStatus `json:"status,omitempty"`
	UpdatedAt   *TimeISO8601   `json:"updated_at,omitempty"`
}

type TransferStatus string

type ThreeDS struct {
	AcsResponse             string `json:"acs_response,omitempty"`
	DeviceDataCollectionRes string `json:"device_data_collection_res,omitempty"`
	ReturnURL               string `json:"return_url,omitempty"`
}

type ThreeDSAction string

type TransactionStatus string

type TransactionType string

type TriggeredBy string

// Basic types

type Currency string

type CurrencyPair string

// TODO: Country Code

type Decimal float64

type Metadata interface{}

type TimeISO8601 struct {
	Time time.Time
}

func (t *TimeISO8601) UnmarshalJSON(data []byte) (err error) {
	var v string
	if err = json.Unmarshal(data, &v); err != nil {
		return errors.Wrap(err, "json.Unmarshal()")
	}

	var tt time.Time
	if tt, err = time.Parse("2006-01-02T15:04:05Z0700", v); err != nil {
		return errors.Wrap(err, "time.Parse()")
	}

	t.Time = tt

	return err
}
