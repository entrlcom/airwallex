package airwallex

import (
	"context"
	"net/http"

	"github.com/pkg/errors"
)

// https://www.airwallex.com/docs/api#/Transactional_FX/Lock_FX/_api_v1_lockfx_create/post

type CreateLockFxQuoteRequest struct {
	BuyAmount      Decimal             `json:"buy_amount,omitempty"`
	BuyCurrency    Currency            `json:"buy_currency,omitempty"`
	ConversionDate string              `json:"conversion_date,omitempty"` // TODO: Time (YYYY-MM-DD)
	SellAmount     Decimal             `json:"sell_amount,omitempty"`
	SellCurrency   Currency            `json:"sell_currency,omitempty"`
	Validity       LockFXQuoteValidity `json:"validity,omitempty"`
}

type CreateLockFxQuoteResponse LockFXQuote

func (awx airwallex) CreateLockFxQuote(
	ctx context.Context,
	in *CreateLockFxQuoteRequest,
) (out *CreateLockFxQuoteResponse, err error) {
	out = new(CreateLockFxQuoteResponse)
	uri := string(awx.apiEndpoint) + string(createLockFxQuoteEndpoint)
	if err = Do(ctx, awx.httpClient, http.MethodPost, uri, in, out); err != nil {
		return out, errors.Wrap(err, "Do()")
	}

	return out, err
}
