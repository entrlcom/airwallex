package airwallex

import (
	"context"
	"net/http"

	"github.com/google/uuid"
	"github.com/pkg/errors"
)

// https://www.airwallex.com/docs/api#/Payment_Acceptance/Refunds/_api_v1_pa_refunds_create/post

type CreateRefundRequest struct {
	Amount           Decimal   `json:"amount,omitempty"`
	Metadata         Metadata  `json:"metadata,omitempty"`
	PaymentIntentID  string    `json:"payment_intent_id,omitempty"`
	PaymentAttemptID string    `json:"payment_attempt_id,omitempty"`
	Reason           string    `json:"reason,omitempty"`
	RequestID        uuid.UUID `json:"request_id,omitempty"`
}

type CreateRefundResponse Refund

func (awx airwallex) CreateRefund(
	ctx context.Context,
	in *CreateRefundRequest,
) (out *CreateRefundResponse, err error) {
	out = new(CreateRefundResponse)
	uri := string(awx.apiEndpoint) + string(createRefundEndpoint)
	if err = Do(ctx, awx.httpClient, http.MethodPost, uri, in, out); err != nil {
		return out, errors.Wrap(err, "Do()")
	}

	return out, err
}

// https://www.airwallex.com/docs/api#/Payment_Acceptance/Refunds/_api_v1_pa_refunds__id_/get

type GetRefundRequest struct {
	ID string `json:"id,omitempty"`
}

type GetRefundResponse Refund

func (awx airwallex) GetRefund(
	ctx context.Context,
	in *GetRefundRequest,
) (out *GetRefundResponse, err error) {
	out = new(GetRefundResponse)
	uri := string(awx.apiEndpoint) + string(getRefundEndpoint)
	if err = Do(ctx, awx.httpClient, http.MethodGet, uri, in, out); err != nil {
		return out, errors.Wrap(err, "Do()")
	}

	return out, err
}

// https://www.airwallex.com/docs/api#/Payment_Acceptance/Refunds/_api_v1_pa_refunds/get

type ListRefundsRequest struct {
	Currency         Currency     `json:"currency,omitempty"`
	FromCreatedAt    *TimeISO8601 `json:"from_created_at,omitempty"`
	PageNum          int32        `json:"page_num,omitempty"`
	PageSize         int32        `json:"page_size,omitempty"`
	PaymentAttemptID string       `json:"payment_attempt_id,omitempty"`
	PaymentIntentID  string       `json:"payment_intent_id,omitempty"`
	Status           RefundStatus `json:"status,omitempty"`
	ToCreatedAt      *TimeISO8601 `json:"to_created_at,omitempty"`
}

type ListRefundsResponse struct {
	HasMore string    `json:"has_more,omitempty"`
	Items   []*Refund `json:"items,omitempty"`
}

func (awx airwallex) ListRefunds(
	ctx context.Context,
	in *ListRefundsRequest,
) (out *ListRefundsResponse, err error) {
	out = new(ListRefundsResponse)
	uri := string(awx.apiEndpoint) + string(listRefundEndpoint)
	if err = Do(ctx, awx.httpClient, http.MethodGet, uri, in, out); err != nil {
		return out, errors.Wrap(err, "Do()")
	}

	return out, err
}
