package airwallex

import (
	"context"
	"net/http"

	"github.com/pkg/errors"
)

// https://www.airwallex.com/docs/api#/Payment_Acceptance/Quote/_api_v1_pa_quotes_create/post

type CreateQuoteRequest struct {
	PaymentCurrency    Currency  `json:"payment_currency,omitempty"`
	SettlementCurrency Currency  `json:"settlement_currency,omitempty"`
	Type               QuoteType `json:"type,omitempty"`
}

type CreateQuoteResponse Quote

func (awx airwallex) CreateQuote(
	ctx context.Context,
	in *CreateQuoteRequest,
) (out *CreateQuoteResponse, err error) {
	out = new(CreateQuoteResponse)
	uri := string(awx.apiEndpoint) + string(createQuoteEndpoint)
	if err = Do(ctx, awx.httpClient, http.MethodPost, uri, in, out); err != nil {
		return out, errors.Wrap(err, "Do()")
	}

	return out, err
}

// https://www.airwallex.com/docs/api#/Payment_Acceptance/Quote/_api_v1_pa_quotes__id_/get

type GetQuoteRequest struct {
	ID string `json:"id,omitempty"`
}

type GetQuoteResponse Quote

func (awx airwallex) GetQuote(
	ctx context.Context,
	in *GetQuoteRequest,
) (out *GetQuoteResponse, err error) {
	out = new(GetQuoteResponse)
	uri := string(awx.apiEndpoint) + string(getQuoteEndpoint)
	if err = Do(ctx, awx.httpClient, http.MethodGet, uri, in, out); err != nil {
		return out, errors.Wrap(err, "Do()")
	}

	return out, err
}

// https://www.airwallex.com/docs/api#/Payment_Acceptance/Quote/_api_v1_pa_quotes/get

type ListQuotesRequest struct {
	FromCreatedAt      *TimeISO8601 `json:"from_created_at,omitempty"`
	PageNum            int32        `json:"page_num,omitempty"`
	PageSize           int32        `json:"page_size,omitempty"`
	PaymentCurrency    Currency     `json:"payment_currency,omitempty"`
	SettlementCurrency Currency     `json:"settlement_currency,omitempty"`
	ToCreatedAt        *TimeISO8601 `json:"to_created_at,omitempty"`
}

type ListQuotesResponse struct {
	HasMore bool     `json:"has_more,omitempty"`
	Items   []*Quote `json:"items,omitempty"`
}

func (awx airwallex) ListQuotes(
	ctx context.Context,
	in *ListQuotesRequest,
) (out *ListQuotesResponse, err error) {
	out = new(ListQuotesResponse)
	uri := string(awx.apiEndpoint) + string(listQuotesEndpoint)
	if err = Do(ctx, awx.httpClient, http.MethodGet, uri, in, out); err != nil {
		return out, errors.Wrap(err, "Do()")
	}

	return out, err
}
