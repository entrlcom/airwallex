package airwallex

import (
	"context"
	"testing"
	"time"

	"github.com/google/uuid"
	"github.com/stretchr/testify/assert"
)

const (
	host     = string(apiDemoEndpoint)
	apiKey   = "e4fcaa48bcbcef1b630f8ca839897c6fda9a8b35a45244e8cfcf69233c3d8d75566962b001f47096f3698afc7a964063"
	clientID = "RP0FNXvGTsGWuOpqKw688A"
)

func TestTransport_RoundTrip(t *testing.T) {
	awx := NewAirwallex(host, apiKey, clientID)

	start := time.Now()
	res, err := awx.CreatePaymentIntent(context.TODO(), &CreatePaymentIntentRequest{
		Amount:          Decimal(200.00),
		Currency:        Currency("EUR"),
		MerchantOrderID: uuid.New().String(),
		PaymentMethodOptions: &PaymentMethodOptions{
			Card: &PaymentMethodOptionsCard{
				RiskControl: &CardRiskControl{
					SkipRiskProcessing: false,
					ThreeDSAction:      ThreeDSActionForce3DS,
				},
			},
		},
		RequestID: uuid.New(),
		ReturnURL: "https://pay.entrl.com/" + uuid.New().String(),
	})
	println("first req: ", time.Since(start))
	assert.NoError(t, err)
	assert.NotNil(t, res)

	for i := 1; i < 10; i++ {
		start = time.Now()
		res, err = awx.CreatePaymentIntent(context.TODO(), &CreatePaymentIntentRequest{
			Amount:          Decimal(200.00),
			Currency:        Currency("EUR"),
			MerchantOrderID: uuid.New().String(),
			PaymentMethodOptions: &PaymentMethodOptions{
				Card: &PaymentMethodOptionsCard{
					RiskControl: &CardRiskControl{
						SkipRiskProcessing: false,
						ThreeDSAction:      ThreeDSActionForce3DS,
					},
				},
			},
			RequestID: uuid.New(),
			ReturnURL: "https://pay.entrl.com/" + uuid.New().String(),
		})
		assert.NoError(t, err)
		assert.NotNil(t, res)
		println("req №: ", i, " ", time.Since(start))
	}
}

// func TestTransport_RoundTrip(t *testing.T) {
// 	awx := NewAirwallex(host, apiKey, clientID)
//
// 	start := time.Now()
// 	res, err := awx.GetBalanceHistory(context.Background(), &GetBalanceHistoryRequest{
// 		Currency:   "",
// 		FromPostAt: nil,
// 		PageNum:    0,
// 		PageSize:   0,
// 		RequestID:  "",
// 		ToPostAt:   nil,
// 	})
// 	println("first req: ", time.Since(start))
// 	assert.NoError(t, err)
// 	assert.NotNil(t, res)
//
// 	for i := 1; i < 10; i++ {
// 		start = time.Now()
// 		res, err = awx.GetBalanceHistory(context.Background(), &GetBalanceHistoryRequest{
// 			Currency:   "",
// 			FromPostAt: nil,
// 			PageNum:    0,
// 			PageSize:   0,
// 			RequestID:  "",
// 			ToPostAt:   nil,
// 		})
// 		assert.NoError(t, err)
// 		assert.NotNil(t, res)
// 		println("req №: ", i, " ", time.Since(start))
// 	}
// }
