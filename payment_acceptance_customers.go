package airwallex

import (
	"context"
	"net/http"

	"github.com/google/uuid"
	"github.com/pkg/errors"
)

// https://www.airwallex.com/docs/api#/Payment_Acceptance/Customers/_api_v1_pa_customers_create/post

type CreateCustomerRequest struct {
	AdditionalInfo     []*AdditionalInfoBasic `json:"additional_info,omitempty"`
	Email              string                 `json:"email,omitempty"`
	FirstName          string                 `json:"first_name,omitempty"`
	LastName           string                 `json:"last_name,omitempty"`
	MerchantCustomerID string                 `json:"merchant_customer_id,omitempty"`
	Metadata           Metadata               `json:"metadata,omitempty"`
	PhoneNumber        string                 `json:"phone_number,omitempty"`
	RequestID          uuid.UUID              `json:"request_id,omitempty"`
}

type CreateCustomerResponse struct {
	Customer
	RequestID uuid.UUID `json:"request_id,omitempty"`
}

func (awx airwallex) CreateCustomer(
	ctx context.Context,
	in *CreateCustomerRequest,
) (out *CreateCustomerResponse, err error) {
	out = new(CreateCustomerResponse)
	uri := string(awx.apiEndpoint) + string(createCustomerEndpoint)
	if err = Do(ctx, awx.httpClient, http.MethodPost, uri, in, out); err != nil {
		return out, errors.Wrap(err, "Do()")
	}

	return out, err
}

// https://www.airwallex.com/docs/api#/Payment_Acceptance/Customers/_api_v1_pa_customers__id_/get

type GetCustomerRequest struct {
	ID string `json:"id,omitempty"`
}

type GetCustomerResponse struct {
	Customer
	RequestID uuid.UUID `json:"request_id,omitempty"`
}

func (awx airwallex) GetCustomer(
	ctx context.Context,
	in *GetCustomerRequest,
) (out *GetCustomerResponse, err error) {
	out = new(GetCustomerResponse)
	uri := string(awx.apiEndpoint) + string(getCustomerEndpoint)
	if err = Do(ctx, awx.httpClient, http.MethodGet, uri, in, out); err != nil {
		return out, errors.Wrap(err, "Do()")
	}

	return out, err
}

// https://www.airwallex.com/docs/api#/Payment_Acceptance/Customers/_api_v1_pa_customers__id__update/post

type UpdateCustomerRequest struct {
	AdditionalInfo     *AdditionalInfoBasic `json:"additional_info,omitempty"`
	Email              string               `json:"email,omitempty"`
	FirstName          string               `json:"first_name,omitempty"`
	ID                 string               `json:"id,omitempty"`
	LastName           string               `json:"last_name,omitempty"`
	MerchantCustomerID string               `json:"merchant_customer_id,omitempty"`
	Metadata           Metadata             `json:"metadata,omitempty"`
	PhoneNumber        string               `json:"phone_number,omitempty"`
	RequestID          uuid.UUID            `json:"request_id,omitempty"`
}

type UpdateCustomerResponse struct {
	Customer
	RequestID uuid.UUID `json:"request_id,omitempty"`
}

func (awx airwallex) UpdateCustomer(
	ctx context.Context,
	in *UpdateCustomerRequest,
) (out *UpdateCustomerResponse, err error) {
	out = new(UpdateCustomerResponse)
	uri := string(awx.apiEndpoint) + string(updateCustomerEndpoint)
	if err = Do(ctx, awx.httpClient, http.MethodPost, uri, in, out); err != nil {
		return out, errors.Wrap(err, "Do()")
	}

	return out, err
}

// https://www.airwallex.com/docs/api#/Payment_Acceptance/Customers/_api_v1_pa_customers__id__generate_client_secret/get

type GenerateCustomerClientSecretRequest struct {
	ID string `json:"id,omitempty"`
}

type GenerateCustomerClientSecretResponse struct {
	ClientSecret string `json:"client_secret,omitempty"`
	ExpiredTime  string `json:"expired_time,omitempty"`
}

func (awx airwallex) GenerateCustomerClientSecret(
	ctx context.Context,
	in *GenerateCustomerClientSecretRequest,
) (out *GenerateCustomerClientSecretResponse, err error) {
	out = new(GenerateCustomerClientSecretResponse)
	uri := string(awx.apiEndpoint) + string(generateCustomerClientSecretEndpoint)
	if err = Do(ctx, awx.httpClient, http.MethodGet, uri, in, out); err != nil {
		return out, errors.Wrap(err, "Do()")
	}

	return out, err
}

// https://www.airwallex.com/docs/api#/Payment_Acceptance/Customers/_api_v1_pa_customers__id__add_payment_method/post

type AddCustomerPaymentMethodRequest struct {
	ID              string    `json:"id,omitempty"`
	PaymentMethodID string    `json:"payment_method_id,omitempty"`
	RequestID       uuid.UUID `json:"request_id,omitempty"`
}

type AddCustomerPaymentMethodResponse struct {
	Customer
	RequestID uuid.UUID `json:"request_id,omitempty"`
}

func (awx airwallex) AddCustomerPaymentMethod(
	ctx context.Context,
	in *AddCustomerPaymentMethodRequest,
) (out *AddCustomerPaymentMethodResponse, err error) {
	out = new(AddCustomerPaymentMethodResponse)
	uri := string(awx.apiEndpoint) + string(addCustomerPaymentMethodEndpoint)
	if err = Do(ctx, awx.httpClient, http.MethodPost, uri, in, out); err != nil {
		return out, errors.Wrap(err, "Do()")
	}

	return out, err
}

// https://www.airwallex.com/docs/api#/Payment_Acceptance/Customers/_api_v1_pa_customers__id__remove_payment_method/post

type DeleteCustomerPaymentMethodRequest struct {
	ID              string    `json:"id,omitempty"`
	PaymentMethodID string    `json:"payment_method_id,omitempty"`
	RequestID       uuid.UUID `json:"request_id,omitempty"`
}

type DeleteCustomerPaymentMethodResponse struct {
	Customer
	RequestID uuid.UUID `json:"request_id,omitempty"`
}

func (awx airwallex) DeleteCustomerPaymentMethod(
	ctx context.Context,
	in *DeleteCustomerPaymentMethodRequest,
) (out *DeleteCustomerPaymentMethodResponse, err error) {
	out = new(DeleteCustomerPaymentMethodResponse)
	uri := string(awx.apiEndpoint) + string(deleteCustomerPaymentMethodEndpoint)
	if err = Do(ctx, awx.httpClient, http.MethodPost, uri, in, out); err != nil {
		return out, errors.Wrap(err, "Do()")
	}

	return out, err
}

// https://www.airwallex.com/docs/api#/Payment_Acceptance/Customers/_api_v1_pa_customers/get

type ListCustomersRequest struct {
	FromCreatedAt      *TimeISO8601 `json:"from_created_at,omitempty"`
	MerchantCustomerID string       `json:"merchant_customer_id,omitempty"`
	PageNum            int32        `json:"page_num,omitempty"`
	PageSize           int32        `json:"page_size,omitempty"`
	ToCreatedAt        *TimeISO8601 `json:"to_created_at,omitempty"`
}

type ListCustomersResponse struct {
	HasMore string      `json:"has_more,omitempty"`
	Items   []*Customer `json:"items,omitempty"`
}

func (awx airwallex) ListCustomers(
	ctx context.Context,
	in *ListCustomersRequest,
) (out *ListCustomersResponse, err error) {
	out = new(ListCustomersResponse)
	uri := string(awx.apiEndpoint) + string(listCustomersEndpoint)
	if err = Do(ctx, awx.httpClient, http.MethodGet, uri, in, out); err != nil {
		return out, errors.Wrap(err, "Do()")
	}

	return out, err
}
