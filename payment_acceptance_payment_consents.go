package airwallex

import (
	"context"
	"net/http"

	"github.com/google/uuid"
	"github.com/pkg/errors"
)

// https://www.airwallex.com/docs/api#/Payment_Acceptance/Payment_Consents/_api_v1_pa_payment_consents_create/post

type CreatePaymentConsentRequest struct {
	ConnectedAccountID    string                `json:"connected_account_id,omitempty"`
	CustomerID            string                `json:"customer_id,omitempty"`
	MerchantTriggerReason MerchantTriggerReason `json:"merchant_trigger_reason,omitempty"`
	Metadata              Metadata              `json:"metadata,omitempty"`
	NextTriggeredBy       TriggeredBy           `json:"next_triggered_by,omitempty"`
	PaymentMethod         *PaymentMethod        `json:"payment_method,omitempty"`
	RequestID             uuid.UUID             `json:"request_id,omitempty"`
	RequiresCVC           bool                  `json:"requires_cvc,omitempty"`
}

type CreatePaymentConsentResponse struct {
	PaymentConsent
	RequestID uuid.UUID `json:"request_id,omitempty"`
}

func (awx airwallex) CreatePaymentConsent(
	ctx context.Context,
	in *CreatePaymentConsentRequest,
) (out *CreatePaymentConsentResponse, err error) {
	out = new(CreatePaymentConsentResponse)
	uri := string(awx.apiEndpoint) + string(createPaymentConsentEndpoint)
	if err = Do(ctx, awx.httpClient, http.MethodPost, uri, in, out); err != nil {
		return out, errors.Wrap(err, "Do()")
	}

	return out, err
}

// https://www.airwallex.com/docs/api#/Payment_Acceptance/Payment_Consents/_api_v1_pa_payment_consents__id__update/post

type UpdatePaymentConsentRequest struct {
	ID            string         `json:"id,omitempty"`
	Metadata      Metadata       `json:"metadata,omitempty"`
	PaymentMethod *PaymentMethod `json:"payment_method,omitempty"`
	RequestID     uuid.UUID      `json:"request_id,omitempty"`
}

type UpdatePaymentConsentResponse struct {
	PaymentConsent
	RequestID uuid.UUID `json:"request_id,omitempty"`
}

func (awx airwallex) UpdatePaymentConsent(
	ctx context.Context,
	in *UpdatePaymentConsentRequest,
) (out *UpdatePaymentConsentResponse, err error) {
	out = new(UpdatePaymentConsentResponse)
	uri := string(awx.apiEndpoint) + string(updatePaymentConsentEndpoint)
	if err = Do(ctx, awx.httpClient, http.MethodPost, uri, in, out); err != nil {
		return out, errors.Wrap(err, "Do()")
	}

	return out, err
}

// https://www.airwallex.com/docs/api#/Payment_Acceptance/Payment_Consents/_api_v1_pa_payment_consents__id__verify/post

type VerifyPaymentConsentRequest struct {
	ID                  string                             `json:"id,omitempty"`
	DeviceData          *DeviceData                        `json:"device_data,omitempty"`
	RequestID           uuid.UUID                          `json:"request_id,omitempty"`
	ReturnURL           string                             `json:"return_url,omitempty"`
	VerificationOptions *PaymentConsentVerificationOptions `json:"verification_options,omitempty"`
}

type VerifyPaymentConsentResponse struct {
	PaymentConsent
	RequestID uuid.UUID `json:"request_id,omitempty"`
}

func (awx airwallex) VerifyPaymentConsent(
	ctx context.Context,
	in *VerifyPaymentConsentRequest,
) (out *VerifyPaymentConsentResponse, err error) {
	out = new(VerifyPaymentConsentResponse)
	uri := string(awx.apiEndpoint) + string(verifyPaymentConsentEndpoint)
	if err = Do(ctx, awx.httpClient, http.MethodPost, uri, in, out); err != nil {
		return out, errors.Wrap(err, "Do()")
	}

	return out, err
}

// https://www.airwallex.com/docs/api#/Payment_Acceptance/Payment_Consents/_api_v1_pa_payment_consents__id__disable/post

type DisablePaymentConsentRequest struct {
	ID        string    `json:"id,omitempty"`
	RequestID uuid.UUID `json:"request_id,omitempty"`
}

type DisablePaymentConsentResponse struct {
	PaymentConsent
	RequestID uuid.UUID `json:"request_id,omitempty"`
}

func (awx airwallex) DisablePaymentConsent(
	ctx context.Context,
	in *DisablePaymentConsentRequest,
) (out *DisablePaymentConsentResponse, err error) {
	out = new(DisablePaymentConsentResponse)
	uri := string(awx.apiEndpoint) + string(disablePaymentConsentEndpoint)
	if err = Do(ctx, awx.httpClient, http.MethodPost, uri, in, out); err != nil {
		return out, errors.Wrap(err, "Do()")
	}

	return out, err
}

// https://www.airwallex.com/docs/api#/Payment_Acceptance/Payment_Consents/_api_v1_pa_payment_consents__id_/get

type GetPaymentConsentRequest struct {
	ID string `json:"id,omitempty"`
}

type GetPaymentConsentResponse struct {
	PaymentConsent
	RequestID uuid.UUID `json:"request_id,omitempty"`
}

func (awx airwallex) GetPaymentConsent(
	ctx context.Context,
	in *GetPaymentConsentRequest,
) (out *GetPaymentConsentResponse, err error) {
	out = new(GetPaymentConsentResponse)
	uri := string(awx.apiEndpoint) + string(getPaymentConsentEndpoint)
	if err = Do(ctx, awx.httpClient, http.MethodGet, uri, in, out); err != nil {
		return out, errors.Wrap(err, "Do()")
	}

	return out, err
}

// https://www.airwallex.com/docs/api#/Payment_Acceptance/Payment_Consents/_api_v1_pa_payment_consents/get

type ListPaymentConsentsRequest struct {
	CustomerID            string                `json:"customer_id,omitempty"`
	FromCreatedAt         *TimeISO8601          `json:"from_created_at,omitempty"`
	MerchantTriggerReason MerchantTriggerReason `json:"merchant_trigger_reason,omitempty"`
	NextTriggeredBy       TriggeredBy           `json:"next_triggered_by,omitempty"`
	PageNum               int32                 `json:"page_num,omitempty"`
	PageSize              int32                 `json:"page_size,omitempty"`
	Status                PaymentMethodStatus   `json:"status,omitempty"`
	ToCreatedAt           *TimeISO8601          `json:"to_created_at,omitempty"`
}

type ListPaymentConsentsResponse struct {
	HasMore bool              `json:"has_more,omitempty"`
	Items   []*PaymentConsent `json:"items,omitempty"`
}

func (awx airwallex) ListPaymentConsents(
	ctx context.Context,
	in *ListPaymentConsentsRequest,
) (out *ListPaymentConsentsResponse, err error) {
	out = new(ListPaymentConsentsResponse)
	uri := string(awx.apiEndpoint) + string(listPaymentConsentsEndpoint)
	if err = Do(ctx, awx.httpClient, http.MethodGet, uri, in, out); err != nil {
		return out, errors.Wrap(err, "Do()")
	}

	return out, err
}
