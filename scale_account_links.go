package airwallex

import (
	"context"
	"net/http"

	"github.com/pkg/errors"
)

// https://www.airwallex.com/docs/api#/Scale/Account_Links/_api_v1_accounts_invitation_links_create/post

type CreateAccountInvitationLinkRequest struct {
	AccountID         string                       `json:"account_id,omitempty"`
	Mode              AccountInvitationLinkMode    `json:"mode,omitempty"`
	OAuth2            *AccountInvitationLinkOAuth2 `json:"oauth2,omitempty"`
	Metadata          Metadata                     `json:"metadata,omitempty"`
	Identifier        string                       `json:"identifier,omitempty"`
	PrefilledFormData []*PrefilledFormData         `json:"prefilled_formdata,omitempty"`
}

type CreateAccountInvitationLinkResponse AccountInvitationLink

func (awx airwallex) CreateAccountInvitationLink(
	ctx context.Context,
	in *CreateAccountInvitationLinkRequest,
) (out *CreateAccountInvitationLinkResponse, err error) {
	out = new(CreateAccountInvitationLinkResponse)
	uri := string(awx.apiEndpoint) + string(createAccountInvitationLinkEndpoint)
	if err = Do(ctx, awx.httpClient, http.MethodPost, uri, in, out); err != nil {
		return out, errors.Wrap(err, "Do()")
	}

	return out, err
}

// https://www.airwallex.com/docs/api#/Scale/Account_Links/_api_v1_accounts_invitation_links__id_/get

type GetAccountInvitationLinkRequest struct {
	ID string `json:"id,omitempty"`
}

type GetAccountInvitationLinkResponse AccountInvitationLink

func (awx airwallex) GetAccountInvitationLink(
	ctx context.Context,
	in *GetAccountInvitationLinkRequest,
) (out *GetAccountInvitationLinkResponse, err error) {
	out = new(GetAccountInvitationLinkResponse)
	uri := string(awx.apiEndpoint) + string(getAccountInvitationLinkEndpoint)
	if err = Do(ctx, awx.httpClient, http.MethodGet, uri, in, out); err != nil {
		return out, errors.Wrap(err, "Do()")
	}

	return out, err
}
