package airwallex

import (
	"context"
	"net/http"

	"github.com/pkg/errors"
)

// https://www.airwallex.com/docs/api#/Authentication/API_Access/_api_v1_authentication_login/post

type LoginResponse struct {
	ExpireAt *TimeISO8601 `json:"expire_at,omitempty"`
	Token    string       `json:"token,omitempty"`
}

func (awx airwallex) Login(
	ctx context.Context,
) (out *LoginResponse, err error) {
	out = new(LoginResponse)

	uri := string(awx.apiEndpoint) + string(loginEndpoint)
	if err = Do(ctx, awx.httpClient, http.MethodPost, uri, nil, out); err != nil {
		return out, errors.Wrap(err, "Do()")
	}

	return out, err
}
