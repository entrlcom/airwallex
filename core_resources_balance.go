package airwallex

import (
	"context"
	"net/http"

	"github.com/pkg/errors"
)

// https://www.airwallex.com/docs/api#/Core_Resources/Balances/_api_v1_balances_current/get

type GetCurrentBalancesResponse []*CurrentBalance

func (awx airwallex) GetCurrentBalances(
	ctx context.Context,
) (out *GetCurrentBalancesResponse, err error) {
	out = new(GetCurrentBalancesResponse)
	uri := string(awx.apiEndpoint) + string(getCurrentBalancesEndpoint)
	if err = Do(ctx, awx.httpClient, http.MethodGet, uri, nil, out); err != nil {
		return out, errors.Wrap(err, "Do()")
	}

	return out, err
}

// https://www.airwallex.com/docs/api#/Core_Resources/Balances/_api_v1_balances_history/get

type GetBalanceHistoryRequest struct {
	Currency   Currency     `json:"currency,omitempty"`
	FromPostAt *TimeISO8601 `json:"from_post_at,omitempty"`
	PageNum    int32        `json:"page_num,omitempty"`
	PageSize   int32        `json:"page_size,omitempty"`
	RequestID  string       `json:"request_id,omitempty"`
	ToPostAt   *TimeISO8601 `json:"to_post_at,omitempty"`
}

type GetBalanceHistoryResponse struct {
	HasMore bool       `json:"has_more,omitempty"`
	Items   []*Balance `json:"items,omitempty"`
}

func (awx airwallex) GetBalanceHistory(
	ctx context.Context,
	in *GetBalanceHistoryRequest,
) (out *GetBalanceHistoryResponse, err error) {
	out = new(GetBalanceHistoryResponse)
	uri := string(awx.apiEndpoint) + string(getBalanceHistoryEndpoint)
	if err = Do(ctx, awx.httpClient, http.MethodGet, uri, in, out); err != nil {
		return out, errors.Wrap(err, "Do()")
	}

	return out, err
}
