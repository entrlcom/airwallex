package airwallex

import (
	"context"
	"net/http"

	"github.com/google/uuid"
	"github.com/pkg/errors"
)

// https://www.airwallex.com/docs/api#/Payouts/Payments/_api_v1_payments/get

type ListPaymentsRequest struct {
	FromCreatedAt    *TimeISO8601  `json:"from_created_at,omitempty"`
	PageNum          int32         `json:"page_num,omitempty"`
	PageSize         int32         `json:"page_size,omitempty"`
	PaymentCurrency  Currency      `json:"payment_currency,omitempty"`
	RequestID        uuid.UUID     `json:"request_id,omitempty"`
	ShortReferenceID string        `json:"short_reference_id,omitempty"`
	Status           PaymentStatus `json:"status,omitempty"`
	ToCreatedAt      *TimeISO8601  `json:"to_created_at,omitempty"`
}

type ListPaymentsResponse struct {
	HasMore bool       `json:"has_more,omitempty"`
	Items   []*Payment `json:"items,omitempty"`
}

func (awx airwallex) ListPayments(
	ctx context.Context,
	in *ListPaymentsRequest,
) (out *ListPaymentsResponse, err error) {
	out = new(ListPaymentsResponse)
	uri := string(awx.apiEndpoint) + string(listPaymentsEndpoint)
	if err = Do(ctx, awx.httpClient, http.MethodGet, uri, in, out); err != nil {
		return out, errors.Wrap(err, "Do()")
	}

	return out, err
}

// https://www.airwallex.com/docs/api#/Payouts/Payments/_api_v1_payments__payment_id_/get

type GetPaymentRequest struct {
	FromCreatedAt    *TimeISO8601  `json:"from_created_at,omitempty"`
	PageNum          int32         `json:"page_num,omitempty"`
	PageSize         int32         `json:"page_size,omitempty"`
	PaymentCurrency  Currency      `json:"payment_currency,omitempty"`
	RequestID        string        `json:"request_id,omitempty"`
	ShortReferenceID string        `json:"short_reference_id,omitempty"`
	Status           PaymentStatus `json:"status,omitempty"`
	ToCreatedAt      *TimeISO8601  `json:"to_created_at,omitempty"`
}

type GetPaymentResponse Payment

func (awx airwallex) GetPayment(
	ctx context.Context,
	in *GetPaymentRequest,
) (out *GetPaymentResponse, err error) {
	out = new(GetPaymentResponse)
	uri := string(awx.apiEndpoint) + string(getPaymentEndpoint)
	if err = Do(ctx, awx.httpClient, http.MethodGet, uri, in, out); err != nil {
		return out, errors.Wrap(err, "Do()")
	}

	return out, err
}

// https://www.airwallex.com/docs/api#/Payouts/Payments/_api_v1_payments_cancel__payment_id_/post

type CancelPaymentRequest struct {
	PaymentID string `json:"payment_id,omitempty"`
}

type CancelPaymentResponse interface{}

func (awx airwallex) CancelPayment(
	ctx context.Context,
	in *CancelPaymentRequest,
) (out *CancelPaymentResponse, err error) {
	out = new(CancelPaymentResponse)
	uri := string(awx.apiEndpoint) + string(cancelPaymentEndpoint)
	if err = Do(ctx, awx.httpClient, http.MethodPost, uri, in, out); err != nil {
		return out, errors.Wrap(err, "Do()")
	}

	return out, err
}

// https://www.airwallex.com/docs/api#/Payouts/Payments/_api_v1_payments_create/post

type CreatePaymentRequest struct {
	Beneficiary       *BeneficiaryDetails        `json:"beneficiary,omitempty"`
	BeneficiaryID     string                     `json:"beneficiary_id,omitempty"`
	ClientData        string                     `json:"client_data,omitempty"`
	FeePaidBy         FeePaidBy                  `json:"fee_paid_by,omitempty"`
	Payer             *PayerDetails              `json:"payer,omitempty"`
	PayerID           string                     `json:"payer_id,omitempty"`
	PaymentAmount     Decimal                    `json:"payment_amount,omitempty"`
	PaymentCurrency   Currency                   `json:"payment_currency,omitempty"`
	PaymentDate       string                     `json:"payment_date,omitempty"` // TODO: Time (YYYY-MM-DD)
	PaymentMethod     GlobalAccountPaymentMethod `json:"payment_method,omitempty"`
	QuoteID           string                     `json:"quote_id,omitempty"`
	Reason            string                     `json:"reason,omitempty"`
	Reference         string                     `json:"reference,omitempty"`
	RequestID         uuid.UUID                  `json:"request_id,omitempty"`
	SourceAmount      Decimal                    `json:"source_amount,omitempty"`
	SourceCurrency    Currency                   `json:"source_currency,omitempty"`
	SWIFTChargeOption SWIFTChargeOption          `json:"swift_charge_option,omitempty"`
}

type CreatePaymentResponse Payment

func (awx airwallex) CreatePayment(
	ctx context.Context,
	in *CreatePaymentRequest,
) (out *CreatePaymentResponse, err error) {
	out = new(CreatePaymentResponse)
	uri := string(awx.apiEndpoint) + string(createPaymentEndpoint)
	if err = Do(ctx, awx.httpClient, http.MethodPost, uri, in, out); err != nil {
		return out, errors.Wrap(err, "Do()")
	}

	return out, err
}

// https://www.airwallex.com/docs/api#/Payouts/Payments/_api_v1_payments_retry__payment_id_/post

type RetryPaymentRequest struct {
	AccountCurrency      Currency       `json:"account_currency,omitempty"`
	AccountName          string         `json:"account_name,omitempty"`
	AccountNumber        string         `json:"account_number,omitempty"`
	AccountRoutingType1  string         `json:"account_routing_type1,omitempty"`
	AccountRoutingType2  string         `json:"account_routing_type2,omitempty"`
	AccountRoutingValue1 string         `json:"account_routing_value1,omitempty"`
	AccountRoutingValue2 string         `json:"account_routing_value2,omitempty"`
	BankBranch           string         `json:"bank_branch,omitempty"`
	BankCountryCode      string         `json:"bank_country_code,omitempty"`
	BankName             string         `json:"bank_name,omitempty"`
	BankStreetAddress    string         `json:"bank_street_address,omitempty"`
	BindingMobileNumber  string         `json:"binding_mobile_number,omitempty"`
	Fingerprint          string         `json:"fingerprint,omitempty"`
	IBAN                 string         `json:"iban,omitempty"`
	LocalClearingSystem  ClearingSystem `json:"local_clearing_system,omitempty"`
	PaymentID            uuid.UUID      `json:"payment_id,omitempty"`
	SwiftCode            string         `json:"swift_code,omitempty"`
	TransactionReference string         `json:"transaction_reference,omitempty"`
}

type RetryPaymentResponse Payment

func (awx airwallex) RetryPayment(
	ctx context.Context,
	in *RetryPaymentRequest,
) (out *RetryPaymentResponse, err error) {
	out = new(RetryPaymentResponse)
	uri := string(awx.apiEndpoint) + string(retryPaymentEndpoint)
	if err = Do(ctx, awx.httpClient, http.MethodPost, uri, in, out); err != nil {
		return out, errors.Wrap(err, "Do()")
	}

	return out, err
}

// https://www.airwallex.com/docs/api#/Payouts/Payments/_api_v1_payments_update__payment_id_/post

type UpdatePaymentRequest Payment

type UpdatePaymentResponse Payment

func (awx airwallex) UpdatePayment(
	ctx context.Context,
	in *UpdatePaymentRequest,
) (out *UpdatePaymentResponse, err error) {
	out = new(UpdatePaymentResponse)
	uri := string(awx.apiEndpoint) + string(updatePaymentEndpoint)
	if err = Do(ctx, awx.httpClient, http.MethodPost, uri, in, out); err != nil {
		return out, errors.Wrap(err, "Do()")
	}

	return out, err
}

// https://www.airwallex.com/docs/api#/Payouts/Payments/_api_v1_payments_validate/post

type ValidatePaymentRequest struct {
	Beneficiary       *BeneficiaryDetails        `json:"beneficiary,omitempty"`
	BeneficiaryID     string                     `json:"beneficiary_id,omitempty"`
	ClientData        string                     `json:"client_data,omitempty"`
	FeePaidBy         FeePaidBy                  `json:"fee_paid_by,omitempty"`
	Payer             *PayerDetails              `json:"payer,omitempty"`
	PayerID           string                     `json:"payer_id,omitempty"`
	PaymentAmount     Decimal                    `json:"payment_amount,omitempty"`
	PaymentCurrency   Currency                   `json:"payment_currency,omitempty"`
	PaymentDate       string                     `json:"payment_date,omitempty"` // TODO: Time (YYYY-MM-DD)
	PaymentMethod     GlobalAccountPaymentMethod `json:"payment_method,omitempty"`
	QuoteID           string                     `json:"quote_id,omitempty"`
	Reason            string                     `json:"reason,omitempty"`
	Reference         string                     `json:"reference,omitempty"`
	RequestID         uuid.UUID                  `json:"request_id,omitempty"`
	SourceAmount      Decimal                    `json:"source_amount,omitempty"`
	SourceCurrency    Currency                   `json:"source_currency,omitempty"`
	SWIFTChargeOption SWIFTChargeOption          `json:"swift_charge_option,omitempty"`
}

type ValidatePaymentResponse interface{}

func (awx airwallex) ValidatePayment(
	ctx context.Context,
	in *ValidatePaymentRequest,
) (out *ValidatePaymentResponse, err error) {
	out = new(ValidatePaymentResponse)
	uri := string(awx.apiEndpoint) + string(validatePaymentEndpoint)
	if err = Do(ctx, awx.httpClient, http.MethodPost, uri, in, out); err != nil {
		return out, errors.Wrap(err, "Do()")
	}

	return out, err
}
