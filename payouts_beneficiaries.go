package airwallex

import (
	"context"
	"net/http"

	"github.com/pkg/errors"
)

// https://www.airwallex.com/docs/api#/Payouts/Beneficiaries/_api_v1_beneficiaries/get

type ListBeneficiariesRequest struct {
	BankAccountNumber string       `json:"bank_account_number,omitempty"`
	CompanyName       string       `json:"company_name,omitempty"`
	EntityType        *EntityType  `json:"entity_type,omitempty"`
	FromDate          *TimeISO8601 `json:"from_date,omitempty"`
	Name              string       `json:"name,omitempty"`
	NickName          string       `json:"nick_name,omitempty"`
	PageNum           int32        `json:"page_num,omitempty"`
	PageSize          int32        `json:"page_size,omitempty"`
	ToDate            *TimeISO8601 `json:"to_date,omitempty"`
}

type ListBeneficiariesResponse struct {
	HasMore bool           `json:"has_more,omitempty"`
	Items   []*Beneficiary `json:"items,omitempty"`
}

func (awx airwallex) ListBeneficiaries(
	ctx context.Context,
	in *ListBeneficiariesRequest,
) (out *ListBeneficiariesResponse, err error) {
	out = new(ListBeneficiariesResponse)
	uri := string(awx.apiEndpoint) + string(listBeneficiariesEndpoint)
	if err = Do(ctx, awx.httpClient, http.MethodGet, uri, in, out); err != nil {
		return out, errors.Wrap(err, "Do()")
	}

	return out, err
}

// https://www.airwallex.com/docs/api#/Payouts/Beneficiaries/_api_v1_beneficiaries__beneficiary_id_/get

type GetBeneficiaryRequest struct {
	BeneficiaryID string `json:"beneficiary_id,omitempty"`
}

type GetBeneficiaryResponse Beneficiary

func (awx airwallex) GetBeneficiary(
	ctx context.Context,
	in *GetBeneficiaryRequest,
) (out *GetBeneficiaryResponse, err error) {
	out = new(GetBeneficiaryResponse)
	uri := string(awx.apiEndpoint) + string(getBeneficiaryEndpoint)
	if err = Do(ctx, awx.httpClient, http.MethodGet, uri, in, out); err != nil {
		return out, errors.Wrap(err, "Do()")
	}

	return out, err
}

// https://www.airwallex.com/docs/api#/Payouts/Beneficiaries/_api_v1_beneficiaries_create/post

type CreateBeneficiaryRequest struct {
	Beneficiary     *Beneficiary                 `json:"beneficiary,omitempty"`
	Nickname        string                       `json:"nickname,omitempty"`
	PayerEntityType EntityType                   `json:"payer_entity_type,omitempty"`
	PaymentMethods  []GlobalAccountPaymentMethod `json:"payment_methods,omitempty"`
	PaymentReason   string                       `json:"payment_reason,omitempty"`
}

type CreateBeneficiaryResponse Beneficiary

func (awx airwallex) CreateBeneficiary(
	ctx context.Context,
	in *CreateBeneficiaryRequest,
) (out *CreateBeneficiaryResponse, err error) {
	out = new(CreateBeneficiaryResponse)
	uri := string(awx.apiEndpoint) + string(createBeneficiaryEndpoint)
	if err = Do(ctx, awx.httpClient, http.MethodPost, uri, in, out); err != nil {
		return out, errors.Wrap(err, "Do()")
	}

	return out, err
}

// https://www.airwallex.com/docs/api#/Payouts/Beneficiaries/_api_v1_beneficiaries_delete__beneficiary_id_/post

type DeleteBeneficiaryRequest struct {
	BeneficiaryID string `json:"beneficiary_id,omitempty"`
}

type DeleteBeneficiaryResponse bool

func (awx airwallex) DeleteBeneficiary(
	ctx context.Context,
	in *DeleteBeneficiaryRequest,
) (out *DeleteBeneficiaryResponse, err error) {
	out = new(DeleteBeneficiaryResponse)
	uri := string(awx.apiEndpoint) + string(deleteBeneficiaryEndpoint)
	if err = Do(ctx, awx.httpClient, http.MethodPost, uri, in, out); err != nil {
		return out, errors.Wrap(err, "Do()")
	}

	return out, err
}

// https://www.airwallex.com/docs/api#/Payouts/Beneficiaries/_api_v1_beneficiaries_update__beneficiary_id_/post

type UpdateBeneficiaryRequest struct {
	BeneficiaryID   string                       `json:"beneficiary_id,omitempty"`
	Beneficiary     *Beneficiary                 `json:"beneficiary,omitempty"`
	Nickname        string                       `json:"nickname,omitempty"`
	PayerEntityType EntityType                   `json:"payer_entity_type,omitempty"`
	PaymentMethods  []GlobalAccountPaymentMethod `json:"payment_methods,omitempty"`
	PaymentReason   string                       `json:"payment_reason,omitempty"`
}

type UpdateBeneficiaryResponse Beneficiary

func (awx airwallex) UpdateBeneficiary(
	ctx context.Context,
	in *UpdateBeneficiaryRequest,
) (out *UpdateBeneficiaryResponse, err error) {
	out = new(UpdateBeneficiaryResponse)
	uri := string(awx.apiEndpoint) + string(updateBeneficiaryEndpoint)
	if err = Do(ctx, awx.httpClient, http.MethodPost, uri, in, out); err != nil {
		return out, errors.Wrap(err, "Do()")
	}

	return out, err
}

// https://www.airwallex.com/docs/api#/Payouts/Beneficiaries/_api_v1_beneficiaries_validate/post

type ValidateBeneficiaryRequest struct {
	Beneficiary     *Beneficiary                 `json:"beneficiary,omitempty"`
	Nickname        string                       `json:"nickname,omitempty"`
	PayerEntityType EntityType                   `json:"payer_entity_type,omitempty"`
	PaymentMethods  []GlobalAccountPaymentMethod `json:"payment_methods,omitempty"`
	PaymentReason   string                       `json:"payment_reason,omitempty"`
}

type ValidateBeneficiaryResponse interface{}

func (awx airwallex) ValidateBeneficiary(
	ctx context.Context,
	in *ValidateBeneficiaryRequest,
) (out *ValidateBeneficiaryResponse, err error) {
	out = new(ValidateBeneficiaryResponse)
	uri := string(awx.apiEndpoint) + string(validateBeneficiaryEndpoint)
	if err = Do(ctx, awx.httpClient, http.MethodPost, uri, in, out); err != nil {
		return out, errors.Wrap(err, "Do()")
	}

	return out, err
}

// https://www.airwallex.com/docs/api#/Payouts/Beneficiaries/_api_v1_beneficiary_api_schemas_generate/post

type GetAPISchemaRequest struct {
	AccountCurrency     Currency                     `json:"account_currency,omitempty"`
	BankCountryCode     string                       `json:"bank_country_code,omitempty"`
	EntityType          EntityType                   `json:"entity_type,omitempty"`
	LocalClearingSystem ClearingSystem               `json:"local_clearing_system,omitempty"`
	PaymentMethod       []GlobalAccountPaymentMethod `json:"payment_method,omitempty"`
}

type GetAPISchemaResponse APISchema

func (awx airwallex) GetAPISchema(
	ctx context.Context,
	in *GetAPISchemaRequest,
) (out *GetAPISchemaResponse, err error) {
	out = new(GetAPISchemaResponse)
	uri := string(awx.apiEndpoint) + string(getAPISchemaEndpoint)
	if err = Do(ctx, awx.httpClient, http.MethodPost, uri, in, out); err != nil {
		return out, errors.Wrap(err, "Do()")
	}

	return out, err
}

// https://www.airwallex.com/docs/api#/Payouts/Beneficiaries/_api_v1_beneficiary_form_schemas_generate/post

type GetFormSchemaRequest struct {
	AccountCurrency     Currency                     `json:"account_currency,omitempty"`
	BankCountryCode     string                       `json:"bank_country_code,omitempty"`
	EntityType          *EntityType                  `json:"entity_type,omitempty"`
	LocalClearingSystem ClearingSystem               `json:"local_clearing_system,omitempty"`
	PaymentMethod       []GlobalAccountPaymentMethod `json:"payment_method,omitempty"`
}

type GetFormSchemaResponse FormSchema

func (awx airwallex) GetFormSchema(
	ctx context.Context,
	in *GetFormSchemaRequest,
) (out *GetFormSchemaResponse, err error) {
	out = new(GetFormSchemaResponse)
	uri := string(awx.apiEndpoint) + string(getFormSchemaEndpoint)
	if err = Do(ctx, awx.httpClient, http.MethodPost, uri, in, out); err != nil {
		return out, errors.Wrap(err, "Do()")
	}

	return out, err
}
