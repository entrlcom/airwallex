package airwallex

import (
	"context"
	"net/http"

	"github.com/google/uuid"
	"github.com/pkg/errors"
)

// https://www.airwallex.com/docs/api#/Payment_Acceptance/Funds_Splits/_api_v1_pa_funds_splits_create/post

type CreateFundsSplitRequest struct {
	Amount      Decimal    `json:"amount,omitempty"`
	AutoRelease bool       `json:"auto_release,omitempty"`
	Destination string     `json:"destination,omitempty"`
	Metadata    Metadata   `json:"metadata,omitempty"`
	RequestID   uuid.UUID  `json:"request_id,omitempty"`
	SourceID    string     `json:"source_id,omitempty"`
	SourceType  SourceType `json:"source_type,omitempty"`
}

type CreateFundsSplitResponse struct {
	FundsSplit
	RequestID uuid.UUID `json:"request_id,omitempty"`
}

func (awx airwallex) CreateFundsSplit(
	ctx context.Context,
	in *CreateFundsSplitRequest,
) (out *CreateFundsSplitResponse, err error) {
	out = new(CreateFundsSplitResponse)
	uri := string(awx.apiEndpoint) + string(createFundsSplitEndpoint)
	if err = Do(ctx, awx.httpClient, http.MethodPost, uri, in, out); err != nil {
		return out, errors.Wrap(err, "Do()")
	}

	return out, err
}

// https://www.airwallex.com/docs/api#/Payment_Acceptance/Funds_Splits/_api_v1_pa_funds_splits__id_/get

type GetFundsSplitRequest struct {
	ID string `json:"id,omitempty"`
}

type GetFundsSplitResponse struct {
	FundsSplit
	RequestID uuid.UUID `json:"request_id,omitempty"`
}

func (awx airwallex) GetFundsSplit(
	ctx context.Context,
	in *GetFundsSplitRequest,
) (out *GetFundsSplitResponse, err error) {
	out = new(GetFundsSplitResponse)
	uri := string(awx.apiEndpoint) + string(getFundsSplitEndpoint)
	if err = Do(ctx, awx.httpClient, http.MethodGet, uri, in, out); err != nil {
		return out, errors.Wrap(err, "Do()")
	}

	return out, err
}

// https://www.airwallex.com/docs/api#/Payment_Acceptance/Funds_Splits/_api_v1_pa_funds_splits/get

type ListFundsSplitsRequest struct {
	PageNum    int32      `json:"page_num,omitempty"`
	PageSize   int32      `json:"page_size,omitempty"`
	SourceID   string     `json:"source_id,omitempty"`
	SourceType SourceType `json:"source_type,omitempty"`
}

type ListFundsSplitsResponse struct {
	HasMore bool          `json:"has_more,omitempty"`
	Items   []*FundsSplit `json:"items,omitempty"`
}

func (awx airwallex) ListFundsSplits(
	ctx context.Context,
	in *ListFundsSplitsRequest,
) (out *ListFundsSplitsResponse, err error) {
	out = new(ListFundsSplitsResponse)
	uri := string(awx.apiEndpoint) + string(listFundsSplitsEndpoint)
	if err = Do(ctx, awx.httpClient, http.MethodGet, uri, in, out); err != nil {
		return out, errors.Wrap(err, "Do()")
	}

	return out, err
}

// https://www.airwallex.com/docs/api#/Payment_Acceptance/Funds_Splits/_api_v1_pa_funds_splits__id__release/post

type ReleaseFundsSplitRequest struct {
	ID        string    `json:"id,omitempty"`
	RequestID uuid.UUID `json:"request_id,omitempty"`
}

type ReleaseFundsSplitResponse struct {
	FundsSplit
	RequestID uuid.UUID `json:"request_id,omitempty"`
}

func (awx airwallex) ReleaseFundsSplit(
	ctx context.Context,
	in *ReleaseFundsSplitRequest,
) (out *ReleaseFundsSplitResponse, err error) {
	out = new(ReleaseFundsSplitResponse)
	uri := string(awx.apiEndpoint) + string(releaseFundsSplitEndpoint)
	if err = Do(ctx, awx.httpClient, http.MethodPost, uri, in, out); err != nil {
		return out, errors.Wrap(err, "Do()")
	}

	return out, err
}
