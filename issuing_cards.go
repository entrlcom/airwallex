package airwallex

import (
	"context"
	"net/http"

	"github.com/google/uuid"
	"github.com/pkg/errors"
)

// https://www.airwallex.com/docs/api#/Issuing/Cards/_api_v1_issuing_cards_create/post

type CreateCardRequest struct {
	ActivateOnIssue       bool                       `json:"activate_on_issue,omitempty"`
	AuthorizationControls *CardAuthorizationControls `json:"authorization_controls,omitempty"`
	CardholderID          string                     `json:"cardholder_id,omitempty"`
	ClientData            string                     `json:"client_data,omitempty"`
	CreatedBy             string                     `json:"created_by,omitempty"`
	FormFactor            CardFormFactor             `json:"form_factor,omitempty"`
	IssueTo               string                     `json:"issue_to,omitempty"`
	NickName              string                     `json:"nick_name,omitempty"`
	Note                  string                     `json:"note,omitempty"`
	PostalAddress         *CardholderAddress         `json:"postal_address,omitempty"`
	PrimaryContactDetails *CardPrimaryContactDetails `json:"primary_contact_details,omitempty"`
	RequestID             uuid.UUID                  `json:"request_id,omitempty"`
	Type                  CardType1                  `json:"type,omitempty"`
}

type CreateCardResponse struct {
	Card
	RequestID uuid.UUID `json:"request_id,omitempty"`
}

func (awx airwallex) CreateCard(
	ctx context.Context,
	in *CreateCardRequest,
) (out *CreateCardResponse, err error) {
	out = new(CreateCardResponse)
	uri := string(awx.apiEndpoint) + string(createCardEndpoint)
	if err = Do(ctx, awx.httpClient, http.MethodPost, uri, in, out); err != nil {
		return out, errors.Wrap(err, "Do()")
	}

	return out, err
}

// https://www.airwallex.com/docs/api#/Issuing/Cards/_api_v1_issuing_cards__id__details/get

type GetSensitiveCardDetailsRequest struct {
	ID string `json:"id,omitempty"`
}

type GetSensitiveCardDetailsResponse struct {
	CardNumber  string `json:"card_number,omitempty"`
	CVV         string `json:"cvv,omitempty"`
	ExpiryMonth int32  `json:"expiry_month,omitempty"`
	ExpiryYear  int32  `json:"expiry_year,omitempty"`
	NameOnCard  string `json:"name_on_card,omitempty"`
}

func (awx airwallex) GetSensitiveCardDetails(
	ctx context.Context,
	in *GetSensitiveCardDetailsRequest,
) (out *GetSensitiveCardDetailsResponse, err error) {
	out = new(GetSensitiveCardDetailsResponse)
	uri := string(awx.apiEndpoint) + string(getSensitiveCardDetailsEndpoint)
	if err = Do(ctx, awx.httpClient, http.MethodGet, uri, in, out); err != nil {
		return out, errors.Wrap(err, "Do()")
	}

	return out, err
}

// https://www.airwallex.com/docs/api#/Issuing/Cards/_api_v1_issuing_cards/get

type ListCardsRequest struct {
	CardStatus    CardStatus   `json:"card_status,omitempty"`
	CardholderID  string       `json:"cardholder_id,omitempty"`
	FromCreatedAt *TimeISO8601 `json:"from_created_at,omitempty"`
	NickName      string       `json:"nick_name,omitempty"`
	PageNum       int32        `json:"page_num,omitempty"`
	PageSize      int32        `json:"page_size,omitempty"`
	ToCreatedAt   *TimeISO8601 `json:"to_created_at,omitempty"`
}

type ListCardsResponse struct {
	HasMore bool    `json:"has_more,omitempty"`
	Items   []*Card `json:"items,omitempty"`
}

func (awx airwallex) ListCards(
	ctx context.Context,
	in *ListCardsRequest,
) (out *ListCardsResponse, err error) {
	out = new(ListCardsResponse)
	uri := string(awx.apiEndpoint) + string(listCardsEndpoint)
	if err = Do(ctx, awx.httpClient, http.MethodGet, uri, in, out); err != nil {
		return out, errors.Wrap(err, "Do()")
	}

	return out, err
}

// https://www.airwallex.com/docs/api#/Issuing/Cards/_api_v1_issuing_cards__id__activate/post

type ActivateCardRequest struct {
	ID string `json:"id,omitempty"`
}

func (awx airwallex) ActivateCard(
	ctx context.Context,
	in *ActivateCardRequest,
) (err error) {
	uri := string(awx.apiEndpoint) + string(activateCardEndpoint)
	if err = Do(ctx, awx.httpClient, http.MethodPost, uri, in, nil); err != nil {
		return errors.Wrap(err, "Do()")
	}

	return err
}

// https://www.airwallex.com/docs/api#/Issuing/Cards/_api_v1_issuing_cards__id_/get

type GetCardRequest struct {
	ID string `json:"id,omitempty"`
}

type GetCardResponse struct {
	Card
	RequestID uuid.UUID `json:"request_id,omitempty"`
}

func (awx airwallex) GetCard(
	ctx context.Context,
	in *GetCardRequest,
) (out *GetCardResponse, err error) {
	out = new(GetCardResponse)
	uri := string(awx.apiEndpoint) + string(getCardEndpoint)
	if err = Do(ctx, awx.httpClient, http.MethodGet, uri, in, out); err != nil {
		return out, errors.Wrap(err, "Do()")
	}

	return out, err
}

// https://www.airwallex.com/docs/api#/Issuing/Cards/_api_v1_issuing_cards__id__limits/get

type GetCardLimitsRequest struct {
	ID string `json:"id,omitempty"`
}

type GetCardLimitsResponse struct {
	Currency Currency     `json:"currency,omitempty"`
	Limits   []*CardLimit `json:"limits,omitempty"`
}

func (awx airwallex) GetCardLimits(
	ctx context.Context,
	in *GetCardLimitsRequest,
) (out *GetCardLimitsResponse, err error) {
	out = new(GetCardLimitsResponse)
	uri := string(awx.apiEndpoint) + string(getCardLimitsEndpoint)
	if err = Do(ctx, awx.httpClient, http.MethodGet, uri, in, out); err != nil {
		return out, errors.Wrap(err, "Do()")
	}

	return out, err
}

// https://www.airwallex.com/docs/api#/Issuing/Cards/_api_v1_issuing_cards__id__update/post

type UpdateCardRequest struct {
	AuthorizationControls *CardAuthorizationControls `json:"authorization_controls,omitempty"`
	CardStatus            CardStatus                 `json:"card_status,omitempty"`
	ID                    string                     `json:"id,omitempty"`
	NickName              string                     `json:"nick_name,omitempty"`
	PrimaryContactDetails *CardPrimaryContactDetails `json:"primary_contact_details,omitempty"`
}

type UpdateCardResponse struct {
	Card
	RequestID uuid.UUID `json:"request_id,omitempty"`
}

func (awx airwallex) UpdateCard(
	ctx context.Context,
	in *UpdateCardRequest,
) (out *UpdateCardResponse, err error) {
	out = new(UpdateCardResponse)
	uri := string(awx.apiEndpoint) + string(updateCardEndpoint)
	if err = Do(ctx, awx.httpClient, http.MethodPost, uri, in, out); err != nil {
		return out, errors.Wrap(err, "Do()")
	}

	return out, err
}
