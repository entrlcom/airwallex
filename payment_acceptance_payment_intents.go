package airwallex

import (
	"context"
	"net/http"

	"github.com/google/uuid"
	"github.com/pkg/errors"
)

// https://www.airwallex.com/docs/api#/Payment_Acceptance/Payment_Intents/_api_v1_pa_payment_intents_create/post

type CreatePaymentIntentRequest struct {
	Amount               Decimal               `json:"amount,omitempty"`
	ConnectedAccountID   string                `json:"connected_account_id,omitempty"`
	Currency             Currency              `json:"currency,omitempty"`
	MerchantOrderID      string                `json:"merchant_order_id,omitempty"`
	Order                *Order                `json:"order,omitempty"`
	CustomerID           string                `json:"customer_id,omitempty"`
	Descriptor           string                `json:"descriptor,omitempty"`
	Metadata             Metadata              `json:"metadata,omitempty"`
	RequestID            uuid.UUID             `json:"request_id,omitempty"`
	ReturnURL            string                `json:"return_url,omitempty"`
	PaymentMethodOptions *PaymentMethodOptions `json:"payment_method_options,omitempty"`
}

type CreatePaymentIntentResponse struct {
	PaymentIntent
	RequestID uuid.UUID `json:"request_id,omitempty"`
}

func (awx airwallex) CreatePaymentIntent(
	ctx context.Context,
	in *CreatePaymentIntentRequest,
) (out *CreatePaymentIntentResponse, err error) {
	out = new(CreatePaymentIntentResponse)
	uri := string(awx.apiEndpoint) + string(createPaymentIntentEndpoint)
	if err = Do(ctx, awx.httpClient, http.MethodPost, uri, in, out); err != nil {
		return out, errors.Wrap(err, "Do()")
	}

	return out, err
}

// https://www.airwallex.com/docs/api#/Payment_Acceptance/Payment_Intents/_api_v1_pa_payment_intents__id_/get

type GetPaymentIntentRequest struct {
	ID string `json:"id,omitempty"`
}

type GetPaymentIntentResponse struct {
	PaymentIntent
	RequestID uuid.UUID `json:"request_id,omitempty"`
}

func (awx airwallex) GetPaymentIntent(
	ctx context.Context,
	in *GetPaymentIntentRequest,
) (out *GetPaymentIntentResponse, err error) {
	out = new(GetPaymentIntentResponse)
	uri := string(awx.apiEndpoint) + string(getPaymentIntentEndpoint)
	if err = Do(ctx, awx.httpClient, http.MethodGet, uri, in, out); err != nil {
		return out, errors.Wrap(err, "Do()")
	}

	return out, err
}

// https://www.airwallex.com/docs/api#/Payment_Acceptance/Payment_Intents/_api_v1_pa_payment_intents__id__confirm/post

type ConfirmPaymentIntentRequest struct {
	ID                      string                   `json:"id,omitempty"`
	ConfirmWithConsent      bool                     `json:"confirm_with_consent,omitempty"`
	Redirect                bool                     `json:"redirect,omitempty"`
	RequestID               uuid.UUID                `json:"request_id,omitempty"`
	CustomerID              string                   `json:"customer_id,omitempty"`
	PaymentMethod           *PaymentMethods          `json:"payment_method,omitempty"`
	PaymentConsentReference *PaymentConsentReference `json:"payment_consent_reference,omitempty"`
	PaymentMethodOptions    *PaymentMethodOptions    `json:"payment_method_options,omitempty"`
	DeviceData              *DeviceData              `json:"device_data,omitempty"`
	ReturnURL               string                   `json:"return_url,omitempty"`
}

type ConfirmPaymentIntentResponse struct {
	PaymentIntent
	RequestID uuid.UUID `json:"request_id,omitempty"`
}

func (awx airwallex) ConfirmPaymentIntent(
	ctx context.Context,
	in *ConfirmPaymentIntentRequest,
) (out *ConfirmPaymentIntentResponse, err error) {
	out = new(ConfirmPaymentIntentResponse)
	uri := string(awx.apiEndpoint) + string(confirmPaymentIntentEndpoint)
	if err = Do(ctx, awx.httpClient, http.MethodPost, uri, in, out); err != nil {
		return out, errors.Wrap(err, "Do()")
	}

	return out, err
}

// https://www.airwallex.com/docs/api#/Payment_Acceptance/Payment_Intents/_api_v1_pa_payment_intents__id__confirm_continue/post

type ContinueConfirmPaymentIntentRequest struct {
	ID        string              `json:"id,omitempty"`
	RequestID uuid.UUID           `json:"request_id,omitempty"`
	ThreeDS   *ThreeDS            `json:"three_ds,omitempty"`
	Type      ContinueConfirmType `json:"type,omitempty"`
	UseDCC    bool                `json:"use_dcc,omitempty"`
}

type ContinueConfirmPaymentIntentResponse struct {
	PaymentIntent
	RequestID uuid.UUID `json:"request_id,omitempty"`
}

func (awx airwallex) ContinueConfirmPaymentIntent(
	ctx context.Context,
	in *ContinueConfirmPaymentIntentRequest,
) (out *ContinueConfirmPaymentIntentResponse, err error) {
	out = new(ContinueConfirmPaymentIntentResponse)
	uri := string(awx.apiEndpoint) + string(continueConfirmPaymentIntentEndpoint)
	if err = Do(ctx, awx.httpClient, http.MethodPost, uri, in, out); err != nil {
		return out, errors.Wrap(err, "Do()")
	}

	return out, err
}

// https://www.airwallex.com/docs/api#/Payment_Acceptance/Payment_Intents/_api_v1_pa_payment_intents__id__capture/post

type CapturePaymentIntentRequest struct {
	Amount    Decimal   `json:"amount,omitempty"`
	ID        string    `json:"id,omitempty"`
	RequestID uuid.UUID `json:"request_id,omitempty"`
}

type CapturePaymentIntentResponse struct {
	PaymentIntent
	RequestID uuid.UUID `json:"request_id,omitempty"`
}

func (awx airwallex) CapturePaymentIntent(
	ctx context.Context,
	in *CapturePaymentIntentRequest,
) (out *CapturePaymentIntentResponse, err error) {
	out = new(CapturePaymentIntentResponse)
	uri := string(awx.apiEndpoint) + string(capturePaymentIntentEndpoint)
	if err = Do(ctx, awx.httpClient, http.MethodPost, uri, in, out); err != nil {
		return out, errors.Wrap(err, "Do()")
	}

	return out, err
}

// https://www.airwallex.com/docs/api#/Payment_Acceptance/Payment_Intents/_api_v1_pa_payment_intents__id__cancel/post

type CancelPaymentIntentRequest struct {
	CancellationReason string    `json:"cancellation_reason,omitempty"`
	ID                 string    `json:"id,omitempty"`
	RequestID          uuid.UUID `json:"request_id,omitempty"`
}

type CancelPaymentIntentResponse struct {
	PaymentIntent
	RequestID uuid.UUID `json:"request_id,omitempty"`
}

func (awx airwallex) CancelPaymentIntent(
	ctx context.Context,
	in *CancelPaymentIntentRequest,
) (out *CancelPaymentIntentResponse, err error) {
	out = new(CancelPaymentIntentResponse)
	uri := string(awx.apiEndpoint) + string(cancelPaymentIntentEndpoint)
	if err = Do(ctx, awx.httpClient, http.MethodPost, uri, in, out); err != nil {
		return out, errors.Wrap(err, "Do()")
	}

	return out, err
}

// https://www.airwallex.com/docs/api#/Payment_Acceptance/Payment_Intents/_api_v1_pa_payment_intents/get

type ListPaymentIntentsRequest struct {
	Currency         Currency            `json:"currency,omitempty"`
	FromCreatedAt    *TimeISO8601        `json:"from_created_at,omitempty"`
	MerchantOrderID  string              `json:"merchant_order_id,omitempty"`
	PageNum          int32               `json:"page_num,omitempty"`
	PageSize         int32               `json:"page_size,omitempty"`
	PaymentConsentID string              `json:"payment_consent_id,omitempty"`
	Status           PaymentIntentStatus `json:"status,omitempty"`
	ToCreatedAt      *TimeISO8601        `json:"to_created_at,omitempty"`
}

type ListPaymentIntentsResponse struct {
	HasMore string           `json:"has_more,omitempty"`
	Items   []*PaymentIntent `json:"items,omitempty"`
}

func (awx airwallex) ListPaymentIntents(
	ctx context.Context,
	in *ListPaymentIntentsRequest,
) (out *ListPaymentIntentsResponse, err error) {
	out = new(ListPaymentIntentsResponse)
	uri := string(awx.apiEndpoint) + string(listPaymentIntentsEndpoint)
	if err = Do(ctx, awx.httpClient, http.MethodGet, uri, in, out); err != nil {
		return out, errors.Wrap(err, "Do()")
	}

	return out, err
}
