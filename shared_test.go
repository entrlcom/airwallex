package airwallex

import (
	"testing"
	"time"
)

func TestTimeISO8601_UnmarshalJSON(t1 *testing.T) {
	type fields struct {
		Time time.Time
	}
	type args struct {
		data []byte
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		wantErr bool
	}{
		{
			name: "case1",
			fields: fields{
				Time: time.Date(2019, 9, 18, 3, 11, 0, 0, time.UTC),
			},
			args: args{
				data: []byte(`"2019-09-18T03:11:00+0000"`),
			},
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t1.Run(tt.name, func(t1 *testing.T) {
			t := &TimeISO8601{
				Time: tt.fields.Time,
			}
			if err := t.UnmarshalJSON(tt.args.data); (err != nil) != tt.wantErr {
				t1.Errorf("UnmarshalJSON() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}
